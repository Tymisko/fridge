using ApiGateway.Authorization.Policies.Definitions;
using ApiGateway.Authorization.Policies.Definitions.DefaultPolicy;

using Microsoft.AspNetCore.Authorization;

namespace ApiGateway.Authorization.Policies;

public static class AuthorizationBuilderExtensions
{
	private static readonly IAuthorizationPolicy[] Policies =
	[
		DefaultPolicy.Definition,
		AllowAnonymousPolicy.Definition
	];

	public static AuthorizationBuilder AddPolicies(this AuthorizationBuilder builder)
	{
		foreach (var policy in Policies)
		{
			builder.AddPolicy(policy.Name, policy.Builder);
		}

		return builder;
	}
}