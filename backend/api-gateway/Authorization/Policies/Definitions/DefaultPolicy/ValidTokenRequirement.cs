using ApiGateway.Services;

using Microsoft.AspNetCore.Authorization;

namespace ApiGateway.Authorization.Policies.Definitions.DefaultPolicy;

// This requirement is just a marker without any data.
public class ValidTokenRequirement : IAuthorizationRequirement;

public class ValidTokenHandler(IIdentityService identityService)
	: AuthorizationHandler<ValidTokenRequirement>
{
	protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
		ValidTokenRequirement requirement)
	{
		var httpContext = context.Resource as HttpContext;
		var token = httpContext?.Request.Headers.Authorization.ToString().Split(' ')[^1];

		if (string.IsNullOrWhiteSpace(token))
		{
			context.Fail();
			return;
		}

		var result = await identityService.ValidateTokenAsync(token);

		if (result is { IsSuccess: true, Value: true })
		{
			context.Succeed(requirement);
		}

		context.Fail();
	}
}