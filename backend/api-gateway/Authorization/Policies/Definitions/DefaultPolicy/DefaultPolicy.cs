using Microsoft.AspNetCore.Authorization;

namespace ApiGateway.Authorization.Policies.Definitions.DefaultPolicy;

public class DefaultPolicy : IAuthorizationPolicy
{
	private static DefaultPolicy? s_instance;
	public static DefaultPolicy Definition => s_instance ??= new DefaultPolicy();

	private const string NameValue = "custom-default";
	public string Name => NameValue;
	public Action<AuthorizationPolicyBuilder> Builder => b =>
	{
		b.AddRequirements(new ValidTokenRequirement());
	};
}