using Microsoft.AspNetCore.Authorization;

namespace ApiGateway.Authorization.Policies.Definitions;

public class AllowAnonymousPolicy : IAuthorizationPolicy
{
	private static AllowAnonymousPolicy? s_instance;
	public static AllowAnonymousPolicy Definition => s_instance ??= new AllowAnonymousPolicy();

	private const string NameValue = "allow-anonymous";
	public string Name => NameValue;

	public Action<AuthorizationPolicyBuilder> Builder => b =>
	{
		// Allow anonymous access
		b.RequireAssertion(_ => true);
	};
}