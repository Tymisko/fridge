using Microsoft.AspNetCore.Authorization;

namespace ApiGateway.Authorization.Policies;

public interface IAuthorizationPolicy
{
	public string Name { get; }
	public Action<AuthorizationPolicyBuilder> Builder { get; }
}