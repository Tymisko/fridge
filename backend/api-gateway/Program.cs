using API.Shared;
using API.Shared.Features.Authentication;
using API.Shared.Features.Versioning.Options;

using ApiGateway.Authorization.Policies;
using ApiGateway.Cors;
using ApiGateway.Options;
using ApiGateway.Services;

using DotNetEnv;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Cors.Infrastructure;

namespace ApiGateway;

public class Program
{
	private Program()
	{
		// This class should not be instantiated, but can't be made static because of the integration tests
	}

	public static async Task Main(string[] args)
	{
		var builder = WebApplication.CreateBuilder(args);

		if (builder.Environment.IsDevelopment())
		{
			var envFilePath = Path.Combine(Directory.GetCurrentDirectory(), "../../.env");
			Env.Load(envFilePath);
		}

		builder.Services.AddJwtOptions();
		builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer();
		builder.Services.AddOptions<ApiOptions>()
			.BindConfiguration(ApiConfigSectionNames.Api)
			.ValidateDataAnnotations()
			.ValidateOnStart();
		builder.Services.AddIdentityService();

		builder.Services.AddOptions<WebappOptions>()
			.BindConfiguration(ApiConfigSectionNames.Webapp)
			.ValidateDataAnnotations()
			.ValidateOnStart();

		builder.Services.AddSingleton<ICorsPolicyProvider, WebappCorsProvider>();
		builder.Services.AddCors();

		builder.Configuration.AddEnvironmentVariables();
		builder.Services.AddHealthChecks();
		AddAuthorization(builder);

		builder.Services.AddReverseProxy().LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"));

		var app = builder.Build();
		app.UseCors(CorsPolicyNames.FrontendApiAccessPolicy);

		if (!app.Environment.IsDevelopment())
		{
			app.UseHttpsRedirection();
		}

		app.MapHealthChecks("api/v1/health");
		app.UseAuthentication();
		app.UseAuthorization();
		app.MapReverseProxy(options =>
		{
			options.UseAuthentication();
			options.UseAuthorization();
		});

		await app.RunAsync();
	}

	private static void AddAuthorization(WebApplicationBuilder builder)
	{
		builder.Services.AddAuthorizationBuilder().AddPolicies();
	}
}