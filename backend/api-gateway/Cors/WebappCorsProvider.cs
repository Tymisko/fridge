﻿using ApiGateway.Options;

using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Options;

namespace ApiGateway.Cors;

public class WebappCorsProvider(IServiceProvider serviceProvider) : ICorsPolicyProvider
{
	public Task<CorsPolicy?> GetPolicyAsync(HttpContext context, string? policyName)
	{
		if (policyName != CorsPolicyNames.FrontendApiAccessPolicy)
		{
			return Task.FromResult<CorsPolicy?>(null);
		}

		var webappOptions = serviceProvider.GetRequiredService<IOptions<WebappOptions>>().Value;
		CorsPolicyBuilder corsPolicyBuilder = new();
		corsPolicyBuilder.WithOrigins(webappOptions.Url)
			.AllowAnyHeader()
			.AllowAnyMethod();

		return Task.FromResult<CorsPolicy?>(corsPolicyBuilder.Build());
	}
}