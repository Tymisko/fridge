﻿namespace ApiGateway.Cors;

public static class CorsPolicyNames
{
	public const string FrontendApiAccessPolicy = "FrontendApiAccessPolicy";
}