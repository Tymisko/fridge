using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Options;

public record IdentityServiceOptions
{
	[Required] public required string Host { get; init; }
	[Required] public required int Port { get; init; }
}