﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Options;

public record WebappOptions
{
	[Required, Url] public required string Url { get; init; }
}