using System.Net;
using System.Net.Http.Headers;

using Domain.Shared.Common;

using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace ApiGateway.Services;

public interface IIdentityService
{
	Task<Result<bool>> ValidateTokenAsync(string token);
}

public class IdentityService(HttpClient client) : IIdentityService
{
	public async Task<Result<bool>> ValidateTokenAsync(string token)
	{
		const string endpoint = "validate";
		var request = new HttpRequestMessage(HttpMethod.Get, endpoint);
		request.Headers.Authorization = new AuthenticationHeaderValue(JwtBearerDefaults.AuthenticationScheme, token);
		var response = await client.SendAsync(request);
		switch (response.StatusCode)
		{
			case HttpStatusCode.OK: return Result<bool>.Success(true);
			case HttpStatusCode.Unauthorized: return Result<bool>.Success(false);
			default:
				{
					var responseContent = await response.Content.ReadAsStringAsync();
					var responseDetails = $"StatusCode: {response.StatusCode}, Content: {responseContent}";
					var message = $"An error occurred while validating the token: {responseDetails}";
					return new Error(HttpStatusCode.InternalServerError, message);
				}
		}
	}
}