using System.Net.Http.Headers;
using System.Net.Mime;

using API.Shared.Features.Versioning.Options;

using ApiGateway.Options;

using Microsoft.Extensions.Options;

namespace ApiGateway.Services;

public static class ServiceCollectionExtensions
{
	public static IServiceCollection AddIdentityService(this IServiceCollection services)
	{
		const string configurationSectionName = "Services:Identity";
		services.AddOptions<IdentityServiceOptions>()
			.BindConfiguration(configurationSectionName)
			.ValidateDataAnnotations()
			.ValidateOnStart();

		services.AddHttpClient<IIdentityService, IdentityService>(nameof(IdentityService), (provider, client) =>
		{
			var apiOptions = provider.GetRequiredService<IOptions<ApiOptions>>().Value;
			var identityServiceOptions =
				provider.GetRequiredService<IOptions<IdentityServiceOptions>>().Value;
			var host = identityServiceOptions.Host;
			var port = identityServiceOptions.Port;
			var basePath = $"api/v{apiOptions.Version}/identity/tokens";

			client.BaseAddress = new Uri($"http://{host}:{port}/{basePath}");
			client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json)
			);
			client.DefaultRequestHeaders.Accept.Add(
				new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.ProblemJson)
			);
		});
		
		return services;
	}
}