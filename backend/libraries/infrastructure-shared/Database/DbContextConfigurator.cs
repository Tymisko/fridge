using Infrastructure.Shared.Options.Postgres;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Infrastructure.Shared.Database;

public static class DbContextConfigurator
{
	public static IServiceCollection AddDatabase<T>(this IServiceCollection services) where T : BaseDbContext
	{
		services.AddTransient<IOptionsFactory<PostgresOptions>, PostgresOptionsFactory>();
		services.AddOptions<PostgresOptions>()
			.ValidateDataAnnotations()
			.ValidateOnStart();

		return services.AddDbContext<T>((serviceProvider, builder) =>
		{
			var postgresOptions = serviceProvider.GetRequiredService<IOptions<PostgresOptions>>().Value;
			var connectionString = postgresOptions.ConnectionString();
			builder.UseNpgsql(connectionString);
		});
	}
}
