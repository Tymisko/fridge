using Infrastructure.Shared.ServiceBus;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Shared;

public abstract class BaseInfrastructureConfigurator(IServiceCollection services, IConfiguration configuration)
{
	protected readonly IServiceCollection _services = services;
	protected readonly IConfiguration _configuration = configuration;

	public virtual IServiceCollection AddInfrastructure()
	{
		return _services.AddServiceBus();
	}
}