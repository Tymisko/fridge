using System.Net;

using Domain.Shared.Common;

namespace Infrastructure.Shared.Errors;

public static class DatabaseErrors
{
	public static Error UpdateConcurrencyConflict(string entityName) => new(HttpStatusCode.Conflict,
		$"Concurrency conflict occured while updating {entityName}");

	public static Error DeleteConcurrencyConflict(string entityName) => new(HttpStatusCode.Conflict,
		$"Concurrency conflict occured while deleting {entityName}");

	public static Error Unexpected => new(HttpStatusCode.InternalServerError, "An unexpected database error occured");
}