using Infrastructure.Shared.Options;

using MassTransit;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;

namespace Infrastructure.Shared.ServiceBus;

internal static class ServiceBusRegistrar
{
	public static IServiceCollection AddServiceBus(this IServiceCollection services)
	{
		services.AddOptions<RabbitMqOptions>()
			.BindConfiguration(InfrastructureConfigSectionNames.RabbitMq)
			.ValidateDataAnnotations()
			.ValidateOnStart();

		services.AddMassTransit(busConfigurator =>
		{
			busConfigurator.SetKebabCaseEndpointNameFormatter();
			busConfigurator.UsingRabbitMq((context, configurator) =>
			{
				var rabbitMqOptions = context.GetRequiredService<IOptions<RabbitMqOptions>>().Value;
				configurator.Host(rabbitMqOptions.Address(), rabbitMqOptions.VirtualHost, h =>
				{
					h.Username(rabbitMqOptions.Username);
					h.Password(rabbitMqOptions.Password);
				});

				configurator.ConfigureEndpoints(context);
				configurator.AutoStart = true;
			});

			busConfigurator.ConfigureHealthCheckOptions(options =>
			{
				options.Name = nameof(MassTransit);
				options.MinimalFailureStatus = HealthStatus.Unhealthy;
				options.Tags.Add("health");
			});
		});

		return services;
	}
}
