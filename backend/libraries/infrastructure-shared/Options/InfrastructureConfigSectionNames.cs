namespace Infrastructure.Shared.Options;

public static class InfrastructureConfigSectionNames
{
	public const string
		Postgres = "Postgres",
		RabbitMq = "RabbitMQ";
}
