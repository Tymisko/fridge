namespace Infrastructure.Shared.Options;

public static class OptionsRestrictions
{
	public const int MinPort = 1024;
	public const int MaxPort = 65535;
	public const int MinRabbitMqUsernameLength = 1;
	public const int MinRabbitMqPasswordLength = 1;
}