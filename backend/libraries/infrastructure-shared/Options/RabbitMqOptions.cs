using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Shared.Options;

internal record RabbitMqOptions
{
	[Required] public required string Host { get; init; }
	[Required] public required string VirtualHost { get; init; }
	[Required, Range(OptionsRestrictions.MinPort, OptionsRestrictions.MaxPort)] public required int Port { get; init; }

	[Required, MinLength(OptionsRestrictions.MinRabbitMqUsernameLength)] public required string Username { get; init; }

	[Required, MinLength(OptionsRestrictions.MinRabbitMqPasswordLength)] public required string Password { get; init; }
	public Uri Address() => new($"amqp://{Host}:{Port}");
}
