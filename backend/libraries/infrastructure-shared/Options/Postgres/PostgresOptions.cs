using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Shared.Options.Postgres;

public record PostgresOptions
{
	[Required] public required string UserId { get; init; }
	[Required] public required string Password { get; init; }
	[Required] public required string Host { get; init; }
	[Required] public required int Port { get; init; }
	[Required] public required string DatabaseName { get; init; }

	public string ConnectionString() =>
		$"User ID={UserId};Password={Password};Host={Host};Port={Port};Database={DatabaseName};";
}
