using Domain.Shared;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Infrastructure.Shared.Options.Postgres;

public class PostgresOptionsFactory(IConfiguration configuration, IMicroserviceNameProvider microserviceNameProvider) : IOptionsFactory<PostgresOptions>
{
	public PostgresOptions Create(string name)
	{
		const string userIdSectionName = $"{InfrastructureConfigSectionNames.Postgres}:UserId";
		var userId = configuration.GetValue<string>(userIdSectionName);
		if (string.IsNullOrWhiteSpace(userId))
		{
			throw new InvalidOperationException($"Missing or empty configuration for '{userIdSectionName}'.");
		}

		const string passwordSectionName = $"{InfrastructureConfigSectionNames.Postgres}:Password";
		var password = configuration.GetValue<string>(passwordSectionName);
		if (string.IsNullOrWhiteSpace(password))
		{
			throw new InvalidOperationException($"Missing or empty configuration for '{passwordSectionName}'.");
		}

		const string hostSectionName = $"{InfrastructureConfigSectionNames.Postgres}:Host";
		var host = configuration.GetValue<string>(hostSectionName);
		if (string.IsNullOrWhiteSpace(host))
		{
			throw new InvalidOperationException($"Missing or invalid configuration for '{hostSectionName}'.");
		}

		const string portSectionName = $"{InfrastructureConfigSectionNames.Postgres}:Port";
		var port = configuration.GetValue<int?>(portSectionName);
		if (!port.HasValue)
		{
			throw new InvalidOperationException($"Missing or invalid configuration for '{portSectionName}'.");
		}

		var microserviceNameUppercase = microserviceNameProvider.GetMicroserviceName();
		var databaseNameSectionName = $"{InfrastructureConfigSectionNames.Postgres}:{microserviceNameUppercase}:DatabaseName";
		var databaseName = configuration.GetValue<string>(databaseNameSectionName);
		if (string.IsNullOrWhiteSpace(databaseName))
		{
			throw new InvalidOperationException($"Missing or empty configuration for '{databaseNameSectionName}'.");
		}

		return new PostgresOptions
		{
			UserId = userId,
			Password = password,
			Host = host, 
			Port = port.Value,
			DatabaseName = databaseName
		};
	}
}
