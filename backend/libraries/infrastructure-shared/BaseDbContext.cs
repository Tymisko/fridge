using System.Reflection;

using Domain.Shared.Entities;

using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Shared;

public abstract class BaseDbContext(DbContextOptions options, Assembly dbContextAssembly) : DbContext(options)
{
	public override int SaveChanges(bool acceptAllChangesOnSuccess)
	{
		SetEntitiesUpdateMetadata();
		return base.SaveChanges(acceptAllChangesOnSuccess);
	}

	public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken =
		default)
	{
		SetEntitiesUpdateMetadata();
		return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
	}

	private void SetEntitiesUpdateMetadata()
	{
		var entries = ChangeTracker
			.Entries()
			.Where(entry =>
				entry is { Entity: BaseEntity, State: EntityState.Modified }
			);

		foreach (var entry in entries)
		{
			var baseEntity = (BaseEntity)entry.Entity;
			baseEntity.SetUpdateAt(DateTime.UtcNow);
			baseEntity.IncrementVersion();
		}
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.ApplyConfigurationsFromAssembly(dbContextAssembly);
		base.OnModelCreating(modelBuilder);
	}

	public bool IsTracked<T>(Guid id) where T : BaseEntity
	{
		var entry = ChangeTracker.Entries<T>().FirstOrDefault(e => e.Entity.Id.Equals(id));
		return entry is not null;
	}
}