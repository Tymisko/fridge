using Infrastructure.Shared.Options;
using Infrastructure.Shared.Options.Postgres;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;

using RabbitMQ.Client;

namespace Infrastructure.Shared;

public static class InfrastructureHealthChecks
{
	public static void AddInfrastructureHealthChecks(this IHealthChecksBuilder healthChecksBuilder)
	{
		healthChecksBuilder
			.AddPostgresqlHealthCheck()
			.AddRabbitMqHealthCheck();
	}

	private static IHealthChecksBuilder AddRabbitMqHealthCheck(this IHealthChecksBuilder builder)
	{
		return builder.AddRabbitMQ(
			sp =>
			{
				var rabbitMqOptions = sp.GetRequiredService<IOptions<RabbitMqOptions>>().Value;

				var factory = new ConnectionFactory
				{
					HostName = rabbitMqOptions.Host,
					Port = rabbitMqOptions.Port,
					UserName = rabbitMqOptions.Username,
					Password = rabbitMqOptions.Password,
					VirtualHost = rabbitMqOptions.VirtualHost,
					AutomaticRecoveryEnabled = true,
					NetworkRecoveryInterval = TimeSpan.FromSeconds(5),
					RequestedHeartbeat = TimeSpan.FromMinutes(30),
					Ssl = null!,
					Uri = rabbitMqOptions.Address()
				};

				return factory.CreateConnectionAsync();
			},
			name: "RabbitMQ",
			failureStatus: HealthStatus.Unhealthy
		);
	}


	private static IHealthChecksBuilder AddPostgresqlHealthCheck(this IHealthChecksBuilder builder)
	{
		return builder.AddNpgSql(
			sp =>
			{
				var postgresOptions = sp.GetRequiredService<IOptions<PostgresOptions>>().Value;
				var connectionString = postgresOptions.ConnectionString();
				return connectionString;
			},
			name: "Postgresql",
			failureStatus: HealthStatus.Unhealthy
		);
	}
}
