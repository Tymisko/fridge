using Domain.Shared.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Shared.Entities.Configurations;

public abstract class BaseEntityTypeConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseEntity
{
	public virtual void Configure(EntityTypeBuilder<T> builder)
	{
		builder.HasKey(e => e.Id);
		builder.Property(e => e.CreatedAt)
			.IsRequired()
			.ValueGeneratedOnAdd();

		builder.Property(e => e.UpdatedAt)
			.IsRequired(false)
			.ValueGeneratedOnUpdate();

		builder.Property(e => e.Version)
			.IsConcurrencyToken();
	}
}