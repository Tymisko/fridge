﻿namespace Infrastructure.Shared.Entities.Configurations;

public record EntityCheckConstraint(string Name, string Sql);