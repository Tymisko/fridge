namespace Domain.Shared;

public interface IMicroserviceNameProvider
{
	string GetMicroserviceName();
}
