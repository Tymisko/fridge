using System.Linq.Expressions;

using Domain.Shared.Common;
using Domain.Shared.Entities;

namespace Domain.Shared.Repositories;

public interface IBaseRepository<T> where T : BaseEntity
{
	Task<Result<Guid>> CreateAsync(T entity, CancellationToken cancellationToken);
	Task<Result<bool>> ExistsAsync(Expression<Func<T, bool>> predicate, CancellationToken cancellationToken);
	Task<Result<bool>> ExistsAsync(Guid id, CancellationToken cancellationToken);
	Task<Result<T>> GetByIdAsync(Guid id, CancellationToken cancellationToken);
	Task<Result> UpdateAsync(T entity, CancellationToken cancellationToken);
	Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken);
}