using System.Net;

namespace Domain.Shared.Common;

public sealed record Error(HttpStatusCode StatusCode, string Details);