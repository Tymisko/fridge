namespace Domain.Shared.Entities;

public abstract record BaseEntity
{
	public required Guid Id { get; init; }
	public required DateTime CreatedAt { get; init; } = DateTime.UtcNow;
	public DateTime? UpdatedAt { get; private set; }
	public int Version { get; private set; } = 1;

	public void IncrementVersion() => Version++;
	public void SetUpdateAt(DateTime dateTime) => UpdatedAt = dateTime;
}