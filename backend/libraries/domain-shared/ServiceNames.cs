namespace Domain.Shared;

public static class ServiceNames
{
	public const string
		Identity = "Identity",
		Ingredient = "Ingredient",
		MealPlan = "MealPlan",
		Recipe = "Recipe",
		ShoppingList = "ShoppingList";
}
