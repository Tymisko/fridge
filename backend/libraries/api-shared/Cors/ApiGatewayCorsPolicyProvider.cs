﻿using API.Shared.Core.Options;

using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Options;

namespace API.Shared.Cors;

public class ApiGatewayCorsPolicyProvider(IServiceProvider serviceProvider) : ICorsPolicyProvider
{
	public Task<CorsPolicy?> GetPolicyAsync(HttpContext context, string? policyName)
	{
		if (policyName != CorsPolicyNames.InternalMicroservicesAccessPolicy)
		{
			return Task.FromResult<CorsPolicy?>(null);
		}

		var apiGatewayOptions = serviceProvider.GetRequiredService<IOptions<ApiGatewayOptions>>().Value;
		CorsPolicyBuilder corsPolicyBuilder = new();
		corsPolicyBuilder.WithOrigins(apiGatewayOptions.Url)
			.AllowAnyHeader()
			.AllowAnyMethod();
		return Task.FromResult<CorsPolicy?>(corsPolicyBuilder.Build());
	}
}