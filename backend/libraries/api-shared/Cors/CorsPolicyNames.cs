﻿namespace API.Shared.Cors;

public static class CorsPolicyNames
{
	public const string InternalMicroservicesAccessPolicy = "InternalMicroservicesAccessPolicy";
}