using API.Shared.Core.Options;
using API.Shared.Cors;
using API.Shared.Extensions;
using API.Shared.Features.Versioning.Options;

using Domain.Shared;

using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;

namespace API.Shared;

public abstract class BaseApi
{
	private WebApplication? _app;

	public virtual void Initialize(WebApplicationBuilder builder, Action<IServiceCollection, IConfiguration> options,
		Action<IHealthChecksBuilder> configureHealthChecks)
	{
		options.Invoke(builder.Services, builder.Configuration);

		builder.Services.AddOptions<ApiOptions>()
			.BindConfiguration(ApiOptions.SectionName)
			.ValidateDataAnnotations()
			.ValidateOnStart();

		builder.Services.AddOptions<ApiGatewayOptions>()
			.BindConfiguration(ApiGatewayOptions.SectionName)
			.ValidateDataAnnotations()
			.ValidateOnStart();

		builder.Services.AddSingleton<ICorsPolicyProvider, ApiGatewayCorsPolicyProvider>();
		builder.Services.AddCors();

		var servicesProvider = builder.Services.BuildServiceProvider();
		var apiOptions = servicesProvider.GetRequiredService<IOptions<ApiOptions>>().Value;
		var microserviceNameProvider = servicesProvider.GetRequiredService<IMicroserviceNameProvider>();
		var microserviceName = microserviceNameProvider.GetMicroserviceName();

		string routePrefix = GetRoutePrefix(apiOptions);

		builder.Services
			.AddControllers(o => o.UseRoutePrefix(routePrefix))
			.AddNewtonsoftJson();

		builder.Services.AddEndpointsApiExplorer();
		if (builder.Environment.IsDevelopment())
		{
			builder.Services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc($"v{apiOptions.Version}",
					new OpenApiInfo { Title = $"{microserviceName} Service", Version = $"v{apiOptions.Version}" });
			});
		}

		var healthChecksBuilder = builder.Services.AddHealthChecks();
		configureHealthChecks.Invoke(healthChecksBuilder);

		_app = builder.Build();
		_app.UseCors(CorsPolicyNames.InternalMicroservicesAccessPolicy);

		if (_app.Environment.IsDevelopment())
		{
			_app.UseSwagger(o =>
			{
				o.RouteTemplate = $"{routePrefix}/docs/{{documentName}}/swagger.json";
			});
			_app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint($"/{routePrefix}/docs/v{apiOptions.Version}/swagger.json",
					$"{microserviceName} Service v{apiOptions.Version}");
				c.RoutePrefix = $"{routePrefix}/docs";
			});
		}

		_app.MapHealthChecks($"/{routePrefix}/health");
		_app.MapControllers();

		if (!_app.Environment.IsDevelopment())
		{
			_app.UseHttpsRedirection();
		}
	}

	private static string GetRoutePrefix(ApiOptions apiOptions)
	{
		return $"api/v{apiOptions.Version}";
	}

	public Task RunAsync()
	{
		if (_app is null)
		{
			throw new InvalidOperationException("The application has not been initialized.");
		}

		return _app.RunAsync();
	}
}