using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Routing;

namespace API.Shared;

public class RoutePrefixConvention(IRouteTemplateProvider route) : IApplicationModelConvention
{
	private readonly AttributeRouteModel _routePrefix = new(route);

	public void Apply(ApplicationModel application)
	{
		foreach (var selector in application.Controllers.SelectMany(c => c.Selectors))
		{
			if (selector.AttributeRouteModel is not null)
			{
				selector.AttributeRouteModel =
					AttributeRouteModel.CombineAttributeRouteModel(_routePrefix, selector.AttributeRouteModel);
				continue;
			}

			selector.AttributeRouteModel = _routePrefix;
		}
	}
}