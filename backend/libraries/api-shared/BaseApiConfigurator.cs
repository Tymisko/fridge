using FluentValidation;
using FluentValidation.AspNetCore;

namespace API.Shared;

public abstract class BaseApiConfigurator<T>(IServiceCollection services) where T : BaseApi
{
	protected readonly IServiceCollection _services = services;

	public virtual void AddApi()
	{
		_services
			.AddMediatR(cfg =>
				cfg.RegisterServicesFromAssemblyContaining<T>()
			)
			.AddAutoMapper(typeof(T).Assembly, typeof(BaseApi).Assembly)
			.AddFluentValidationAutoValidation()
			.AddValidatorsFromAssemblyContaining<T>();
	}
}