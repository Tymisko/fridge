using Microsoft.AspNetCore.Mvc;

namespace API.Shared.Extensions;

public static class MvcOptionsExtensions
{
	public static void UseRoutePrefix(this MvcOptions options, string prefix)
	{
		var routeAttribute = new RouteAttribute(prefix);
		var routePrefixConvention = new RoutePrefixConvention(routeAttribute);
		options.Conventions.Add(routePrefixConvention);
	}
}