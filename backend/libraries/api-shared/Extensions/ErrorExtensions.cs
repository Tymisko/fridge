using Domain.Shared.Common;

using Microsoft.AspNetCore.Mvc;

namespace API.Shared.Extensions;

public static class ErrorExtensions
{
	public static ProblemDetails ToProblemDetails(this Error error)
	{
		int statusCode = (int)error.StatusCode;
		var problemDetails = new ProblemDetails { Status = statusCode, Detail = error.Details };
		return problemDetails;
	}

	public static ObjectResult ToProblemDetailsResult(this Error error)
	{
		var problemDetails = error.ToProblemDetails();
		return new ObjectResult(problemDetails) { StatusCode = problemDetails.Status };
	}
}