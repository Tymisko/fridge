﻿using System.ComponentModel.DataAnnotations;

namespace API.Shared.Core.Options;

public record ApiGatewayOptions
{
	public const string SectionName = "ApiGateway";
	[Required, Url] public required string Url { get; init; }
};
