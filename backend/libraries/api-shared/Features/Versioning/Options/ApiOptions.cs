using System.ComponentModel.DataAnnotations;

namespace API.Shared.Features.Versioning.Options;

public record ApiOptions
{
	public const string SectionName = "Api";
	[Required, Range(1, 2)] public int Version { get; init; }
}
