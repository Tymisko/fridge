using API.Shared.Features.Authentication.Options;

namespace API.Shared.Features.Authentication;

public static class ServiceCollectionExtensions
{
	private const string JwtOptionsSectionName = "Authentication";

	public static IServiceCollection AddJwtOptions(this IServiceCollection services)
	{
		services.AddOptions<JwtOptions>()
			.BindConfiguration(JwtOptionsSectionName)
			.ValidateDataAnnotations()
			.ValidateOnStart();
		services.ConfigureOptions<JwtBearerOptionsSetup>();

		return services;
	}
}