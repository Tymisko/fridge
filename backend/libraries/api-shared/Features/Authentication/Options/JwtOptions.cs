using System.ComponentModel.DataAnnotations;

namespace API.Shared.Features.Authentication.Options;

public record JwtOptions
{
	[Required, MinLength(3)] public required string Issuer { get; init; }
	[Required, MinLength(3)] public required string Audience { get; init; }
	[Required, MinLength(128)] public required string SecretKey { get; init; }
}