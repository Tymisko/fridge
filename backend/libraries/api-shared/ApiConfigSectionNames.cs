﻿namespace API.Shared;

public static class ApiConfigSectionNames
{
	public const string Api = "Api";
	public const string Webapp = "Webapp";
}