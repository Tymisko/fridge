using DotNetEnv;

using API.Shared;

using Infrastructure.Shared;

namespace Host.Shared;

public class Program<TApi> where TApi : BaseApi, new()
{
	private readonly TApi _api;

	public Program(string[] args,
		Action<IServiceCollection, IConfiguration> options
	)
	{
		var builder = WebApplication.CreateBuilder(args);
		if (builder.Environment.IsDevelopment())
		{
			var envFilePath = Path.Combine(Directory.GetCurrentDirectory(), "../../../../.env");
			Env.Load(envFilePath);
		}

		builder.Configuration.AddEnvironmentVariables();
		builder.Services.AddHostedService<HealthChecksHostedService>();
		_api = new TApi();
		_api.Initialize(builder, options, ConfigureHealthChecks);
	}

	private static void ConfigureHealthChecks(IHealthChecksBuilder healthChecksBuilder)
	{
		healthChecksBuilder.AddInfrastructureHealthChecks();
	}

	public async Task RunAsync()
	{
		await _api.RunAsync();
	}
}
