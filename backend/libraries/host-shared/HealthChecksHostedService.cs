using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Host.Shared;

public class HealthChecksHostedService(
	ILogger<HealthChecksHostedService> logger,
	HealthCheckService healthCheckService,
	IHostApplicationLifetime applicationLifetime
) : BackgroundService
{
	private readonly TimeSpan _interval = TimeSpan.FromSeconds(30);

	protected override Task ExecuteAsync(CancellationToken stoppingToken)
	{
		try
		{
			applicationLifetime.ApplicationStarted.Register(() =>
			{
				_ = RunHealthChecksPeriodicallyAsync(stoppingToken);
			});
		}
		catch (Exception ex)
		{
			logger.LogError(ex, "An error occurred while starting the health checks background service.");
		}

		return Task.CompletedTask;
	}

	private async Task RunHealthChecksPeriodicallyAsync(CancellationToken cancellationToken)
	{
		try
		{
			while (!cancellationToken.IsCancellationRequested)
			{
				await Task.Delay(_interval, cancellationToken);
				await RunHealthChecksAsync(cancellationToken);
			}
		}
		catch (TaskCanceledException ex)
		{
			logger.LogWarning(ex, "Health checks background service was cancelled.");
		}
	}

	private async Task RunHealthChecksAsync(CancellationToken cancellationToken)
	{
		var report = await healthCheckService.CheckHealthAsync(cancellationToken);

		foreach (var entry in report.Entries)
		{
			if (entry.Value.Status == HealthStatus.Healthy)
			{
				logger.LogInformation("{Timestamp} - {HealthCheckName} - {Status}",
					DateTime.UtcNow,
					entry.Key,
					entry.Value.Status
				);
				continue;
			}

			logger.LogError("{Timestamp} - {HealthCheckName} - {Status}",
				DateTime.UtcNow,
				entry.Key,
				entry.Value.Status
			);
		}
	}
}
