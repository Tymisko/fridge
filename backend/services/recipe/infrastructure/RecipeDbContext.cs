﻿using Infrastructure.Shared;

using Microsoft.EntityFrameworkCore;

namespace Recipe.Infrastructure;

public class RecipeDbContext(DbContextOptions<RecipeDbContext> options)
	: BaseDbContext(options, typeof(RecipeDbContext).Assembly)
{
	public DbSet<Domain.Entities.Recipe> Recipes { get; set; }
}