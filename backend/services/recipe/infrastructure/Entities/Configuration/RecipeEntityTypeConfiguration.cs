﻿using Infrastructure.Shared.Entities.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Recipe.Domain.Entities;

namespace Recipe.Infrastructure.Entities.Configuration;

public class RecipeEntityTypeConfiguration : BaseEntityTypeConfiguration<Domain.Entities.Recipe>
{
	public override void Configure(EntityTypeBuilder<Domain.Entities.Recipe> builder)
	{
		base.Configure(builder);

		builder.Property(r => r.UserId)
			.IsRequired();

		builder.HasIndex(r => r.UserId)
			.HasDatabaseName(RecipeIndexes.UserId);

		builder.Property(r => r.Name)
			.IsRequired()
			.HasMaxLength(RecipeValidationRules.NameMaxLength);
	}
}