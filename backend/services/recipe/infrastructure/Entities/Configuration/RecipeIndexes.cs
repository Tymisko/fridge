﻿namespace Recipe.Infrastructure.Entities.Configuration;

public static class RecipeIndexes
{
	private const string Prefix = "IX_Recipe";
	public const string UserId = $"{Prefix}_UserId";
}