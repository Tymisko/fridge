﻿using Domain.Shared.Entities;

namespace Recipe.Domain.Entities;

public record Recipe : BaseEntity
{
	public required Guid UserId { get; init; }
	public required string Name { get; init; }
}