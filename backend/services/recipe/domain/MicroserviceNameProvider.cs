﻿using Domain.Shared;

namespace Recipe.Domain;

public class MicroserviceNameProvider : IMicroserviceNameProvider
{
	public string GetMicroserviceName()
	{
		return ServiceNames.Recipe;
	}
}