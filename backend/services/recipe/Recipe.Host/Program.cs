using Domain.Shared;

using Host.Shared;

using Recipe.API;
using Recipe.Domain;
using Recipe.Infrastructure;

namespace Recipe.Host;

public class Program
{
	private Program()
	{
		// This is a static class
	}
	
	public static async Task Main(string[] args)
	{
		var api = new Program<RecipeApi>(args, (services, configuration) =>
		{
			services.AddSingleton<IMicroserviceNameProvider, MicroserviceNameProvider>();
			new InfrastructureConfigurator(services, configuration).AddInfrastructure();
		});
		await api.RunAsync();
	}
}