﻿namespace ShoppingList.Infrastructure.Entities.Configurations;

public static class ShoppingListIndexes
{
	private const string Prefix = "IX_ShoppingList";
	public const string UserId = $"{Prefix}_UserId";
}