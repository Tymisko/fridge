﻿using Infrastructure.Shared.Entities.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ShoppingList.Domain.Entities;

namespace ShoppingList.Infrastructure.Entities.Configurations;

public class ShoppingListEntityTypeConfiguration : BaseEntityTypeConfiguration<Domain.Entities.ShoppingList>
{
	public override void Configure(EntityTypeBuilder<Domain.Entities.ShoppingList> builder)
	{
		base.Configure(builder);

		builder.Property(l => l.Name)
			.IsRequired()
			.HasMaxLength(ShoppingListValidationRules.NameMaxLength);

		builder.Property(l => l.UserId)
			.IsRequired();

		builder.HasIndex(l => l.UserId)
			.HasDatabaseName(ShoppingListIndexes.UserId);
	}
}