﻿using Infrastructure.Shared;

using Microsoft.EntityFrameworkCore;

namespace ShoppingList.Infrastructure;

public class ShoppingListDbContext(DbContextOptions<ShoppingListDbContext> options)
	: BaseDbContext(options, typeof(ShoppingListDbContext).Assembly)
{
	public DbSet<Domain.Entities.ShoppingList> ShoppingLists { get; set; }
}