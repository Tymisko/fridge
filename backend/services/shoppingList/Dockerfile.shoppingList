# Build stage
FROM mcr.microsoft.com/dotnet/sdk:8.0-alpine AS build
WORKDIR /src
ARG PORT=80

# Copy project files for restore
COPY Directory.Build.props ./
COPY libraries/api-shared/API.Shared.csproj libraries/api-shared/
COPY libraries/domain-shared/Domain.Shared.csproj libraries/domain-shared/
COPY libraries/host-shared/Host.Shared.csproj libraries/host-shared/
COPY libraries/infrastructure-shared/Infrastructure.Shared.csproj libraries/infrastructure-shared/
COPY services/shoppingList/api/*.csproj ./services/shoppingList/api/
COPY services/shoppingList/domain/*.csproj ./services/shoppingList/domain/
COPY services/shoppingList/infrastructure/*.csproj ./services/shoppingList/infrastructure/
COPY services/shoppingList/ShoppingList.Host/*.csproj ./services/shoppingList/ShoppingList.Host/

RUN dotnet new sln --name shoppingList && \
    find . -name '*.csproj' -exec dotnet sln add {} \; && \
    dotnet restore

COPY libraries/ ./libraries/
COPY services/shoppingList/ ./services/shoppingList/

RUN dotnet publish services/shoppingList/ShoppingList.Host/ShoppingList.Host.csproj \
    --configuration Release \
    --runtime linux-musl-x64 \
    --self-contained true \
    -p:PublishSingleFile=true \
    -p:EnableCompressionInSingleFile=true \
    --output /app/publish \
    --no-restore

# Runtime stage
FROM alpine:3.19 AS runtime
WORKDIR /app
ARG PORT

# Install required dependencies && add non-root user
RUN apk add --no-cache \
    icu-libs \
    libintl \
    libstdc++ \
    tzdata \
    && adduser -u 1000 --disabled-password --gecos "" appuser && chown -R appuser /app 
USER appuser

# Copy the published files
COPY --from=build --chown=appuser:appuser /app/publish .

ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false
ENV ASPNETCORE_ENVIRONMENT=Production
ENV PORT=${PORT}
ENV ASPNETCORE_URLS=http://+:${PORT}

EXPOSE ${PORT}

ENTRYPOINT ["./ShoppingList.Host"]
