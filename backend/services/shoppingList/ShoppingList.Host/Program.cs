using System.Threading.Tasks;

using Domain.Shared;

using Host.Shared;

using Microsoft.Extensions.DependencyInjection;

using ShoppingList.API;
using ShoppingList.Domain;
using ShoppingList.Infrastructure;

namespace ShoppingList.Host;

public class Program
{
	private Program()
	{
		// This is a static class
	}

	public static async Task Main(string[] args)
	{
		var api = new Program<ShoppingListApi>(args, (services, configuration) =>
		{
			services.AddSingleton<IMicroserviceNameProvider, MicroserviceNameProvider>();
			new InfrastructureConfigurator(services, configuration).AddInfrastructure();
		});

		await api.RunAsync();
	}
}