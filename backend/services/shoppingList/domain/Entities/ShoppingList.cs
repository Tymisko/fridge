﻿using Domain.Shared.Entities;

namespace ShoppingList.Domain.Entities;

public record ShoppingList : BaseEntity
{
	public required Guid UserId { get; init; }
	public required string Name { get; init; }
}