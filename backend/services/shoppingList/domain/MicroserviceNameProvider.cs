﻿using Domain.Shared;

namespace ShoppingList.Domain;

public class MicroserviceNameProvider : IMicroserviceNameProvider
{
	public string GetMicroserviceName()
	{
		return ServiceNames.ShoppingList;
	}
}