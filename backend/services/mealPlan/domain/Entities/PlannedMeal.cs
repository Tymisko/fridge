﻿using Domain.Shared.Entities;

namespace MealPlan.Domain.Entities;

public record PlannedMeal : BaseEntity
{
	public required Guid UserId { get; init; }
	public required Guid RecipeId { get; init; }
	public required Guid MealTypeId { get; init; }
	public required DateTime Day { get; init; }
}