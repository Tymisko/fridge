﻿using Domain.Shared;

namespace MealPlan.Domain;

public class MicroserviceNameProvider : IMicroserviceNameProvider
{
	public string GetMicroserviceName()
	{
		return ServiceNames.MealPlan;
	}
}