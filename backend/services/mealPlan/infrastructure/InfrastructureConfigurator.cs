using Infrastructure.Shared;
using Infrastructure.Shared.Database;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MealPlan.Infrastructure;

public class InfrastructureConfigurator(IServiceCollection services, IConfiguration configuration)
	: BaseInfrastructureConfigurator(services, configuration)
{
	public override IServiceCollection AddInfrastructure()
	{
		return base.AddInfrastructure()
			.AddDatabase<MealPlanDbContext>();
	}
}