﻿using Infrastructure.Shared.Entities.Configurations;

using MealPlan.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MealPlan.Infrastructure.Entities.Configuration;

public class PlannedMealEntityTypeConfiguration : BaseEntityTypeConfiguration<PlannedMeal>
{
	public override void Configure(EntityTypeBuilder<PlannedMeal> builder)
	{
		base.Configure(builder);
		
		builder.Property(p => p.UserId)
			.IsRequired();

		builder.HasIndex(p => p.UserId)
			.HasDatabaseName(MealPlanIndexes.UserId);

		builder.Property(p => p.RecipeId)
			.IsRequired();
		
		builder.HasIndex(p => p.RecipeId)
			.HasDatabaseName(MealPlanIndexes.RecipeId);

		builder.Property(p => p.MealTypeId)
			.IsRequired();
		
		builder.HasIndex(p => p.MealTypeId)
			.HasDatabaseName(MealPlanIndexes.MealTypeId);

		builder.Property(p => p.Day)
			.IsRequired();
	}
}