﻿namespace MealPlan.Infrastructure.Entities.Configuration;

public static class MealPlanIndexes
{
	private const string Prefix = "IX_MealPlan";
	public const string UserId = $"{Prefix}_UserId";
	public const string RecipeId = $"{Prefix}_RecipeId";
	public const string MealTypeId = $"{Prefix}_MealTypeId";
}