﻿using Infrastructure.Shared;

using MealPlan.Domain.Entities;

using Microsoft.EntityFrameworkCore;

namespace MealPlan.Infrastructure;

public class MealPlanDbContext(DbContextOptions<MealPlanDbContext> options)
	: BaseDbContext(options, typeof(MealPlanDbContext).Assembly)
{
	public DbSet<PlannedMeal> PlannedMeals { get; set; }
}