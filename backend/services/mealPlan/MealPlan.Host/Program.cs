using Domain.Shared;

using Host.Shared;

using MealPlan.API;
using MealPlan.Domain;
using MealPlan.Infrastructure;

namespace MealPlan.Host;

public class Program
{
	private Program()
	{
		// This is a static class
	}
	
	public static async Task Main(string[] args)
	{
		var api = new Program<MealPlanApi>(args, (services, configuration) =>
		{
			services.AddSingleton<IMicroserviceNameProvider, MicroserviceNameProvider>();
			new InfrastructureConfigurator(services, configuration).AddInfrastructure();
		});

		await api.RunAsync();
	}
}