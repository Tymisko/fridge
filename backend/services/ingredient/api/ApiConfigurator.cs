﻿using API.Shared;

namespace Ingredient.API;

public class ApiConfigurator(IServiceCollection services) : BaseApiConfigurator<IngredientApi>(services)
{
	public override void AddApi()
	{
		base.AddApi();
		_services
			.AddAutoMapper(config =>
			{
				config.ConstructServicesUsing(type => ActivatorUtilities.CreateInstance(_services.BuildServiceProvider(), type));
			}, typeof(IngredientApi).Assembly);
	}
}