using Domain.Shared;

using Host.Shared;

using Ingredient.API;
using Ingredient.Domain;
using Ingredient.Infrastructure;

namespace Ingredient.Host;

public class Program
{
	private Program()
	{
		// This is a static class
	}

	public static async Task Main(string[] args)
	{
		var api = new Program<IngredientApi>(args, (services, configuration) =>
		{
			services.AddSingleton<IMicroserviceNameProvider, MicroserviceNameProvider>();
			new InfrastructureConfigurator(services, configuration).AddInfrastructure();
			new ApiConfigurator(services).AddApi();
		});
		await api.RunAsync();
	}
}