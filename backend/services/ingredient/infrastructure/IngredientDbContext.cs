﻿using Infrastructure.Shared;

using Microsoft.EntityFrameworkCore;

namespace Ingredient.Infrastructure;

public class IngredientDbContext(DbContextOptions<IngredientDbContext> options) : BaseDbContext(options, typeof(IngredientDbContext).Assembly)
{
	public DbSet<Ingredient.Domain.Entities.Ingredient> Ingredients { get; set; }
}