﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ingredient.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddIngredientEntity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Fat = table.Column<decimal>(type: "numeric", nullable: false),
                    Carbohydrates = table.Column<decimal>(type: "numeric", nullable: false),
                    Protein = table.Column<decimal>(type: "numeric", nullable: false),
                    Calories = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    Version = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                    table.CheckConstraint("CK_Ingredient_Calories", "\"Calories\" >= 0 AND \"Calories\" <= 900");
                    table.CheckConstraint("CK_Ingredient_Carbohydrates", "\"Carbohydrates\" >= 0 AND \"Carbohydrates\" <= 100");
                    table.CheckConstraint("CK_Ingredient_Fat", "\"Fat\" >= 0 AND \"Fat\" <= 100");
                    table.CheckConstraint("CK_Ingredient_Protein", "\"Protein\" >= 0 AND \"Protein\" <= 100");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Ingredient_Name",
                table: "Ingredients",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ingredient_UserId",
                table: "Ingredients",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ingredients");
        }
    }
}
