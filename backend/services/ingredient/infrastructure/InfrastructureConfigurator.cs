using Infrastructure.Shared;
using Infrastructure.Shared.Database;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ingredient.Infrastructure;

public class InfrastructureConfigurator(IServiceCollection services, IConfiguration configuration)
	: BaseInfrastructureConfigurator(services, configuration)
{
	public override IServiceCollection AddInfrastructure()
	{
		return base.AddInfrastructure()
			.AddDatabase<IngredientDbContext>();
	}
}