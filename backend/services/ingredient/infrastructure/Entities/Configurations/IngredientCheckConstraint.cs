﻿using Infrastructure.Shared.Entities.Configurations;

using Ingredient.Domain.Entities;

namespace Ingredient.Infrastructure.Entities.Configurations;

public static class IngredientCheckConstraints
{
	private const string Prefix = "CK_Ingredient";
	public static readonly EntityCheckConstraint Protein = new($"{Prefix}_Protein", $"\"Protein\" >= {IngredientValidationRules.ProteinMinValue} AND \"Protein\" <= {IngredientValidationRules.ProteinMaxValue}");
	public static readonly EntityCheckConstraint Fat = new($"{Prefix}_Fat", $"\"Fat\" >= {IngredientValidationRules.FatMinValue} AND \"Fat\" <= {IngredientValidationRules.FatMaxValue}");
	public static readonly EntityCheckConstraint Carbohydrates = new($"{Prefix}_Carbohydrates", $"\"Carbohydrates\" >= {IngredientValidationRules.CarbohydratesMinValue} AND \"Carbohydrates\" <= {IngredientValidationRules.CarbohydratesMaxValue}");
	public static readonly EntityCheckConstraint Calories = new($"{Prefix}_Calories", $"\"Calories\" >= {IngredientValidationRules.CaloriesMinValue} AND \"Calories\" <= {IngredientValidationRules.CaloriesMaxValue}");
}