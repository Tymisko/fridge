﻿namespace Ingredient.Infrastructure.Entities.Configurations;

public static class IngredientIndexes
{
	private const string Prefix = "IX_Ingredient";
	
	public const string Name = $"{Prefix}_Name";
	public const string UserId = $"{Prefix}_UserId";
}