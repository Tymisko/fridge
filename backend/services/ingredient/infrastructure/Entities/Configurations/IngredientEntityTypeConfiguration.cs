﻿using Infrastructure.Shared.Entities.Configurations;

using Ingredient.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ingredient.Infrastructure.Entities.Configurations;

public class IngredientEntityTypeConfiguration : BaseEntityTypeConfiguration<Domain.Entities.Ingredient>
{
	public override void Configure(EntityTypeBuilder<Domain.Entities.Ingredient> builder)
	{
		base.Configure(builder);

		builder.Property(i => i.Name)
			.IsRequired()
			.HasMaxLength(IngredientValidationRules.NameMaxLength)
			.IsUnicode(true);

		builder.HasIndex(i => i.Name)
			.HasDatabaseName(IngredientIndexes.Name)
			.IsUnique();

		builder.Property(i => i.Carbohydrates)
			.IsRequired();

		builder.Property(i => i.Protein)
			.IsRequired();

		builder.Property(i => i.Fat)
			.IsRequired();

		builder.Property(i => i.Calories)
			.IsRequired();

		builder.HasIndex(i => i.UserId)
			.HasDatabaseName(IngredientIndexes.UserId);

		builder.ToTable(t =>
		{
			t.HasCheckConstraint(IngredientCheckConstraints.Protein.Name, IngredientCheckConstraints.Protein.Sql);
			t.HasCheckConstraint(IngredientCheckConstraints.Fat.Name, IngredientCheckConstraints.Fat.Sql);
			t.HasCheckConstraint(
				IngredientCheckConstraints.Carbohydrates.Name,
				IngredientCheckConstraints.Carbohydrates.Sql
			);
			t.HasCheckConstraint(IngredientCheckConstraints.Calories.Name, IngredientCheckConstraints.Calories.Sql);
		});
	}
}