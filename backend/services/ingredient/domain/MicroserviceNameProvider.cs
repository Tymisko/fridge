﻿using Domain.Shared;

namespace Ingredient.Domain;

public class MicroserviceNameProvider : IMicroserviceNameProvider
{
	public string GetMicroserviceName()
	{
		return ServiceNames.Ingredient;
	}
}