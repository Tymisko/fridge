﻿using Domain.Shared.Entities;

namespace Ingredient.Domain.Entities;

public record Ingredient : BaseEntity
{
	public required Guid UserId { get; init; }
	public required string Name { get; init; }
	public required decimal Fat { get; init; }
	public required decimal Carbohydrates { get; init; }
	public required decimal Protein { get; init; }
	public required int Calories { get; init; }
}