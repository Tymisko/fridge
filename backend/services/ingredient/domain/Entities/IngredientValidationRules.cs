﻿namespace Ingredient.Domain.Entities;

public static class IngredientValidationRules
{
	public const int NameMaxLength = 100;
	public const int CaloriesMinValue = 0;
	public const int CaloriesMaxValue = 900;
	public const int CarbohydratesMinValue = 0;
	public const int CarbohydratesMaxValue = 100;
	public const int FatMinValue = 0;
	public const int FatMaxValue = 100;
	public const int ProteinMinValue = 0;
	public const int ProteinMaxValue = 100;
}
