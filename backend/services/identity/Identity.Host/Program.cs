﻿using Domain.Shared;

using Host.Shared;

using Identity.Api;
using Identity.Domain;
using Identity.Infrastructure;

namespace Identity.Host;

public class Program
{
	private Program()
	{
		// This is a static class
	}

	public static async Task Main(string[] args)
	{
		var api = new Program<IdentityApi>(args, (services, configuration) =>
		{
			services.AddSingleton<IMicroserviceNameProvider, MicroserviceNameProvider>();
			new InfrastructureConfigurator(services, configuration).AddInfrastructure();
			new ApiConfigurator(services).AddApi();
		});

		await api.RunAsync();
	}
}