﻿using Identity.Domain.Services;

using Isopoh.Cryptography.Argon2;

namespace Identity.Infrastructure.Services;

public class Argon2HashService : IHashService
{
	public string Hash(string rawPassword)
	{
		return Argon2.Hash(rawPassword);
	}

	public bool Verify(string rawPassword, string hashedPassword)
	{
		return Argon2.Verify(hashedPassword, rawPassword);
	}
}