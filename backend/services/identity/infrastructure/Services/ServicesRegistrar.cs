﻿using Identity.Domain.Services;

using Microsoft.Extensions.DependencyInjection;

namespace Identity.Infrastructure.Services;

public static class ServicesRegistrar
{
	public static IServiceCollection AddServices(this IServiceCollection services)
	{
		services.AddSingleton<IHashService, Argon2HashService>();
		return services;
	}
}