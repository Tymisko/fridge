using Identity.Infrastructure.Repositories;
using Identity.Infrastructure.Services;
using Infrastructure.Shared;
using Infrastructure.Shared.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Identity.Infrastructure;

public class InfrastructureConfigurator(IServiceCollection serviceCollection, IConfiguration configuration)
	: BaseInfrastructureConfigurator(serviceCollection, configuration)
{
	public override IServiceCollection AddInfrastructure()
	{
		return base.AddInfrastructure()
			.AddDatabase<IdentityDbContext>()
			.AddRepositories()
			.AddServices();
	}
}
