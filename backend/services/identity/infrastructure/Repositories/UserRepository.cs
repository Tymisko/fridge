using System.Linq.Expressions;

using Domain.Shared.Common;

using Identity.Domain.Entities;
using Identity.Domain.Errors;
using Identity.Domain.Repositories;

using Infrastructure.Shared.Errors;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Identity.Infrastructure.Repositories;

public class UserRepository(
	IdentityDbContext dbContext,
	ILogger<UserRepository> logger
) : IUserRepository
{
	public async Task<Result<Guid>> CreateAsync(User entity, CancellationToken cancellationToken)
	{
		await dbContext.Users.AddAsync(entity, cancellationToken);
		await dbContext.SaveChangesAsync(cancellationToken);
		return Result<Guid>.Success(entity.Id);
	}

	public async Task<Result<bool>> ExistsAsync(
		Guid id,
		CancellationToken cancellationToken
	)
	{
		var exists = await dbContext.Users.AsNoTracking().AnyAsync(u => u.Id.Equals(id), cancellationToken);
		return Result<bool>.Success(exists);
	}

	public async Task<Result<bool>> ExistsAsync(Expression<Func<User, bool>> predicate,
		CancellationToken cancellationToken)
	{
		var exists = await dbContext.Users.AsNoTracking().AnyAsync(predicate, cancellationToken);
		return Result<bool>.Success(exists);
	}

	public async Task<Result<User>> GetByIdAsync(Guid id, CancellationToken cancellationToken)
	{
		var user = await dbContext.Users
			.AsNoTracking()
			.SingleOrDefaultAsync(u => u.Id.Equals(id), cancellationToken);

		if (user is null)
		{
			return Result<User>.Failure(UserErrors.DoesNotExists(id));
		}

		return Result<User>.Success(user);
	}

	public async Task<Result> UpdateAsync(User entity, CancellationToken cancellationToken)
	{
		var existsResult = await ExistsAsync(entity.Id, cancellationToken);
		if (existsResult.IsFailure)
		{
			return existsResult.Error;
		}

		var exists = existsResult.Value;
		if (!exists)
		{
			return UserErrors.DoesNotExists(entity.Id);
		}

		var isAlreadyTracked = dbContext.IsTracked<User>(entity.Id);
		if (!isAlreadyTracked)
		{
			dbContext.Users.Attach(entity);
		}

		dbContext.Entry(entity).State = EntityState.Modified;

		try
		{
			await dbContext.SaveChangesAsync(cancellationToken);
		}
		catch (DbUpdateConcurrencyException ex)
		{
			var error = DatabaseErrors.UpdateConcurrencyConflict(nameof(User));
			logger.LogError(ex, error.Details);
			return error;
		}
		catch (Exception ex)
		{
			var error = DatabaseErrors.Unexpected;
			logger.LogError(ex, error.Details);
			return error;
		}

		return Result.Success();
	}

	public async Task<Result> DeleteAsync(Guid id, CancellationToken cancellationToken)
	{
		var existsResult = await ExistsAsync(id, cancellationToken);
		if (existsResult.IsFailure)
		{
			return existsResult.Error;
		}

		var exists = existsResult.Value;
		if (!exists)
		{
			return UserErrors.DoesNotExists(id);
		}

		var existingUser = await dbContext.Users.FindAsync(id);
		dbContext.Users.Remove(existingUser!);
		try
		{
			await dbContext.SaveChangesAsync(cancellationToken);
		}
		catch (DbUpdateConcurrencyException ex)
		{
			var error = DatabaseErrors.DeleteConcurrencyConflict(nameof(User));
			logger.LogError(ex, error.Details);
			return error;
		}
		catch (Exception ex)
		{
			var error = DatabaseErrors.Unexpected;
			logger.LogError(ex, error.Details);
			return error;
		}

		return Result.Success();
	}

	public async Task<Result<User>> GetByEmailAsync(string email, CancellationToken cancellationToken)
	{
		var lowerCaseEmail = email.ToLowerInvariant();
		var user = await dbContext.Users.AsNoTracking().SingleOrDefaultAsync(
			u => EF.Functions.Like(u.Email, lowerCaseEmail),
			cancellationToken
		);
		return user is null ? UserErrors.DoesNotExists(email) : Result<User>.Success(user);
	}

	public async Task<Result<bool>> WithEmailExistsAsync(string email, CancellationToken cancellationToken)
	{
		var lowerCaseEmail = email.ToLowerInvariant();
		var exists = await dbContext.Users.AsNoTracking()
			.AnyAsync(u =>
					EF.Functions.Like(u.Email, lowerCaseEmail),
				cancellationToken
			);

		return Result<bool>.Success(exists);
	}
}
