using Identity.Domain.Repositories;

using Microsoft.Extensions.DependencyInjection;

namespace Identity.Infrastructure.Repositories;

public static class RepositoriesConfigurator
{
	internal static IServiceCollection AddRepositories(this IServiceCollection services)
	{
		return services.AddScoped<IUserRepository, UserRepository>();
	}
}