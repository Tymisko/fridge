using Identity.Domain.Entities;

using Infrastructure.Shared.Entities.Configurations;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.Infrastructure.Entities.Configurations;

public class UserEntityTypeConfiguration : BaseEntityTypeConfiguration<User>
{
	public override void Configure(EntityTypeBuilder<User> builder)
	{
		base.Configure(builder);
		builder.Property(u => u.Email)
			.IsRequired()
			.HasMaxLength(UserValidationRules.EmailMaxLength)
			.IsUnicode(true);

		builder.HasIndex(u => u.Email)
			.HasDatabaseName(UserIndexes.Email)
			.IsUnique();
		
		builder.Property(u => u.Username)
			.IsRequired()
			.HasMaxLength(UserValidationRules.UsernameMaxLength)
			.IsUnicode(true);

		builder.HasIndex(u => u.Username)
			.HasDatabaseName(UserIndexes.Username)
			.IsUnique();

		builder.Property(u => u.Password)
			.IsRequired()
			.HasMaxLength(UserValidationRules.PasswordMaxLength);
	}
}
