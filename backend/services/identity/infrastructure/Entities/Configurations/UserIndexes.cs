﻿namespace Identity.Infrastructure.Entities.Configurations;

public static class UserIndexes
{
	private const string Prefix = "IX_User";

	public const string 
		Email = $"{Prefix}_Email",
		Username = $"{Prefix}_Username";
}
