using Identity.Domain.Entities;

using Infrastructure.Shared;

using Microsoft.EntityFrameworkCore;

namespace Identity.Infrastructure;

public class IdentityDbContext(DbContextOptions<IdentityDbContext> options) : BaseDbContext(options, typeof(IdentityDbContext).Assembly)
{
	public DbSet<User> Users { get; set; }
}