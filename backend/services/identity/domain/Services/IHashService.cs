﻿namespace Identity.Domain.Services;

public interface IHashService
{
	string Hash(string rawPassword);
	bool Verify(string rawPassword, string hashedPassword);
}