using System.Net;

using Domain.Shared.Common;

namespace Identity.Domain.Errors;

public static class UserErrors
{
	public static Error DoesNotExists(Guid id) =>
		new(HttpStatusCode.NotFound, $"User with id {id} does not exists");

	public static Error DoesNotExists(string email) =>
		new(HttpStatusCode.NotFound, $"User with email {email} does not exists");

	public static Error EmailAlreadyTaken(string email) =>
		new(HttpStatusCode.Conflict, $"An account with email {email} already exists.");
	
	public static Error UsernameAlreadyTaken(string username) => 
		new(HttpStatusCode.Conflict, $"Username {username} is already taken.");

	public static Error InvalidPassword() => new(HttpStatusCode.Unauthorized, "Invalid credentials");
}