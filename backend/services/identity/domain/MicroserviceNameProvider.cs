using Domain.Shared;

namespace Identity.Domain;

public class MicroserviceNameProvider : IMicroserviceNameProvider
{
	public string GetMicroserviceName()
	{
		return ServiceNames.Identity;
	}
}
