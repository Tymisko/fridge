using Domain.Shared.Common;
using Domain.Shared.Repositories;

using Identity.Domain.Entities;

namespace Identity.Domain.Repositories;

public interface IUserRepository : IBaseRepository<User>
{
	Task<Result<User>> GetByEmailAsync(string email, CancellationToken cancellationToken);
	Task<Result<bool>> WithEmailExistsAsync(string email, CancellationToken cancellationToken);
}