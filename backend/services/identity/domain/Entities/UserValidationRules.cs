using System.Text.RegularExpressions;

namespace Identity.Domain.Entities;

public static partial class UserValidationRules
{
	public const int EmailMaxLength = 320;  // RFC 5321
	public static readonly Regex EmailRegex = GeneratedEmailRegex(); 
	public const int UsernameMaxLength = 50;
	public const int UsernameMinLength = 3;
	public const int PasswordMinLength = 16;
	public const int PasswordMaxLength = 128;

	[GeneratedRegex("""^(?!\.)((?!\.\.)[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]|"[^"\\]*(?:\\.[^"\\]*)*")+(?<!\.)@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$""", RegexOptions.IgnoreCase)]
	private static partial Regex GeneratedEmailRegex(); // RFC 5322
}
