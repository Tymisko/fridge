using Domain.Shared.Entities;

namespace Identity.Domain.Entities;

public record User : BaseEntity
{
	public required string Username { get; set; }
	public required string Email { get; set; }
	public required string Password { get; set; }
}