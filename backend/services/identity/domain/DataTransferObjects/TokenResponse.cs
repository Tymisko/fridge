﻿namespace Identity.Domain.DataTransferObjects;

public record TokenResponse
{
	public required string Token { get; init; }
};