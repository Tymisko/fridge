namespace Identity.Domain.DataTransferObjects;

public record UpdateUserRequest
{
	public string? Username { get; init; }
	public string? Email { get; init; }
};