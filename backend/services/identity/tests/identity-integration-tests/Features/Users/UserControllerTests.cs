﻿using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;

using Bogus;

using FluentAssertions;

using Identity.Domain.DataTransferObjects;
using Identity.Domain.Entities;
using Identity.Domain.Services;
using Identity.Infrastructure;
using Identity.IntegrationTests.Core;
using Identity.IntegrationTests.Factories.User;

using Meziantou.Xunit;

using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json;

using Xunit.Abstractions;

namespace Identity.IntegrationTests.Features.Users;

[Collection(IntegrationTestCollection.CollectionName)]
[EnableParallelization]
public class UserControllerTests : BaseIntegrationTest
{
	private const string CreateUserRoute = "/api/v1/identity/users/create";
	private static string GetUserRoute(Guid id) => $"/api/v1/identity/users/{id}";
	private static string UpdateUserRoute(Guid id) => $"/api/v1/identity/users/{id}";
	private static string DeleteUserRoute(Guid id) => $"/api/v1/identity/users/{id}";
	private readonly CreateUserCommandFaker _createUserCommandFaker = new();
	private readonly IdentityDbContext _dbContext;
	private readonly ITestOutputHelper _outputHelper;
	private readonly Faker _faker = new();

	public UserControllerTests(IntegrationTestEnvironment testEnvironment, ITestOutputHelper outputHelper) : base(
		testEnvironment)
	{
		_outputHelper = outputHelper;
		_dbContext = _serviceProvider.GetRequiredService<IdentityDbContext>();
	}

	[Fact]
	public async Task CreateUser_WhenDataIsValid_ShouldReturnCreated()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();

		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);

		// Assert
		_outputHelper.WriteLine("Response content: {0}", await response.Content.ReadAsStringAsync());
		response.StatusCode.Should().Be(HttpStatusCode.Created);
	}

	[Fact]
	public async Task CreateUser_WhenDataIsValid_ShouldReturnCreatedUserId()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();

		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		response.EnsureSuccessStatusCode();

		// Assert
		var responseContent = await response.Content.ReadAsStringAsync();
		responseContent = responseContent.Trim('"');
		_outputHelper.WriteLine("Response content: {0}", responseContent);
		var userId = Guid.Parse(responseContent);
		userId.Should().NotBeEmpty();
	}

	[Fact]
	public async Task CreateUser_WhenDataIsValid_ShouldSaveUserDataToDatabase()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();

		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		response.EnsureSuccessStatusCode();

		// Assert
		var responseContent = await response.Content.ReadAsStringAsync();
		responseContent = responseContent.Trim('"');
		var userId = Guid.Parse(responseContent);
		var user = await _dbContext.Users.AsNoTracking().Where(u => u.Id.Equals(userId)).SingleOrDefaultAsync();
		user.Should().NotBeNull();
		user!.Email.Should().Be(createUserRequest.Email.ToLowerInvariant());
		user.Username.Should().Be(createUserRequest.Username.ToLowerInvariant());
		user.Password.Should().NotBeEmpty();
	}

	[Fact]
	public async Task CreateUser_WhenDataIsValid_ShouldSaveHashedPassword()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();

		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		response.EnsureSuccessStatusCode();

		// Assert
		var responseContent = await response.Content.ReadAsStringAsync();
		var userId = Guid.Parse(responseContent.Trim('"'));
		var user = await _dbContext.Users.AsNoTracking().Where(u => u.Id.Equals(userId)).SingleOrDefaultAsync();
		user.Should().NotBeNull();
		user!.Password.Should().NotBe(createUserRequest.Password);
		var hashService = _serviceProvider.GetRequiredService<IHashService>();
		hashService.Verify(createUserRequest.Password, user.Password).Should().BeTrue();
	}

	public static IEnumerable<object[]> InvalidEmailData()
	{
		yield return ["plainaddress"];
		yield return ["@missingusername.com"];
		yield return ["missingatsign.com"];
		yield return ["missingdomain@.com"];
		yield return ["missingdot@domain"];
		yield return ["missingextension@domain."];
		yield return ["invalid@characters!.com"];
		yield return ["invalid@space inaddress.com"];
		yield return ["double@@domain.com"];
		yield return ["missing@domain..com"];
		yield return [".leadingdot@domain.com"];
		yield return ["trailingdot@domain.com."];
		yield return ["double..dots@domain.com"];
		yield return ["no_tld@domain"];
		yield return ["specialchar@domain..com"];
		yield return ["@domain.com"];
		yield return ["user@.domain.com"];
		yield return ["user@domain,com"];
	}

	[Theory]
	[MemberData(nameof(InvalidEmailData))]
	public async Task CreateUser_EmailIsInvalid_ShouldReturnBadRequest(string email)
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();
		createUserRequest = createUserRequest with { Email = email };

		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.BadRequest,
			$"The email {email} is invalid and should not be accepted.");
	}

	public static IEnumerable<object[]> ValidEmailAddresses()
	{
		yield return ["john.doe@example.com"];
		yield return ["jane_doe123@example.co.uk"];
		yield return ["\"first.last\"@example.org"];
		yield return ["user+tag@example.io"];
		yield return ["\"special_chars!#$%&'*+/=?^_`{|}~\"@example.com"];
		yield return ["name.surname@sub.example.org"];
		yield return ["user.name+label@example-mail.com"];
		yield return ["\"quoted.name\"@example.name"];
		yield return ["contact@my-domain.info"];
		yield return ["underscores_are_valid@example.museum"];
	}

	[Theory]
	[MemberData(nameof(ValidEmailAddresses))]
	public async Task CreateUser_EmailIsValid_ShouldReturn201Created(string email)
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();
		createUserRequest = createUserRequest with { Email = email };

		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		var content = await response.Content.ReadAsStringAsync();

		// Assert
		bool isValid = response.StatusCode == HttpStatusCode.Created ||
		               !content.Contains(nameof(User.Email));

		isValid.Should()
			.BeTrue(
				$"For email {email}: Either the status code should be Created or the content does should not contain {nameof(User.Email)}");
	}

	[Fact]
	public async Task CreateUser_UsernameAlreadyTaken_ShouldReturn409Conflict()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();
		await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		var createSecondUserRequest = createUserRequest with { Email = _faker.Internet.Email() };

		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createSecondUserRequest);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.Conflict);
		var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
		problemDetails.Should().NotBeNull();
		problemDetails!.Detail.Should().Contain($"Username {createUserRequest.Username} is already taken.");
	}

	[Fact]
	public async Task CreateUser_EmailAlreadyTaken_ShouldReturn409Conflict()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();
		await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		var createSecondUserRequest = createUserRequest with { Username = _faker.Internet.UserName() };
		
		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createSecondUserRequest);
		
		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.Conflict);
		var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
		problemDetails.Should().NotBeNull();
		problemDetails!.Detail.Should().Contain($"An account with email {createUserRequest.Email} already exists.");
	}

	[Fact]
	public async Task GetUser_WhenUserExists_ShouldReturnOkWithUserData()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();
		var createResponse = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		var userId = Guid.Parse((await createResponse.Content.ReadAsStringAsync()).Trim('"'));

		// Act
		var route = GetUserRoute(userId);
		var response = await _client.GetAsync(route);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.OK);
		var user = await response.Content.ReadFromJsonAsync<UserResponse>();
		user.Should().NotBeNull();
		user!.Id.Should().Be(userId);
		user.Email.Should().Be(createUserRequest.Email.ToLowerInvariant());
		user.Username.Should().Be(createUserRequest.Username.ToLowerInvariant());
	}

	[Fact]
	public async Task GetUser_WhenUserDoesNotExist_ShouldReturnNotFound()
	{
		// Arrange
		var nonExistentUserId = Guid.NewGuid();

		// Act
		var route = GetUserRoute(nonExistentUserId);
		var response = await _client.GetAsync(route);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.NotFound);
	}

	[Fact]
	public async Task UpdateUser_WhenUserExists_ShouldUpdateAndReturnNoContent()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();
		var createResponse = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		var userId = Guid.Parse((await createResponse.Content.ReadAsStringAsync()).Trim('"'));

		var patchDoc = new JsonPatchDocument<UpdateUserRequest>();
		var newUsername = "updated_username";
		patchDoc.Replace(x => x.Username, newUsername);

		// Act
		var route = UpdateUserRoute(userId);
		var response = await _client.PatchAsync(
			route,
			new StringContent(
				JsonConvert.SerializeObject(patchDoc),
				Encoding.UTF8,
				new MediaTypeHeaderValue(MediaType.JsonPatch)
			)
		);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.NoContent);

		// Verify the update in database
		var updatedUser = await _dbContext.Users
			.AsNoTracking()
			.FirstOrDefaultAsync(u => u.Id == userId);
		updatedUser.Should().NotBeNull();
		updatedUser!.Username.Should().Be(newUsername.ToLowerInvariant());
	}

	[Fact]
	public async Task UpdateUser_WhenUserDoesNotExist_ShouldReturnNotFound()
	{
		// Arrange
		var nonExistentUserId = Guid.NewGuid();
		var patchDoc = new JsonPatchDocument<UpdateUserRequest>();
		patchDoc.Replace(x => x.Username, "new_username");

		// Act
		var route = UpdateUserRoute(nonExistentUserId);
		var response = await _client.PatchAsync(
			route,
			new StringContent(
				JsonConvert.SerializeObject(patchDoc),
				Encoding.UTF8,
				new MediaTypeHeaderValue(MediaType.JsonPatch)
			)
		);
		_outputHelper.WriteLine(await response.Content.ReadAsStringAsync());

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.NotFound);
	}

	[Fact]
	public async Task DeleteUser_WhenUserExists_ShouldDeleteAndReturnNoContent()
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();
		var createResponse = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		var userId = Guid.Parse((await createResponse.Content.ReadAsStringAsync()).Trim('"'));

		// Act
		var route = DeleteUserRoute(userId);
		var response = await _client.DeleteAsync(route);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.NoContent);

		// Verify user is deleted from database
		var deletedUser = await _dbContext.Users
			.AsNoTracking()
			.FirstOrDefaultAsync(u => u.Id == userId);
		deletedUser.Should().BeNull();
	}

	[Fact]
	public async Task DeleteUser_WhenUserDoesNotExist_ShouldReturnNotFound()
	{
		// Arrange
		var nonExistentUserId = Guid.NewGuid();

		// Act
		var route = DeleteUserRoute(nonExistentUserId);
		var response = await _client.DeleteAsync(route);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.NotFound);
	}

	public static IEnumerable<object[]> InvalidPasswordData()
	{
		yield return new object[] { "nouppercasechar123!", "Password must contain at least one uppercase letter." };
		yield return new object[] { "NOLOWERCASECHAR123!", "Password must contain at least one lowercase letter." };
		yield return new object[] { "NoSpecialCharacter123", "Password must contain at least one special character." };
		yield return new object[] { "NoDigitsHere!@#$%", "Password must contain at least one digit." };
	}

	[Theory]
	[MemberData(nameof(InvalidPasswordData))]
	public async Task CreateUser_WhenPasswordMissingRequiredCharacters_ShouldReturnBadRequest(string password, string expectedError)
	{
		// Arrange
		var createUserRequest = _createUserCommandFaker.Generate();
		createUserRequest = createUserRequest with { Password = password };

		// Act
		var response = await _client.PostAsJsonAsync(CreateUserRoute, createUserRequest);
		var content = await response.Content.ReadAsStringAsync();

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
		content.Should().Contain(expectedError);
		_outputHelper.WriteLine($"Password: {password}");
		_outputHelper.WriteLine($"Response: {content}");
	}
}
