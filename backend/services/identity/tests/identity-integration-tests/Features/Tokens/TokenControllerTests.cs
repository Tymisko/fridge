﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;

using API.Shared.Features.Authentication.Options;

using FluentAssertions;

using Identity.Api.Features.Tokens;
using Identity.Api.Features.Tokens.Commands;
using Identity.Domain.DataTransferObjects;
using Identity.IntegrationTests.Factories.User;
using Identity.IntegrationTests.Services;

using Meziantou.Xunit;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using Xunit.Abstractions;

namespace Identity.IntegrationTests.Features.Tokens;

[Collection(IntegrationTestCollection.CollectionName)]
[EnableParallelization]
public class TokenControllerTests : BaseIntegrationTest
{
	private readonly CreateUserCommandFaker _createUserCommandFaker = new();
	private readonly ITestOutputHelper _outputHelper;
	private readonly UserService _userService;
	private readonly TokenService _tokenService;
	private readonly JwtOptions _jwtOptions;

	public TokenControllerTests(IntegrationTestEnvironment testEnvironment, ITestOutputHelper outputHelper) : base(
		testEnvironment)
	{
		_outputHelper = outputHelper;
		_jwtOptions = _serviceProvider.GetRequiredService<IOptions<JwtOptions>>().Value;
		_userService = new UserService(_client, outputHelper);
		_tokenService = new TokenService(_client, outputHelper);
	}


	private const string GenerateTokenRoute = "api/v1/identity/tokens";
	private const string ValidateTokenRoute = "api/v1/identity/tokens/validate";

	[Fact]
	public async Task GenerateToken_WhenUserExists_ReturnsToken()
	{
		// Arrange
		var createUserCommand = _createUserCommandFaker.Generate();
		await _userService.CreateUserAsync(createUserCommand);
		var request = new GenerateJwtToken(createUserCommand.Email, createUserCommand.Password);

		// Act
		var response = await _client.PostAsJsonAsync(GenerateTokenRoute, request);

		// Assert
		var responseContent = await response.Content.ReadAsStringAsync();
		_outputHelper.WriteLine(responseContent);

		response.StatusCode.Should().Be(HttpStatusCode.OK);
		var token = responseContent;
		token.Should().NotBeNullOrWhiteSpace();
	}

	[Fact]
	public async Task GenerateToken_WhenUserExists_TokenHasCorrectClaims()
	{
		// Arrange
		var createUserCommand = _createUserCommandFaker.Generate();
		var userId = await _userService.CreateUserAsync(createUserCommand);
		var request = new GenerateJwtToken(createUserCommand.Email, createUserCommand.Password);

		// Act
		var response = await _client.PostAsJsonAsync(GenerateTokenRoute, request);
		var tokenResponse = await response.Content.ReadFromJsonAsync<TokenResponse?>();

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.OK);
		tokenResponse.Should().NotBeNull();
		var token = tokenResponse!.Token;
		token.Should().NotBeNullOrWhiteSpace();

		var tokenHandler = new JwtSecurityTokenHandler();
		var jwtToken = tokenHandler.ReadJwtToken(token);

		jwtToken.Issuer.Should().Be(_jwtOptions.Issuer);
		jwtToken.Audiences.Should().Contain(_jwtOptions.Audience);

		var claims = jwtToken.Claims.ToDictionary(c => c.Type, c => c.Value);
		claims.Should().ContainKey("sub");
		claims["sub"].Should().Be(userId.ToString());
		claims.Should().ContainKey("email");
		claims["email"].Should().Be(createUserCommand.Email.ToLowerInvariant());

		jwtToken.ValidTo.Should().BeAfter(DateTime.UtcNow);
		jwtToken.ValidFrom.Should().BeBefore(DateTime.UtcNow);
	}

	[Fact]
	public async Task GenerateToken_WhenUserDoesNotExists_ReturnsNotFound()
	{
		// Arrange
		var request = new GenerateJwtToken("nonexistent@example.com", "password123");

		// Act
		var response = await _client.PostAsJsonAsync(GenerateTokenRoute, request);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.NotFound);
	}

	[Fact]
	public async Task GenerateToken_WhenInvalidCredentials_ReturnsUnauthorized()
	{
		// Arrange
		var createUserCommand = _createUserCommandFaker.Generate();
		await _userService.CreateUserAsync(createUserCommand);
		var request = new GenerateJwtToken(createUserCommand.Email, "wrongPassword123!");

		// Act
		var response = await _client.PostAsJsonAsync(GenerateTokenRoute, request);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
		var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
		problemDetails.Should().NotBeNull();
		problemDetails!.Status.Should().Be((int)HttpStatusCode.Unauthorized);
	}

	[Fact]
	public async Task ValidateToken_WhenTokenIsValid_ReturnsOk()
	{
		// Arrange
		var createUserCommand = _createUserCommandFaker.Generate();
		await _userService.CreateUserAsync(createUserCommand);
		var generateTokenRequest = new GenerateJwtToken(createUserCommand.Email, createUserCommand.Password);
		var token = await _tokenService.GenerateTokenAsync(generateTokenRequest);

		_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Trim('"'));

		// Act
		var response = await _client.GetAsync(ValidateTokenRoute);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.OK);
	}

	[Fact]
	public async Task ValidateToken_WhenTokenExpired_ReturnsUnauthorized()
	{
		// Arrange
		var createUserCommand = _createUserCommandFaker.Generate();
		await _userService.CreateUserAsync(createUserCommand);
		var generateTokenRequest = new GenerateJwtToken(createUserCommand.Email, createUserCommand.Password);
		var token = await _tokenService.GenerateTokenAsync(generateTokenRequest);
		var expiredToken = MakeTokenBeingExpired(token);
		_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", expiredToken);

		// Act
		var response = await _client.GetAsync(ValidateTokenRoute);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
		var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
		problemDetails.Should().NotBeNull();
		problemDetails!.Status.Should().Be((int)HttpStatusCode.Unauthorized);
		problemDetails.Detail.Should().Be(TokenErrors.TokenExpired.Details);
	}

	private string MakeTokenBeingExpired(string token)
	{
		var handler = new JwtSecurityTokenHandler();
		var securityToken = handler.ReadJwtToken(token);

		var now = DateTime.UtcNow;
		var expiredToken = new JwtSecurityToken(
			issuer: securityToken.Issuer,
			audience: securityToken.Audiences.First(),
			claims: securityToken.Claims,
			notBefore: now.AddMinutes(-30), // Token became valid 30 minutes ago
			expires: now.AddMinutes(-1), // Token expired 1 minute ago
			signingCredentials: new SigningCredentials(
				new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.SecretKey)),
				SecurityAlgorithms.HmacSha256
			)
		);
		return handler.WriteToken(expiredToken);
	}

	[Fact]
	public async Task ValidateToken_WhenTokenDoesNotStartWithValidScheme_ReturnsUnauthorized()
	{
		// Arrange
		var createUserCommand = _createUserCommandFaker.Generate();
		await _userService.CreateUserAsync(createUserCommand);
		var generateTokenRequest = new GenerateJwtToken(createUserCommand.Email, createUserCommand.Password);
		var token = await _tokenService.GenerateTokenAsync(generateTokenRequest);
		_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token.Trim('"'));

		// Act
		var response = await _client.GetAsync(ValidateTokenRoute);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
		var problemDetails = await response.Content.ReadFromJsonAsync<ProblemDetails>();
		problemDetails.Should().NotBeNull();
		problemDetails!.Status.Should().Be((int)HttpStatusCode.Unauthorized);
		problemDetails.Detail.Should().Be(TokenErrors.InvalidTokenScheme.Details);
	}
}