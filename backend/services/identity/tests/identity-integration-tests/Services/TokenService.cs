﻿using System.Net.Http.Headers;
using System.Net.Http.Json;

using FluentAssertions;

using Identity.Api.Features.Tokens.Commands;
using Identity.Domain.DataTransferObjects;

using Xunit.Abstractions;

namespace Identity.IntegrationTests.Services;

internal class TokenService(HttpClient httpClient, ITestOutputHelper testOutputHelper)
{
	private const string GenerateTokenRoute = "api/v1/identity/tokens";
	private const string ValidateTokenRoute = "api/v1/identity/tokens/validate";

	public async Task<string> GenerateTokenAsync(GenerateJwtToken request)
	{
		var response = await httpClient.PostAsJsonAsync(GenerateTokenRoute, request);
		
		if (!response.IsSuccessStatusCode)
		{
			var content = await response.Content.ReadAsStringAsync();
			testOutputHelper.WriteLine(content);
			throw new HttpRequestException($"Failed to generate token. Status: {response.StatusCode}");
		}

		var tokenResponse = await response.Content.ReadFromJsonAsync<TokenResponse>();
		tokenResponse.Should().NotBeNull();
		return tokenResponse!.Token;
	}

	public async Task<bool> ValidateTokenAsync(string token)
	{
		SetAuthorizationHeader("Bearer", token);
		var response = await httpClient.GetAsync(ValidateTokenRoute);
		
		if (!response.IsSuccessStatusCode)
		{
			var content = await response.Content.ReadAsStringAsync();
			testOutputHelper.WriteLine(content);
		}
		
		ClearAuthorizationHeader();
		return response.IsSuccessStatusCode;
	}

	private void SetAuthorizationHeader(string scheme, string token)
	{
		httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(scheme, token);
	}

	public void ClearAuthorizationHeader()
	{
		httpClient.DefaultRequestHeaders.Authorization = null;
	}
}
