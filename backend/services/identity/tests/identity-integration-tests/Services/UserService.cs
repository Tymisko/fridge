﻿using System.Net.Http.Json;

using Identity.Api.Features.Users.Commands;

using Xunit.Abstractions;

namespace Identity.IntegrationTests.Services;

public class UserService(HttpClient httpClient, ITestOutputHelper testOutputHelper)
{
	private const string CreateUserRoute = "/api/v1/identity/users/create";

	public async Task<Guid> CreateUserAsync(CreateUser createUser)
	{
		var response = await httpClient.PostAsJsonAsync(CreateUserRoute, createUser);
		
		if (!response.IsSuccessStatusCode)
		{
			var content = await response.Content.ReadAsStringAsync();
			testOutputHelper.WriteLine(content);
			throw new HttpRequestException("The HTTP response is unsuccessful");
		}

		var id = await response.Content.ReadFromJsonAsync<Guid>();
		return id;
	}
}