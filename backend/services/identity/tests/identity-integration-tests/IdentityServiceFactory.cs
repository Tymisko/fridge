﻿using DotNetEnv;

using Identity.Host;
using Identity.IntegrationTests.Core.Logging;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Identity.IntegrationTests;

public class IdentityServiceFactory : WebApplicationFactory<Program>
{
	private static string CurrentDirectory => AppContext.BaseDirectory;
	
	protected override void ConfigureWebHost(IWebHostBuilder builder)
	{
		base.ConfigureWebHost(builder);
		builder.UseEnvironment(Environments.Production);
		builder.UseContentRoot(CurrentDirectory);
		builder.ConfigureAppConfiguration(SetupAppConfiguration);
		builder.ConfigureServices(SetupServices);
		var envFilePath = Path.Combine(Directory.GetCurrentDirectory(), "../../../../../../../../.env");
		Env.NoClobber().Load(envFilePath);
	}

	private static void SetupAppConfiguration(IConfigurationBuilder configurationBuilder)
	{
		configurationBuilder
			.SetBasePath(CurrentDirectory)
			.AddEnvironmentVariables();
	}

	private static void SetupServices(IServiceCollection services)
	{
		services.SetupFileLogging();
	}
}