﻿using Identity.Infrastructure;

using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Identity.IntegrationTests;

// ReSharper disable once ClassNeverInstantiated.Global
public sealed class IntegrationTestEnvironment : IAsyncLifetime
{
	private readonly TestServer _server;
	private readonly IServiceScope _serviceScope;
	private readonly IServiceProvider _serviceProvider;
	private readonly ILogger<IntegrationTestEnvironment> _logger;

	public IntegrationTestEnvironment()
	{
		var application = new IdentityServiceFactory();
		_server = application.Server;
		_server.PreserveExecutionContext = true;
		_serviceScope = _server.Services.CreateScope();
		_serviceProvider = _serviceScope.ServiceProvider;
		_logger = _serviceProvider.GetRequiredService<ILogger<IntegrationTestEnvironment>>();
	}

	public async Task InitializeAsync()
	{
		var hasPendingMigrations = await HasPendingMigrationsAsync();
		if (hasPendingMigrations)
		{
			_logger.LogInformation("Pending migrations found - applying migrations");
			await ApplyDatabaseMigrationsAsync();
		}

		await CleanUpDatabaseAsync();
	}

	public async Task DisposeAsync()
	{
		_serviceScope.Dispose();
		_server.Dispose();
		await Task.CompletedTask;
	}

	public HttpClient CreateClient() => _server.CreateClient();
	public IServiceScope CreateServiceScope() => _server.Services.CreateScope();
	
	private async Task<bool> HasPendingMigrationsAsync()
	{
		var dbContext = _serviceProvider.GetRequiredService<IdentityDbContext>();
		IEnumerable<string> pendingMigrations;
		try
		{
			pendingMigrations = await dbContext.Database.GetPendingMigrationsAsync();
		}
		catch (Exception ex)
		{
			_logger.LogCritical("Error while checking for pending migrations {Message}", ex.Message);
			return true;
		}

		return pendingMigrations.Any();
	}

	private async Task ApplyDatabaseMigrationsAsync()
	{
		try
		{
			var dbContext = _serviceProvider.GetRequiredService<IdentityDbContext>();
			await dbContext.Database.MigrateAsync();
		}
		catch (Exception ex)
		{
			_logger.LogCritical("Error while migrating database: {Message}", ex.Message);
		}
	}

	private async Task CleanUpDatabaseAsync()
	{
		try
		{
			var dbContext = _serviceProvider.GetRequiredService<IdentityDbContext>();
			await dbContext.Users.ExecuteDeleteAsync();
			await dbContext.DisposeAsync();
		}
		catch (Exception ex)
		{
			_logger.LogCritical("Error while cleaning up database: {Message}", ex.Message);
		}
	}
}