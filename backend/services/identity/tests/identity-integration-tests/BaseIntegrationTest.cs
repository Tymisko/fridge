﻿using Microsoft.Extensions.DependencyInjection;

namespace Identity.IntegrationTests;

public abstract class BaseIntegrationTest : IDisposable
{
	protected readonly HttpClient _client;
	private readonly IServiceScope _serviceScope;
	protected readonly IServiceProvider _serviceProvider;
	private bool _disposed;

	protected BaseIntegrationTest(IntegrationTestEnvironment testEnvironment)
	{
		_client = testEnvironment.CreateClient();
		_serviceScope = testEnvironment.CreateServiceScope();
		_serviceProvider = _serviceScope.ServiceProvider;
	}

	public void Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
	}

	~BaseIntegrationTest()
	{
		Dispose(false);
	}

	protected virtual void Dispose(bool disposing)
	{
		if (_disposed) return;

		if (!disposing) return;
		
		_client.Dispose();
		_serviceScope.Dispose();

		_disposed = true;
	}
}