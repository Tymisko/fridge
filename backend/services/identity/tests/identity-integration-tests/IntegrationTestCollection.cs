﻿using Meziantou.Xunit;

namespace Identity.IntegrationTests;

[CollectionDefinition(CollectionName)]
[EnableParallelization]
public class IntegrationTestCollection : ICollectionFixture<IntegrationTestEnvironment>
{
	public const string CollectionName = "IntegrationTestCollection";
}