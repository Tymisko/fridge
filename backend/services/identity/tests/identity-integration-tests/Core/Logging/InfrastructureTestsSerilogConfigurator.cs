﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using Serilog;

namespace Identity.IntegrationTests.Core.Logging;

public static class InfrastructureTestsSerilogConfigurator
{
	public static void SetupFileLogging(this IServiceCollection services)
	{
		services.AddSerilog((servicesProvider, loggerConfigurator) =>
		{
			var folderPath = GetLogsFolderPath(servicesProvider);
			const string fromOutputToProject = "../../../";
			folderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fromOutputToProject, folderPath);
			loggerConfigurator
				.MinimumLevel.Debug()
				.Enrich.FromLogContext()
				.WriteTo.File(
					folderPath,
					rollingInterval: RollingInterval.Hour,
					outputTemplate: "{Timestamp:HH:mm:ss.fff} {ActionId} [{Level:u3}] {SourceContext} {Message:lj}{NewLine}{Exception}"
				);
		});
	}

	private static string GetLogsFolderPath(IServiceProvider serviceProvider)
	{
		const string logsFolderPathConfigurationKey = "Logging:FileLogging:FilePath";
		var configuration = serviceProvider.GetRequiredService<IConfiguration>();
		var folderPath = configuration.GetValue<string?>(logsFolderPathConfigurationKey);
		if (string.IsNullOrWhiteSpace(folderPath))
		{
			throw new OptionsValidationException(
				nameof(folderPath),
				typeof(string),
				[$"$The {logsFolderPathConfigurationKey} is missing or empty."]
			);
		}

		return folderPath;
	}
}