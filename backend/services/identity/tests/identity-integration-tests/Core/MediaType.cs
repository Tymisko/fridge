﻿namespace Identity.IntegrationTests.Core;

public static class MediaType
{
	public const string JsonPatch = "application/json-patch+json";
	public const string Json = "application/json";
}