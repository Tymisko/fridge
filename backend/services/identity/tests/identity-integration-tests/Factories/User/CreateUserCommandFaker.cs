﻿using Bogus;

using Identity.Api.Features.Users.Commands;
using Identity.Domain.Entities;

namespace Identity.IntegrationTests.Factories.User;

public sealed class CreateUserCommandFaker : Faker<CreateUser>
{
	public CreateUserCommandFaker()
	{
		RuleFor(u => u.Username, f => f.Internet.UserName());
		RuleFor(u => u.Email, f => f.Person.Email);
		RuleFor(u => u.Password, f =>
		{
			// Generate a password that meets all requirements
			var length = f.Random.Int(UserValidationRules.PasswordMinLength, UserValidationRules.PasswordMaxLength);
			var lowercase = f.Random.String2(1, 1);
			var uppercase = f.Random.String2(1, 1, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			var digit = f.Random.String2(1, 1, "0123456789");
			var special = f.Random.String2(1, 1, "!@#$%^&*()_+-=[]{}|;:,.<>?");
			
			var remainingLength = length - 4; // subtract the 4 required characters
			var remainingChars = f.Random.String2(remainingLength, remainingLength, 
				"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+-=[]{}|;:,.<>?");

			// Combine all parts and shuffle
			var password = lowercase + uppercase + digit + special + remainingChars;
			return new string(password.ToCharArray().OrderBy(x => f.Random.Int()).ToArray());
		});
	}
}
