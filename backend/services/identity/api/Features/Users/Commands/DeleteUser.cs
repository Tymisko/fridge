using Domain.Shared.Common;

using Identity.Domain.Errors;
using Identity.Domain.Repositories;

using MediatR;

namespace Identity.Api.Features.Users.Commands;

public record DeleteUser(Guid Id) : IRequest<Result>;

public class DeleteUserHandler(IUserRepository userRepository) : IRequestHandler<DeleteUser, Result>
{
	public async Task<Result> Handle(DeleteUser request, CancellationToken cancellationToken)
	{
		var existsResult = await userRepository.ExistsAsync(request.Id, cancellationToken);
		if (existsResult.IsFailure)
		{
			return existsResult.Error;
		}

		var exists = existsResult.Value;
		if (!exists)
		{
			return UserErrors.DoesNotExists(request.Id);
		}

		var deleteResult = await userRepository.DeleteAsync(request.Id, cancellationToken);
		if (deleteResult.IsFailure)
		{
			return deleteResult.Error;
		}

		return Result.Success();
	}
}