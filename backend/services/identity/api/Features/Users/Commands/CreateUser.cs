using System.Linq.Expressions;

using AutoMapper;

using Domain.Shared.Common;

using Identity.Domain.Entities;
using Identity.Domain.Errors;
using Identity.Domain.Repositories;

using MediatR;

namespace Identity.Api.Features.Users.Commands;

public record CreateUser : IRequest<Result<Guid>>
{
	public required string Username { get; init; }
	public required string Email { get; init; }
	public required string Password { get; init; }
};

public class CreateUserHandler(IUserRepository userRepository, IMapper mapper)
	: IRequestHandler<CreateUser, Result<Guid>>
{
	public async Task<Result<Guid>> Handle(CreateUser request, CancellationToken cancellationToken)
	{
		var lowercaseEmail = request.Email.ToLowerInvariant();
		Expression<Func<User, bool>> emailAlreadyTakenPredicate = user => user.Email.Equals(lowercaseEmail);
		var emailAlreadyTakenResult = await userRepository.ExistsAsync(emailAlreadyTakenPredicate, cancellationToken);
		if (emailAlreadyTakenResult.IsFailure)
		{
			return emailAlreadyTakenResult.Error;
		}

		var emailAlreadyTaken = emailAlreadyTakenResult.Value;
		if (emailAlreadyTaken)
		{
			return UserErrors.EmailAlreadyTaken(request.Email);
		}

		var lowerCaseUsername = request.Username.ToLowerInvariant();
		Expression<Func<User, bool>> usernameAlreadyTakenPredicate = user => user.Username.Equals(lowerCaseUsername);
		var usernameAlreadyTakenResult =
			await userRepository.ExistsAsync(usernameAlreadyTakenPredicate, cancellationToken);
		if (usernameAlreadyTakenResult.IsFailure)
		{
			return usernameAlreadyTakenResult.Error;
		}

		var usernameAlreadyTaken = usernameAlreadyTakenResult.Value;
		if (usernameAlreadyTaken)
		{
			return UserErrors.UsernameAlreadyTaken(request.Username);
		}

		var user = mapper.Map<User>(request);
		var createResult = await userRepository.CreateAsync(user, cancellationToken);
		if (createResult.IsFailure)
		{
			return createResult.Error;
		}

		return Result<Guid>.Success(createResult.Value);
	}
}