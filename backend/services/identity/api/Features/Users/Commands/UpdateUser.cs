using AutoMapper;

using Domain.Shared.Common;

using Identity.Domain.DataTransferObjects;
using Identity.Domain.Entities;
using Identity.Domain.Errors;
using Identity.Domain.Repositories;

using MediatR;

using Microsoft.AspNetCore.JsonPatch;

namespace Identity.Api.Features.Users.Commands;

public record UpdateUser(Guid Id, JsonPatchDocument<UpdateUserRequest> PatchDocument) : IRequest<Result<Guid>>;

public class UpdateUserHandler(IUserRepository userRepository, IMapper mapper) : IRequestHandler<UpdateUser,
	Result<Guid>>
{
	public async Task<Result<Guid>> Handle(UpdateUser request, CancellationToken cancellationToken)
	{
		var existsResult = await userRepository.ExistsAsync(request.Id, cancellationToken);
		if (existsResult.IsFailure)
		{
			return existsResult.Error;
		}

		var exists = existsResult.Value;
		if (!exists)
		{
			return UserErrors.DoesNotExists(request.Id);
		}

		var userResult = await userRepository.GetByIdAsync(request.Id, cancellationToken);
		if (userResult.IsFailure)
		{
			return userResult.Error;
		}

		var user = userResult.Value!;
		var patchDoc = mapper.Map<JsonPatchDocument<User>>(request.PatchDocument);
		patchDoc.ApplyTo(user);
		var updateResult = await userRepository.UpdateAsync(user, cancellationToken);
		if (updateResult.IsFailure)
		{
			return updateResult.Error;
		}

		return Result<Guid>.Success(request.Id);
	}
}