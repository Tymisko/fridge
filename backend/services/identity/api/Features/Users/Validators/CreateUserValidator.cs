﻿using FluentValidation;

using Identity.Api.Features.Users.Commands;
using Identity.Domain.Entities;

namespace Identity.Api.Features.Users.Validators;

public class CreateUserValidator : AbstractValidator<CreateUser>
{
	public CreateUserValidator()
	{
		RuleFor(u => u.Email)
			.NotEmpty()
			.Matches(UserValidationRules.EmailRegex).WithMessage("Please provide a valid email address.");

		RuleFor(u => u.Username)
			.MinimumLength(UserValidationRules.UsernameMinLength)
			.WithMessage($"Username must be at least {UserValidationRules.UsernameMinLength} characters long.")
			.MaximumLength(UserValidationRules.UsernameMaxLength)
			.WithMessage($"Username must not exceed {UserValidationRules.UsernameMaxLength} characters.");

		RuleFor(u => u.Password)
			.NotEmpty().WithMessage("Password is required.")
			.MinimumLength(UserValidationRules.PasswordMinLength)
			.WithMessage($"Password must be at least {UserValidationRules.PasswordMinLength} characters long.")
			.MaximumLength(UserValidationRules.PasswordMaxLength)
			.WithMessage($"Password must not exceed {UserValidationRules.PasswordMaxLength} characters.")
			.Matches("[A-Z]").WithMessage("Password must contain at least one uppercase letter.")
			.Matches("[a-z]").WithMessage("Password must contain at least one lowercase letter.")
			.Matches("[0-9]").WithMessage("Password must contain at least one digit.")
			.Matches("[^a-zA-Z0-9\\s]").WithMessage("Password must contain at least one special character.");
	}
}
