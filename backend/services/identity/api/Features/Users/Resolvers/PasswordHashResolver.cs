using AutoMapper;

using Identity.Api.Features.Users.Commands;
using Identity.Domain.Entities;
using Identity.Domain.Services;

namespace Identity.Api.Features.Users.Resolvers;

public class PasswordHashResolver(IHashService hashService) : IValueResolver<CreateUser, User, string>
{
	public string Resolve(CreateUser source, User destination, string destMember, ResolutionContext context)
    {
        return hashService.Hash(source.Password);
    }
} 
