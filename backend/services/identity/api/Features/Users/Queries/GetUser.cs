using AutoMapper;

using Domain.Shared.Common;

using Identity.Domain.DataTransferObjects;
using Identity.Domain.Entities;
using Identity.Domain.Repositories;

using MediatR;

namespace Identity.Api.Features.Users.Queries;

public record GetUser(Guid Id) : IRequest<Result<UserResponse>>;

public class GetUserHandler(IUserRepository userRepository, IMapper mapper) : IRequestHandler<GetUser, Result<UserResponse>>
{
	public async Task<Result<UserResponse>> Handle(GetUser request, CancellationToken cancellationToken)
	{
		var userResult = await userRepository.GetByIdAsync(request.Id, cancellationToken);
		if (userResult.IsFailure)
		{
			return userResult.Error;
		}

		var user = userResult.Value;
		var userResponse = mapper.Map<UserResponse>(user);
		return Result<UserResponse>.Success(userResponse);
	}
}