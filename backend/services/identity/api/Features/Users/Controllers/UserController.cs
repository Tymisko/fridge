using API.Shared.Extensions;

using Identity.Api.Features.Users.Commands;
using Identity.Api.Features.Users.Queries;
using Identity.Domain.DataTransferObjects;

using MediatR;

using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Identity.Api.Features.Users.Controllers;

[ApiController, Route("identity/users")]
public class UserController(IMediator mediator) : ControllerBase
{
	[HttpPost("create")]
	public async Task<ActionResult> CreateUserAsync([FromBody] CreateUser request, CancellationToken cancellationToken)
	{
		var result = await mediator.Send(request, cancellationToken);
		if (result.IsFailure)
		{
			return result.Error.ToProblemDetailsResult();
		}

		return CreatedAtRoute(nameof(GetUserAsync), new { id = result.Value }, result.Value);
	}

	[HttpGet("{id:guid}", Name = nameof(GetUserAsync))]
	public async Task<ActionResult<UserResponse>> GetUserAsync(Guid id, CancellationToken cancellationToken)
	{
		var request = new GetUser(id);
		var result = await mediator.Send(request, cancellationToken);
		if (result.IsFailure)
		{
			return result.Error.ToProblemDetailsResult();
		}

		return Ok(result.Value);
	}

	[HttpPatch("{id:guid}")]
	public async Task<ActionResult> UpdateUserAsync(
		[FromRoute] Guid id,
		[FromBody] JsonPatchDocument<UpdateUserRequest> patchDocument,
		CancellationToken cancellationToken
	)
	{
		var request = new UpdateUser(id, patchDocument);
		var result = await mediator.Send(request, cancellationToken);
		if (result.IsFailure)
		{
			return result.Error.ToProblemDetailsResult();
		}

		return NoContent();
	}


	[HttpDelete("{id:guid}")]
	public async Task<ActionResult> DeleteUserAsync(Guid id, CancellationToken cancellationToken)
	{
		var request = new DeleteUser(id);
		var result = await mediator.Send(request, cancellationToken);
		if (result.IsFailure)
		{
			return result.Error.ToProblemDetailsResult();
		}

		return NoContent();
	}
}