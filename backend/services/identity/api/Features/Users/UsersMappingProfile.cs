using AutoMapper;
using Identity.Api.Features.Users.Commands;
using Identity.Api.Features.Users.Resolvers;
using Identity.Domain.DataTransferObjects;
using Identity.Domain.Entities;

namespace Identity.Api.Features.Users;

// ReSharper disable once UnusedType.Global
public class UsersMappingProfile : Profile
{
	public UsersMappingProfile()
	{
		CreateMap<CreateUser, User>()
			.ForMember(dest => dest.Id, opt => opt.Ignore())
			.ForMember(dest => dest.Email, EmailMappingConfiguration)
			.ForMember(dest => dest.Username, UsernameMappingConfiguration)
			.ForMember(dest => dest.Password, opt => opt.MapFrom<PasswordHashResolver>());

		CreateMap<UpdateUserRequest, User>()
			.ForMember(dest => dest.Id, opt => opt.Ignore())
			.ForMember(dest => dest.Email, NullableEmailMappingConfiguration)
			.ForMember(dest => dest.Username, NullableUsernameMappingConfiguration);

		CreateMap<User, UserResponse>();
	}

	private static void UsernameMappingConfiguration(IMemberConfigurationExpression<CreateUser, User, string> opt) =>
		opt.MapFrom(src => src.Username.ToLowerInvariant());

	private static void EmailMappingConfiguration(IMemberConfigurationExpression<CreateUser, User, string> opt) =>
		opt.MapFrom(src => src.Email.ToLowerInvariant());

	private static void NullableEmailMappingConfiguration(
		IMemberConfigurationExpression<UpdateUserRequest, User, string> opt) =>
		opt.MapFrom(src => src.Email == null ? null : src.Email.ToLowerInvariant());

	private static void NullableUsernameMappingConfiguration(
		IMemberConfigurationExpression<UpdateUserRequest, User, string> opt) =>
		opt.MapFrom(src => src.Username == null ? null : src.Username.ToLowerInvariant());
}
