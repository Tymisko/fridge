using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using API.Shared.Features.Authentication.Options;

using Domain.Shared.Common;

using Identity.Domain.Entities;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Identity.Api.Features.Tokens.Services;

public interface IJwtService
{
	string Generate(User user);
	Task<Result<bool>> IsValidAsync(string token);
}

public class JwtService(
	IOptions<JwtOptions> jwtOptions,
	IOptions<JwtBearerOptions> jwtBearerOptions
) : IJwtService
{
	private readonly JwtOptions _options = jwtOptions.Value;
	private readonly JwtBearerOptions _bearerOptions = jwtBearerOptions.Value;
	private readonly JwtSecurityTokenHandler _tokenHandler = new();

	public string Generate(User user)
	{
		Claim[] claims =
		[
			new(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
			new(JwtRegisteredClaimNames.Email, user.Email)
		];

		var signingCredentials = new SigningCredentials(
			new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.SecretKey)), SecurityAlgorithms.HmacSha256);

		var token = new JwtSecurityToken(
			issuer: _options.Issuer,
			audience: _options.Audience,
			claims: claims,
			notBefore: null,
			expires: DateTime.UtcNow.AddHours(1),
			signingCredentials: signingCredentials
		);

		var tokenValue = _tokenHandler.WriteToken(token);
		return tokenValue;
	}

	public async Task<Result<bool>> IsValidAsync(string token)
	{
		var tokenHandler = new JwtSecurityTokenHandler();
		var tokenValidationResult =
			await tokenHandler.ValidateTokenAsync(token, _bearerOptions.TokenValidationParameters);
		if (tokenValidationResult.IsValid)
		{
			return Result<bool>.Success(true);
		}

		return tokenValidationResult.Exception switch
		{
			SecurityTokenExpiredException => TokenErrors.TokenExpired,
			SecurityTokenMalformedException => TokenErrors.TokenMalformed,
			SecurityTokenSignatureKeyNotFoundException => TokenErrors.TokenSignatureInvalid,
			_ => TokenErrors.TokenValidationFailed
		};
	}
}