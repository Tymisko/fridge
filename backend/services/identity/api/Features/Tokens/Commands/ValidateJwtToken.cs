using Domain.Shared.Common;

using Identity.Api.Features.Tokens.Services;

using MediatR;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Logging;

namespace Identity.Api.Features.Tokens.Commands;

public record ValidateJwtToken(string Token) : IRequest<Result<bool>>;

public class ValidateJwtTokenHandler(IJwtService jwtService, ILogger<ValidateJwtTokenHandler> logger)
	: IRequestHandler<ValidateJwtToken, Result<bool>>
{
	public async Task<Result<bool>> Handle(ValidateJwtToken request, CancellationToken cancellationToken)
	{
		if (!request.Token.StartsWith(JwtBearerDefaults.AuthenticationScheme))
		{
			logger.LogWarning("While validating token received invalid authentication scheme");
			return TokenErrors.InvalidTokenScheme;
		}

		var token = request.Token.Split(' ')[^1];
		var validationResult = await jwtService.IsValidAsync(token);
		return validationResult.IsFailure ? validationResult.Error : validationResult;
	}
}
