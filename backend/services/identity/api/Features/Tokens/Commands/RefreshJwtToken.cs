using Domain.Shared.Common;

using MediatR;

namespace Identity.Api.Features.Tokens.Commands;

public record RefreshJwtToken(string AccessToken, string RefreshToken)  : IRequest<Result<string>>;