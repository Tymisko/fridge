using Domain.Shared.Common;

using Identity.Api.Features.Tokens.Services;
using Identity.Domain.DataTransferObjects;
using Identity.Domain.Errors;
using Identity.Domain.Repositories;
using Identity.Domain.Services;

using MediatR;

namespace Identity.Api.Features.Tokens.Commands;

public record GenerateJwtToken(string Email, string Password) : IRequest<Result<TokenResponse>>;

public class GenerateJwtTokenHandler(
	IUserRepository userRepository,
	IJwtService jwtService,
	IHashService hashService
) : IRequestHandler<GenerateJwtToken, Result<TokenResponse>>
{
	public async Task<Result<TokenResponse>> Handle(GenerateJwtToken request, CancellationToken cancellationToken)
	{
		var existsResult = await userRepository.WithEmailExistsAsync(request.Email, cancellationToken);
		if (existsResult.IsFailure)
		{
			return existsResult.Error;
		}

		var userExists = existsResult.Value;
		if (!userExists)
		{
			return UserErrors.DoesNotExists(request.Email);
		}

		var getUserResult = await userRepository.GetByEmailAsync(request.Email, cancellationToken);
		if (getUserResult.IsFailure)
		{
			return getUserResult.Error;
		}

		var user = getUserResult.Value!;
		var passwordMatches = hashService.Verify(request.Password, user.Password);
		if (!passwordMatches)
		{
			return UserErrors.InvalidPassword();
		}

		var response = new TokenResponse
		{
			Token = jwtService.Generate(user)
		};
		return Result<TokenResponse>.Success(response);
	}
}