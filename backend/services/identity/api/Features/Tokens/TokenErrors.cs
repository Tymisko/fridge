using System.Net;

using Domain.Shared.Common;

namespace Identity.Api.Features.Tokens;

public static class TokenErrors
{
	public static readonly Error InvalidTokenScheme =
		new(HttpStatusCode.Unauthorized, "Invalid token authentication scheme");
	
	public static readonly Error TokenExpired =
		new(HttpStatusCode.Unauthorized, "Token has expired");

	public static readonly Error TokenMalformed =
		new(HttpStatusCode.BadRequest, "Token is malformed");

	public static readonly Error TokenSignatureInvalid =
		new(HttpStatusCode.Unauthorized, "Token signature is invalid");

	public static readonly Error TokenNotYetValid =
		new(HttpStatusCode.Unauthorized, "Token is not yet valid");

	public static readonly Error TokenValidationFailed =
		new(HttpStatusCode.Unauthorized, "Token validation failed");
}