using API.Shared.Extensions;

using Identity.Api.Features.Tokens.Commands;
using Identity.Domain.DataTransferObjects;

using MediatR;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Identity.Api.Features.Tokens.Controllers;

[ApiController, AllowAnonymous, Route("identity/tokens")]
public class TokenController(IMediator mediator) : ControllerBase
{
	[HttpPost]
	public async Task<ActionResult<TokenResponse>> GenerateToken([FromBody] GenerateJwtToken request,
		CancellationToken cancellationToken)
	{
		var generateTokenResult = await mediator.Send(request, cancellationToken);
		if (generateTokenResult.IsFailure)
		{
			return generateTokenResult.Error.ToProblemDetailsResult();
		}

		return Ok(generateTokenResult.Value);
	}

	[HttpGet("validate")]
	public async Task<ActionResult> ValidateToken(
		[FromHeader(Name = "Authorization")] string authorizationHeader,
		CancellationToken cancellationToken
	)
	{
		var request = new ValidateJwtToken(authorizationHeader);
		var validateTokenResult = await mediator.Send(request, cancellationToken);
		if (validateTokenResult.IsFailure)
		{
			return validateTokenResult.Error.ToProblemDetailsResult();
		}

		var isValid = validateTokenResult.Value;
		return isValid ? Ok() : Unauthorized(validateTokenResult.Error.ToProblemDetails());
	}

	[HttpPost("refresh")]
	public Task<ActionResult<string>> RefreshToken(
		[FromBody] RefreshJwtToken request,
		CancellationToken cancellationToken
	)
	{
		throw new NotImplementedException();
	}
}
