using API.Shared.Features.Authentication;

using Identity.Api.Features.Tokens.Services;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;

namespace Identity.Api.Features.Tokens;

public static class ServiceCollectionExtensions
{
	public static IServiceCollection AddJwtBearerTokenAuthentication(this IServiceCollection services)
	{
		services.AddJwtOptions();
		services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer();
		services.AddScoped<IJwtService, JwtService>();
		return services;
	}
}