using API.Shared;

using Identity.Api.Features.Tokens;

namespace Identity.Api;

public class ApiConfigurator(IServiceCollection services) : BaseApiConfigurator<IdentityApi>(services)
{
	public override void AddApi()
	{
		base.AddApi();
		_services
			.AddJwtBearerTokenAuthentication()
			.AddAutoMapper(config =>
			{
				config.ConstructServicesUsing(type => ActivatorUtilities.CreateInstance(_services.BuildServiceProvider(), type));
			}, typeof(IdentityApi).Assembly);
	}
}
