#Requires -Version 7.4.6

param (
    [Parameter(Mandatory = $true)]
    [string]$projectName,
    [Parameter(Mandatory = $true)]
    [string]$migrationName
)

$projectPath = Join-Path -Path $PSScriptRoot -ChildPath "../services/$projectName"
$infrastructureProjectPath = Join-Path -Path $projectPath -ChildPath "infrastructure/$projectName.Infrastructure.csproj"
$hostProjectPath = Join-Path -Path $projectPath -ChildPath "$projectName.Host/$projectName.Host.csproj"
$migrationsOutputDir = Join-Path -Path $projectPath -ChildPath "infrastructure/Migrations"
$dbContextName = "${projectName}DbContext"

$errors = @()
if (-not (Test-Path $infrastructureProjectPath)) {
    $errors += "Infrastructure project not found at: $infrastructureProjectPath"
}
if (-not (Test-Path $hostProjectPath)) {
    $errors += "Host project not found at: $hostProjectPath"
}

if ($errors.Count -gt 0) {
    Write-Error "Errors: $($errors -join ", ")"
    exit 1
}

Write-Output "Creating migration '$migrationName' for project '$projectName'..."
dotnet ef migrations add "$migrationName" `
    --project $infrastructureProjectPath `
    --startup-project $hostProjectPath `
    --output-dir $migrationsOutputDir `
    --context $dbContextName

if ($LASTEXITCODE -ne 0) {
    Write-Error "Failed to create migration '$migrationName' for project '$projectName'. Please check the errors above."
    exit $LASTEXITCODE
}

Write-Output "Migration '$migrationName' created successfully for project '$projectName'."
