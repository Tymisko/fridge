#Requires -Version 7.4.6
param (
    [Parameter(Mandatory = $true)]
    [string]$serviceName,
    [Parameter(Mandatory = $false)]
    [string]$envFileRelativePath = "../.env"
)

$servicePath = Join-Path -Path $PSScriptRoot -ChildPath "../backend/services/$serviceName"
$dockerfilePath = Join-Path -Path $servicePath -ChildPath "Dockerfile.$serviceName"
$envFile = Join-Path -Path $PSScriptRoot -ChildPath $envFileRelativePath
. "$PSScriptRoot/utils/environment-utils.ps1"


$errors = @()
if (-not (Test-Path $servicePath)) {
    $errors += "Service not found at: $servicePath"
}

if(-not (Test-Path $envFile)) {
    $errors += "Environment file not found at: $envFile"
}

if ($errors.Count -gt 0) {
    Write-Error "Errors: $($errors -join ", ")"
    exit 1
}

if(-not (Test-Path $dockerfilePath)) {
    Write-Error "Dockerfile not found at: $dockerfilePath"
    exit 1
}

$contextPath = Join-Path -Path $PSScriptRoot -ChildPath "../backend"
$port = GetEnvVariable "${serviceName}__PORT" $envFile

docker build -t $serviceName `
    --file $dockerfilePath `
    --build-arg PORT=$port `
    $contextPath

if ($LASTEXITCODE -ne 0) {
    Write-Error "Failed to build Docker image for service '$serviceName'. Please check the errors above."
    exit $LASTEXITCODE
}

Write-Output "Docker image for service '$serviceName' built successfully."


docker run -d -p "${port}:${port}" --name $serviceName-outside-compose --env-file $envFile $serviceName
