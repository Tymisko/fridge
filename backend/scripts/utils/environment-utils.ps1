function GetEnvVariable([string]$variableName, [string]$envFilePath) {
    if(-not (Test-Path $envFilePath)) {
        Write-Host "Environment file not found at: $envFilePath"
        exit 1
    }
    
    $value = Get-Content $envFilePath | Where-Object { $_ -match "^\s*$variableName\s*=" } | ForEach-Object {
        $parts = $_ -split '=', 2
        if ($parts.Length -eq 2) {
            return $parts[1].Trim().Trim('"', "'")
        }
    }
    
    if (-Not $value) {
        throw "Variable '$variableName' not found in $envFilePath"
    }
    
    return $value
}