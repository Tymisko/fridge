#Requires -Version 7.4.6

param (
    [Parameter(Mandatory = $true)]
    [string]$projectName,
    [Parameter(Mandatory = $false)]
    [string]$containerName = "Postgres",
    [Parameter(Mandatory = $false)]
    [string]$envFileRelativePath = "../../.env"
)
. "$PSScriptRoot/utils/environment-utils.ps1"

function MigrateDatabase() {
	Write-Host "Applying pending migrations for '$projectName' project database"
	try {
		dotnet ef database update --project $infrastructureProject --startup-project $startupProject --context "${capitalizedProjectName}DbContext"
	}
	catch {
		Write-Host "Failed to apply migrations: $_"
		exit 1
	}
	
	Write-Host "Database migrations applied successfully"
}

$envFile = Join-Path -Path $PSScriptRoot -ChildPath $envFileRelativePath

$userId = GetEnvVariable "POSTGRES__USERID" $envFile
$password = GetEnvVariable "POSTGRES__PASSWORD" $envFile
$hostName = GetEnvVariable "POSTGRES__HOST" $envFile
$port = GetEnvVariable "POSTGRES__PORT" $envFile
$databaseName = GetEnvVariable "POSTGRES__${projectName}__DATABASENAME" $envFile

$capitalizedProjectName = -join ($projectName.Substring(0, 1).ToUpper(), $projectName.Substring(1))

# Set environment variables for EF Core CLI
$env:ConnectionStrings__Default = "Host=$hostName;Port=$port;Database=$databaseName;Username=$userId;Password=$password"

# Build paths
$infrastructureProject = Join-Path -Path $PSScriptRoot -ChildPath "..\services\$projectName\infrastructure\$capitalizedProjectName.Infrastructure.csproj"
$startupProject = Join-Path -Path $PSScriptRoot -ChildPath "..\services\$projectName\$capitalizedProjectName.Host\$capitalizedProjectName.Host.csproj"

# Collect all errors
$errors = @()

if (-not (Test-Path $infrastructureProject)) {
    $errors += "Infrastructure project not found at: $infrastructureProject"
}

if (-not (Test-Path $startupProject)) {
    $errors += "Startup project not found at: $startupProject"
}

$containerRunning = docker ps -q -f name=$containerName
if (-not $containerRunning) {
    $errors += "Container '$containerName' is not running"
}

# If any errors were found, report them all and exit
if ($errors.Count -gt 0) {
    Write-Host "Found the following errors:"
    $errors | ForEach-Object { Write-Host "- $_" }
    exit 1
}

$databaseExistsQuery = "SELECT 1 FROM pg_database WHERE datname='$databaseName';"

$result = docker exec $containerName psql -U $userId -d "postgres" -tAc $databaseExistsQuery
$databaseExists = $result -eq "1"
if (-not $databaseExists) {
    Write-Host "Database '$databaseName' does not exist. Applying migrations without backing up."
    MigrateDatabase
    exit 0;
}

Write-Host "Checking pending migrations for '$projectName' project database"
$anyPendingMigrations = $false

try {

    # Get all migrations
    $allMigrations = dotnet ef migrations list --project $infrastructureProject --startup-project $startupProject --context "${capitalizedProjectName}DbContext" 2>&1
    
    # Get applied migrations
    $appliedMigrations = dotnet ef database list --project $infrastructureProject --startup-project $startupProject --context "${capitalizedProjectName}DbContext" 2>&1
    
    # Compare to find pending migrations
    $pendingMigrations = Compare-Object -ReferenceObject $allMigrations -DifferenceObject $appliedMigrations | 
    Where-Object { $_.SideIndicator -eq '<=' } | 
    Select-Object -ExpandProperty InputObject

    if ($pendingMigrations) {
        Write-Host "Pending migrations found:"
        $pendingMigrations | ForEach-Object { Write-Host "- $_" }
        $anyPendingMigrations = $true
    }
    else {
        Write-Host "The database is up to date with all migrations."
        exit 0;
    }
}
catch {
    Write-Host "Failed to check migrations: $_"
    exit 1
}

$doBackup = $databaseExists -and $anyPendingMigrations

if ($doBackup) {
    Write-Host "Backing up database '$databaseName' using pg_dump"

    # Define the backups directory path
    $backupDirectory = Join-Path -Path $PSScriptRoot -ChildPath "backups"

    # Check if the backups directory exists; if not, create it
    if (-not (Test-Path $backupDirectory)) {
        Write-Host "Backups directory not found. Creating at $backupDirectory"
        New-Item -ItemType Directory -Path $backupDirectory | Out-Null
    }

    $timestamp = Get-Date -Format "yyyyMMdd_HHmmss"
    $backupFile = "${databaseName}_${timestamp}.sql"

    docker exec $containerName pg_dump -U $userId -d $databaseName -F c -b -v -f "/tmp/$backupFile"
    docker cp "${containerName}:/tmp/$backupFile" "$backupDirectory\$backupFile"

    if (-not (Test-Path "$backupDirectory\$backupFile")) {
        Write-Host "Failed to save database backup to $backupDirectory\$backupFile"
        exit 1
    }
    Write-Host "Database backup saved to $backupDirectory\$backupFile"
}

if(-not $anyPendingMigrations) {
    Write-Host "No pending migrations found. Exiting."
    exit 0;
}

MigrateDatabase
