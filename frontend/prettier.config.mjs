import importsPlugin from 'prettier-plugin-organize-imports';
import packagePlugin from 'prettier-plugin-package';

// prettier.config.js
/** @type {import("prettier").Config} */
const config = {
	printWidth: 120,
	tabWidth: 4,
	useTabs: true,
	semi: true,
	singleQuote: true,
	quoteProps: 'as-needed',
	jsxSingleQuote: true,
	trailingComma: 'es5',
	bracketSpacing: true,
	arrowParens: 'avoid',
	requirePragma: false,
	insertPragma: false,
	proseWrap: 'preserve',
	htmlWhitespaceSensitivity: 'strict',
	endOfLine: 'lf',
	embeddedLanguageFormatting: 'auto',
	singleAttributePerLine: true,
	bracketSameLine: false,
	overrides: [
		{
			files: '*.html',
			options: {
				parser: 'angular',
				htmlWhitespaceSensitivity: 'ignore',
			},
		},
		{
			files: '*.md',
			options: {
				proseWrap: 'always',
			},
		},
	],

	plugins: [importsPlugin, packagePlugin],
};

export default config;
