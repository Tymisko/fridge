import { execSync } from 'child_process';

// This script is used to run ng lint on the staged files using lint-staged, because it does not support ng lint yet.

const files = process.argv.slice(2);

const command = `ng lint --eslint-config eslint.config.js ${files.map(file => `--lint-file-patterns="${file}"`).join(' ')}`;

console.log(`Running command: ${command}`);

try {
	execSync(command, { stdio: 'inherit' });
} catch (error) {
	process.exit(error.status);
}
