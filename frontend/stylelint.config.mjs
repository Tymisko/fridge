/** @type {import('stylelint').Config} */
import stylelintConfigStandardScss from 'stylelint-config-standard-scss';
import stylelintScss from 'stylelint-scss';
import stylelintSelectorBemPattern from 'stylelint-selector-bem-pattern';

export default {
	extends: [stylelintConfigStandardScss],
	plugins: [stylelintScss, stylelintSelectorBemPattern],
	rules: {
		'scss/at-rule-no-unknown': true,
		'at-rule-no-unknown': [
			true,
			{
				ignoreAtRules: ['extend', 'include'],
			},
		],
		'block-no-empty': true,
		'color-no-invalid-hex': true,
		'no-empty-source': true,
		'plugin/selector-bem-pattern': {
			componentName: '[A-Z][a-zA-Z0-9]+',
			componentSelectors: {
				initial: '^\\.{componentName}(?:__{elementName}|--{modifierName})?$',
				local: '^\\.{componentName}(?:__{elementName}|--{modifierName})?$',
			},
			utilitySelectors: '^u-.*$',
		},
	},
	ignoreFiles: ['dist/**/*', 'node_modules/**/*'],
};
