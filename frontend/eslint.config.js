// @ts-check

const js = require('@eslint/js');
const ts = require('typescript-eslint');
const angular = require('angular-eslint');
const prettier = require('eslint-plugin-prettier/recommended');

module.exports = ts.config(
	{
		ignores: ['node_modules', 'dist'],
	},
	{
		files: ['**/*.ts'],
		extends: [
			js.configs.recommended,
			...ts.configs.strictTypeChecked,
			...ts.configs.stylistic,
			...angular.configs.tsRecommended,
			prettier,
		],
		languageOptions: {
			parser: ts.parser,
			parserOptions: {
				project: ['./tsconfig.json', './tsconfig.spec.json', './tsconfig.app.json'],
			},
		},
		processor: angular.processInlineTemplates,
		rules: {
			'@typescript-eslint/explicit-function-return-type': 'error',
			'@typescript-eslint/explicit-module-boundary-types': 'error',
			'@typescript-eslint/typedef': [
				'error',
				{
					arrayDestructuring: true,
					arrowParameter: true,
					memberVariableDeclaration: true,
					objectDestructuring: true,
					parameter: true,
					propertyDeclaration: true,
					variableDeclaration: true,
					variableDeclarationIgnoreFunction: true,
				},
			],
			'@typescript-eslint/no-inferrable-types': 'off',
			'@typescript-eslint/consistent-type-definitions': ['error', 'interface'],
			'@typescript-eslint/consistent-type-imports': [
				'error',
				{
					prefer: 'type-imports',
					disallowTypeAnnotations: true,
					fixStyle: 'separate-type-imports',
				},
			],
			'@typescript-eslint/no-explicit-any': 'error',
			'@typescript-eslint/no-unsafe-assignment': 'error',
			'@typescript-eslint/no-unsafe-call': 'error',
			'@typescript-eslint/no-unsafe-member-access': 'error',
			'@typescript-eslint/no-unsafe-return': 'error',
			'@typescript-eslint/strict-boolean-expressions': 'error',
			'@typescript-eslint/explicit-member-accessibility': 'error',
			'@typescript-eslint/parameter-properties': [
				'error',
				{
					allow: ['private readonly', 'protected readonly'],
					prefer: 'parameter-property',
				},
			],
			'@typescript-eslint/no-require-imports': [
				'error',
				{
					allow: ['/prettier\\.config\\.js$', '/package\\.json$'],
				},
			],
			'@typescript-eslint/no-unnecessary-boolean-literal-compare': 'error',
			'@typescript-eslint/no-unnecessary-condition': 'error',
			'@typescript-eslint/no-unnecessary-qualifier': 'error',
			'@typescript-eslint/no-unnecessary-type-arguments': 'error',
			'@typescript-eslint/no-unnecessary-type-constraint': 'error',
			'@typescript-eslint/no-unsafe-argument': 'error',
			'@typescript-eslint/prefer-as-const': 'error',
			'@typescript-eslint/prefer-enum-initializers': 'error',
			'@typescript-eslint/prefer-function-type': 'error',
			'@typescript-eslint/prefer-includes': 'error',
			'@typescript-eslint/prefer-literal-enum-member': 'error',
			'@typescript-eslint/prefer-namespace-keyword': 'error',
		},
	},
	{
		files: ['**/*.html'],
		extends: [...angular.configs.templateRecommended, ...angular.configs.templateAccessibility],
		rules: {},
	}
);
