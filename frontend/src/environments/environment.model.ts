import type { ConfigLevels } from '@ngworker/lumberjack';

export interface Environment {
	readonly production: boolean;
	readonly logLevels: ConfigLevels;
	readonly apiUrl: string | undefined;
}
