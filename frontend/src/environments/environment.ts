import type { Environment } from './environment.model';

export const environment: Environment = {
	production: false,
	logLevels: ['debug', 'info', 'warn', 'error', 'critical'],
	apiUrl: 'http://localhost:8080/api/v1',
} as const;
