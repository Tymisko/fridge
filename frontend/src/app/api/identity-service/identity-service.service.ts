import { environment } from '@/environments/environment';
import { HttpClient, HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IdentityErrors } from '@app/api/identity-service/models/IdentityErrors.model';
import { ErrorResult, UnknownError } from '@app/models/results/error-result.model';
import { getProblemDetails, ProblemDetails } from '@app/models/results/problem-details.model';
import { Result } from '@app/models/results/result-t.model';
import { LumberjackService } from '@ngworker/lumberjack';
import type { UUID } from 'node:crypto';
import { catchError, map, type Observable, of } from 'rxjs';
import type { CreateUserDto } from './dto/create-user.dto';
import { GenerateJwtTokenDto } from './dto/generate-jwt-token.dto';
import { TokenResponseDto } from './dto/token-response.dto';

@Injectable({
	providedIn: 'root',
})
export class IdentityServiceService {
	private readonly baseApiUrl: string;

	public constructor(
		private readonly http: HttpClient,
		private readonly logger: LumberjackService
	) {
		const apiUrl: string | undefined = environment.apiUrl;
		if (apiUrl === undefined || apiUrl === '') {
			throw new Error('API URL is not defined');
		}

		this.baseApiUrl = apiUrl;
	}

	public createUser(createUserDto: CreateUserDto): Observable<Result<UUID>> {
		const url: string = `${this.baseApiUrl}/identity/users/create`;
		const request: Observable<UUID> = this.http.post<UUID>(url, createUserDto);
		return request.pipe(
			map((response: UUID): Result<UUID> => Result.success(response)),
			catchError((errorResponse: HttpErrorResponse): Observable<Result<UUID>> => {
				const problemDetails: ProblemDetails = getProblemDetails(errorResponse);

				if (problemDetails.status === HttpStatusCode.Conflict) {
					this.logger.logDebug(
						`A conflict occurred while creating a user: ${JSON.stringify(problemDetails)}`,
						undefined,
						IdentityServiceService.name
					);
					return of(this.handleCreateUserConflictResponse(problemDetails, createUserDto));
				}

				this.logger.logDebug(
					`An unknown error occurred while creating a user: ${JSON.stringify(problemDetails)}`,
					undefined,
					IdentityServiceService.name
				);
				const error: ErrorResult = UnknownError(problemDetails.detail ?? '');
				const result: Result<UUID> = Result.failure(error);
				return of(result);
			})
		);
	}

	private handleCreateUserConflictResponse(
		problemDetails: ProblemDetails,
		createUserDto: CreateUserDto
	): Result<UUID> {
		const isEmailTaken: boolean = problemDetails.detail?.toLocaleLowerCase().includes('email') ?? false;
		if (isEmailTaken) {
			this.logger.logDebug(
				`Conflict reason: The email ${createUserDto.email} is already taken.`,
				undefined,
				IdentityServiceService.name
			);
			const error: ErrorResult = IdentityErrors.UserWithEmailAlreadyExists(createUserDto.email);
			const result: Result<UUID> = Result.failure(error);
			return result;
		}

		const isUsernameTaken: boolean = problemDetails.detail?.toLocaleLowerCase().includes('username') ?? false;
		if (isUsernameTaken) {
			this.logger.logDebug(
				`Conflict reason: The username ${createUserDto.username} is already taken.`,
				undefined,
				IdentityServiceService.name
			);
			const error: ErrorResult = IdentityErrors.UserWithUsernameAlreadyExists(createUserDto.username);
			const result: Result<UUID> = Result.failure(error);
			return result;
		}

		this.logger.logDebug(
			`Unknown error with ${problemDetails.status.toString()} status code.`,
			undefined,
			IdentityServiceService.name
		);
		const message: string = `Unknown error with ${problemDetails.status.toString()} status code.`;
		const error: ErrorResult = UnknownError(message);
		const result: Result<UUID> = Result.failure(error);
		return result;
	}

	public generateJwtToken(generateJwtTokenDto: GenerateJwtTokenDto): Observable<Result<string>> {
		const url: string = `${this.baseApiUrl}/identity/tokens`;
		const request: Observable<TokenResponseDto> = this.http.post<TokenResponseDto>(url, generateJwtTokenDto);
		return request.pipe(
			map((response: TokenResponseDto): Result<string> => Result.success(response.token)),
			catchError((errorResponse: HttpErrorResponse): Observable<Result<string>> => {
				const problemDetails: ProblemDetails = getProblemDetails(errorResponse);
				const result: Result<string> = this.handleGenerateJwtTokenError(problemDetails, generateJwtTokenDto);
				return of(result);
			})
		);
	}

	private handleGenerateJwtTokenError(
		problemDetails: ProblemDetails,
		generateJwtTokenDto: GenerateJwtTokenDto
	): Result<string> {
		if (problemDetails.status === HttpStatusCode.NotFound) {
			this.logger.logDebug(
				`Received 404 not found error while generating JWT token for user with email ${generateJwtTokenDto.email}`,
				undefined,
				IdentityServiceService.name
			);
			const error: ErrorResult = IdentityErrors.UserDoesNotExist(generateJwtTokenDto.email);
			const result: Result<string> = Result.failure(error);
			return result;
		}

		if (problemDetails.status === HttpStatusCode.Unauthorized) {
			this.logger.logDebug(
				`Received 401 unauthorized error while generating JWT token for user with email ${generateJwtTokenDto.email}`,
				undefined,
				IdentityServiceService.name
			);
			const error: ErrorResult = IdentityErrors.InvalidPassword();
			const result: Result<string> = Result.failure(error);
			return result;
		}

		this.logger.logDebug(
			`An unknown error occured while generating JWT token: ${problemDetails.type}, ${problemDetails.detail ?? ''}`
		);
		const error: ErrorResult = UnknownError(problemDetails.detail ?? '');
		const result: Result<string> = Result.failure(error);
		return result;
	}
}
