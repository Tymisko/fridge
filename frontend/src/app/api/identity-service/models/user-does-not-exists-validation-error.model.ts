export const USER_DOES_NOT_EXIST_ERROR: string = 'userDoesNotExist';

export interface UserDoesNotExistValidationError {
	readonly userDoesNotExist: true;
	readonly message: string;
}

export const isUserDoesNotExistValidationError: (error: unknown) => error is UserDoesNotExistValidationError = (
	error: unknown
): error is UserDoesNotExistValidationError => {
	return typeof error === 'object' && error !== null && 'userDoesNotExist' in error && 'message' in error;
};
