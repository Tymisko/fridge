import { IdentityErrorTypes } from '@app/api/identity-service/enums/identity-errors.enum';
import { ErrorResult } from '@app/models/results/error-result.model';

interface IdentityErrors {
	readonly UserWithEmailAlreadyExists: (email: string) => ErrorResult;
	readonly UserWithUsernameAlreadyExists: (username: string) => ErrorResult;
	readonly UserDoesNotExist: (email: string) => ErrorResult;
	readonly InvalidPassword: () => ErrorResult;
}

export const IdentityErrors: IdentityErrors = {
	UserWithEmailAlreadyExists: (email: string): ErrorResult =>
		new ErrorResult(IdentityErrorTypes.UserWithEmailAlreadyExists, `User with email ${email} already exists`),
	UserWithUsernameAlreadyExists: (username: string): ErrorResult =>
		new ErrorResult(
			IdentityErrorTypes.UserWithUsernameAlreadyExists,
			`User with username ${username} already exists`
		),
	UserDoesNotExist: (email: string): ErrorResult =>
		new ErrorResult(IdentityErrorTypes.UserDoesNotExist, `User with email ${email} does not exist`),
	InvalidPassword: (): ErrorResult => new ErrorResult(IdentityErrorTypes.InvalidPassword, 'Invalid password'),
} as const;
