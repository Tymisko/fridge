export const INVALID_PASSWORD_ERROR: string = 'invalidPassword';

export interface InvalidPasswordValidationError {
	readonly invalidPassword: true;
	readonly message: string;
}

export const isInvalidPasswordValidationError: (error: unknown) => error is InvalidPasswordValidationError = (
	error: unknown
): error is InvalidPasswordValidationError => {
	return typeof error === 'object' && error !== null && 'invalidPassword' in error && 'message' in error;
};
