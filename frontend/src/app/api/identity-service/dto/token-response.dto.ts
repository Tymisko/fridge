export interface TokenResponseDto {
	readonly token: string;
}
