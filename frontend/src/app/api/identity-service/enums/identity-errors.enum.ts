export enum IdentityErrorTypes {
	UserWithEmailAlreadyExists = 'UserWithEmailAlreadyExists',
	UserWithUsernameAlreadyExists = 'UserWithUsernameAlreadyExists',
	UserDoesNotExist = 'UserDoesNotExist',
	InvalidPassword = 'InvalidPassword',
}
