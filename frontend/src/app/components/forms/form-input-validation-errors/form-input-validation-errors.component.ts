import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FormErrorMessagesPipe } from '@app/pipes/forms/form-error-messages.pipe';
import { Message } from 'primeng/message';

@Component({
	selector: 'fdg-form-input-validation-errors',
	standalone: true,
	imports: [Message, FormErrorMessagesPipe],
	template: `@if (IsInvalid) {
		@for (error of control.errors | formErrorMessages; track $index) {
			<p-message severity="error">
				{{ error }}
			</p-message>
		}
	}`,
	styles: `
		:host {
			display: flex;
			flex-direction: column;
			gap: 0.25rem;
		}
	`,
})
export class FormInputValidationErrorsComponent {
	@Input({ required: true }) public control!: AbstractControl;

	protected get IsInvalid(): boolean {
		const isInvalid: boolean = this.control.invalid;
		if (!isInvalid) {
			return false;
		}

		const isDirty: boolean = this.control.dirty;
		const isTouched: boolean = this.control.touched;
		return isDirty || isTouched;
	}
}
