import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { IftaLabel } from 'primeng/iftalabel';
import { InputText } from 'primeng/inputtext';
import { FormInputValidationErrorsComponent } from '../form-input-validation-errors/form-input-validation-errors.component';

@Component({
	selector: 'fdg-form-text-input',
	standalone: true,
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => FormTextInputComponent),
			multi: true,
		},
	],
	imports: [IftaLabel, InputText, ReactiveFormsModule, FormInputValidationErrorsComponent],
	template: `
		<p-iftalabel>
			<input
				pInputText
				[type]="type"
				[name]="id"
				[id]="id"
				[value]="value"
				(input)="onInput($event)"
				(blur)="onTouched()"
				[disabled]="isDisabled"
				[class.ng-invalid]="IsInvalid"
				[class.ng-dirty]="IsInvalid"
			/>
			<label [for]="id">{{ label }}</label>
		</p-iftalabel>
		<fdg-form-input-validation-errors [control]="formControl"></fdg-form-input-validation-errors>
	`,
	styles: `
		p-iftalabel input {
			width: 100%;
		}
	`,
})
export class FormTextInputComponent implements ControlValueAccessor {
	@Input({ required: true }) public id!: string;
	@Input({ required: true }) public label!: string;
	@Input() public isEmail: boolean = false;
	@Input({ required: true }) public formControl!: FormControl<string>;
	protected value: string = '';
	protected isDisabled: boolean = false;

	protected get type(): string {
		return this.isEmail ? 'email' : 'text';
	}

	public writeValue(value: string): void {
		this.value = value;
	}

	protected onChange!: (value: string) => void;

	public registerOnChange(fn: (value: string) => void): void {
		this.onChange = fn;
	}

	protected onTouched!: () => void;

	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn;
	}

	public setDisabledState?(isDisabled: boolean): void {
		this.isDisabled = isDisabled;
	}

	protected onInput(event: Event): void {
		const input: HTMLInputElement = event.target as HTMLInputElement;
		const value: string = input.value;

		this.value = value;
		this.onChange(value);
	}

	protected get IsInvalid(): boolean {
		const isInvalid: boolean = this.formControl.invalid;
		if (!isInvalid) {
			return false;
		}

		const isDirty: boolean = this.formControl.dirty;
		const isTouched: boolean = this.formControl.touched;
		return isDirty || isTouched;
	}
}
