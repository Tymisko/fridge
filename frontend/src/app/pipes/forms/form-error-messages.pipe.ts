import {
	INVALID_PASSWORD_ERROR,
	isInvalidPasswordValidationError,
} from '@/app/api/identity-service/models/invalid-password-validation-error.model';
import {
	isUserDoesNotExistValidationError,
	USER_DOES_NOT_EXIST_ERROR,
} from '@/app/api/identity-service/models/user-does-not-exists-validation-error.model';
import { Pipe, PipeTransform } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { CONTAINS_DIGIT_ERROR } from '@app/features/authentication/validators/containDigit/contains-digit-validator.const';
import { CONTAINS_LOWERCASE_LETTER_ERROR } from '@app/features/authentication/validators/containLowercaseLetter/contains-lowercase-letter-validator.const';
import { CONTAINS_SPECIAL_CHARACTER_ERROR } from '@app/features/authentication/validators/containSpecialCharacter/contains-special-character-validator.const';
import { CONTAINS_UPPERCASE_LETTER_ERROR } from '@app/features/authentication/validators/containUppercaseLetter/contains-uppercase-letter-validator.const';
import { EMAIL_VALIDATION_ERROR } from '@app/features/authentication/validators/email/email-validator.const';
import {
	EMAIL_ALREADY_TAKEN_ERROR,
	isEmailAlreadyTakenValidationError,
} from '@app/models/forms/validation-errors/email-already-taken-validation-error.model';
import { isMaxLengthValidationError } from '@app/models/forms/validation-errors/max-length-validation-error.model';
import { isMinLengthValidationError } from '@app/models/forms/validation-errors/min-length-validation-error.model';
import {
	isUsernameAlreadyTakenValidationError,
	USERNAME_ALREADY_TAKEN_ERROR,
} from '@app/models/forms/validation-errors/username-already-taken-validation-error.model';

@Pipe({
	name: 'formErrorMessages',
	standalone: true,
	pure: true,
})
export class FormErrorMessagesPipe implements PipeTransform {
	public transform(errors: ValidationErrors | null): string[] {
		if (!errors) {
			return [];
		}

		const messages: string[] = [];
		const keys: string[] = Object.keys(errors);
		for (const key of keys) {
			switch (key) {
				case 'required':
					messages.push('This field is required');
					break;
				case 'email':
					messages.push('Invalid email address');
					break;
				case 'minlength': {
					const message: string = FormErrorMessagesPipe.getMessageForMinLengthError(errors[key]);
					messages.push(message);
					break;
				}
				case 'maxLength': {
					const message: string = FormErrorMessagesPipe.getMessageForMaxLengthError(errors[key]);
					messages.push(message);
					break;
				}
				case 'passwordMatch':
					messages.push('Passwords do not match');
					break;
				case EMAIL_VALIDATION_ERROR:
					messages.push('Invalid email address');
					break;
				case CONTAINS_LOWERCASE_LETTER_ERROR:
					messages.push('Password must contain at least one lowercase letter');
					break;
				case CONTAINS_UPPERCASE_LETTER_ERROR:
					messages.push('Password must contain at least one uppercase letter');
					break;
				case CONTAINS_DIGIT_ERROR:
					messages.push('Password must contain at least one digit');
					break;
				case CONTAINS_SPECIAL_CHARACTER_ERROR:
					messages.push('Password must contain at least one special character');
					break;
				case EMAIL_ALREADY_TAKEN_ERROR: {
					const message: string = FormErrorMessagesPipe.getMessageForEmailAlreadyTakenError(errors[key]);
					messages.push(message);
					break;
				}
				case USERNAME_ALREADY_TAKEN_ERROR: {
					const message: string = FormErrorMessagesPipe.getMessageForUsernameAlreadyTakenError(errors[key]);
					messages.push(message);
					break;
				}
				case INVALID_PASSWORD_ERROR: {
					const message: string = FormErrorMessagesPipe.getMessageForInvalidPasswordError(errors[key]);
					messages.push(message);
					break;
				}
				case USER_DOES_NOT_EXIST_ERROR: {
					const message: string = FormErrorMessagesPipe.getMessageForUserDoesNotExistError(errors[key]);
					messages.push(message);
					break;
				}
				default:
					messages.push('Unknown error');
					break;
			}
		}

		return messages;
	}

	private static getMessageForUserDoesNotExistError(error: unknown): string {
		if (!isUserDoesNotExistValidationError(error)) {
			throw new Error('Not user does not exist error');
		}

		return error.message;
	}

	private static getMessageForInvalidPasswordError(error: unknown): string {
		if (!isInvalidPasswordValidationError(error)) {
			throw new Error('Not invalid password error');
		}

		return error.message;
	}

	private static getMessageForUsernameAlreadyTakenError(error: unknown): string {
		if (!isUsernameAlreadyTakenValidationError(error)) {
			throw new Error('Not username already taken error');
		}

		return error.message;
	}

	private static getMessageForEmailAlreadyTakenError(error: unknown): string {
		if (!isEmailAlreadyTakenValidationError(error)) {
			throw new Error('Not email already taken error');
		}

		return error.message;
	}

	private static getMessageForMinLengthError(error: unknown): string {
		if (!isMinLengthValidationError(error)) {
			throw new Error('Not min length error');
		}

		return `Minimum length is ${error.requiredLength.toString()}, but actual is ${error.actualLength.toString()}`;
	}

	private static getMessageForMaxLengthError(error: unknown): string {
		if (!isMaxLengthValidationError(error)) {
			throw new Error('Not max length error');
		}

		return `Maximum length is ${error.requiredLength.toString()}, but actual is ${error.actualLength.toString()}`;
	}
}
