import type { Routes } from '@angular/router';
import { authRoutes } from './features/authentication/authentication.routes';
import { authenticationGuard } from './features/authentication/guards/authentication.guard';
import { DashboardComponent } from './features/dashboard/dashboard.component';

export const routes: Routes = [
	{
		path: '',
		canActivate: [authenticationGuard],
		children: [
			{
				path: '',
				redirectTo: '/dashboard',
				pathMatch: 'full',
			},
			{
				path: 'dashboard',
				component: DashboardComponent,
			},
		],
	},
	{ path: 'auth', children: authRoutes },
	{ path: '**', redirectTo: '/dashboard', pathMatch: 'full' },
];
