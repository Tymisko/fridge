import type { ErrorResult } from './error-result.model';

export class Result<T> {
	private readonly _value?: T;
	private readonly _error?: ErrorResult;
	private readonly _isSuccess: boolean;

	private constructor(isSuccess: boolean, value?: T, error?: ErrorResult) {
		this._isSuccess = isSuccess;
		this._value = value;
		this._error = error;
	}

	public get isSuccess(): boolean {
		return this._isSuccess;
	}

	public get isFailure(): boolean {
		return !this._isSuccess;
	}

	public get value(): T {
		if (this.isFailure) {
			throw new Error('Cannot retrieve value from a failed result.');
		}

		if (this._value === undefined) {
			throw new Error('Value is undefined for a successful result');
		}

		return this._value;
	}

	public get error(): ErrorResult {
		if (this.isSuccess) {
			throw new Error('Cannot retrieve error from a successful result.');
		}

		if (this._error === undefined) {
			throw new Error('Error is undefined for a failed result');
		}

		return this._error;
	}

	public static success<T>(value: T): Result<T> {
		return new Result<T>(true, value);
	}

	public static failure<T>(error: ErrorResult): Result<T> {
		return new Result<T>(false, undefined, error);
	}
}
