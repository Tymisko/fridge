import type { IdentityErrorTypes } from '@app/api/identity-service/enums/identity-errors.enum';

type ErrorTypes = 'Unknown' | IdentityErrorTypes;

export class ErrorResult {
	private readonly _message: string;
	private readonly _type: ErrorTypes;

	public constructor(type: ErrorTypes, message: string) {
		this._type = type;
		this._message = message;
	}

	public get type(): ErrorTypes {
		return this._type;
	}

	public get message(): string {
		return this._message;
	}
}

export const UnknownError: (details: string) => ErrorResult = (details: string): ErrorResult =>
	new ErrorResult('Unknown', `An unknown error occurred: ${details}`);
