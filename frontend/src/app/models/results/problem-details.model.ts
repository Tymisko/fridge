import type { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';

export interface ProblemDetails {
	type: string;
	title: string;
	status: HttpStatusCode;
	detail?: string;
	instance?: string;
	error?: ProblemDetails;
}

const isProblemDetails: (value: unknown) => value is ProblemDetails = (value: unknown): value is ProblemDetails => {
	if (typeof value !== 'object' || value === null) {
		return false;
	}

	const obj: Record<string, unknown> = value as Record<string, unknown>;

	return (
		typeof obj['type'] === 'string' &&
		typeof obj['title'] === 'string' &&
		typeof obj['status'] === 'number' &&
		(obj['detail'] === undefined || typeof obj['detail'] === 'string') &&
		(obj['instance'] === undefined || typeof obj['instance'] === 'string') &&
		(obj['error'] === undefined || isProblemDetails(obj['error']))
	);
};

export const getProblemDetails: (errorResponse: HttpErrorResponse) => ProblemDetails = (
	errorResponse: HttpErrorResponse
): ProblemDetails => {
	const defaultProblemDetails: ProblemDetails = {
		type: `https://example.com/errors/${errorResponse.status.toString()}`,
		title: errorResponse.statusText || 'Unknown Error',
		status: errorResponse.status,
		detail: errorResponse.message,
		instance: errorResponse.url ?? undefined,
	};

	if (isProblemDetails(errorResponse.error)) {
		return {
			...defaultProblemDetails,
			...errorResponse.error,
		};
	}

	if (typeof errorResponse.error === 'object' && errorResponse.error !== null) {
		const error: Record<string, unknown> = errorResponse.error as Record<string, unknown>;
		return {
			...defaultProblemDetails,
			detail: (error['detail'] as string) || defaultProblemDetails.detail,
			instance: (error['instance'] as string) || defaultProblemDetails.instance,
			error: isProblemDetails(error['error']) ? error['error'] : undefined,
		};
	}

	return defaultProblemDetails;
};
