export class Result {
	private readonly _error?: Error;
	private readonly _isSuccess: boolean;

	private constructor(isSuccess: boolean, error?: Error) {
		this._isSuccess = isSuccess;
		this._error = error;
	}

	public get isSuccess(): boolean {
		return this._isSuccess;
	}

	public get isFailure(): boolean {
		return !this._isSuccess;
	}

	public get error(): Error {
		if (this.isSuccess) {
			throw new Error('Cannot retrieve error from a successful result.');
		}

		if (this._error === undefined) {
			throw new Error('Error is undefined for a failed result');
		}

		return this._error;
	}

	public static success(): Result {
		return new Result(true);
	}

	public static failure(error: Error): Result {
		return new Result(false, error);
	}
}
