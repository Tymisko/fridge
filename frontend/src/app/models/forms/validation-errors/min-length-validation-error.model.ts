export interface MinLengthValidationError {
	readonly requiredLength: number;
	readonly actualLength: number;
}

export const isMinLengthValidationError: (error: unknown) => error is MinLengthValidationError = (
	error: unknown
): error is MinLengthValidationError => {
	if (typeof error !== 'object' || error === null) {
		return false;
	}

	return Object.keys(error).includes('requiredLength') && Object.keys(error).includes('actualLength');
};
