export interface MaxLengthValidationError {
	requiredLength: number;
	actualLength: number;
}

export const isMaxLengthValidationError: (error: unknown) => error is MaxLengthValidationError = (
	error: unknown
): error is MaxLengthValidationError => {
	if (typeof error !== 'object' || error === null) {
		return false;
	}

	return Object.keys(error).includes('requiredLength') && Object.keys(error).includes('actualLength');
};
