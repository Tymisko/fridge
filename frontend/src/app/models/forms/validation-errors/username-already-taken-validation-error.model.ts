export const USERNAME_ALREADY_TAKEN_ERROR: string = 'usernameAlreadyTaken';

export interface UsernameAlreadyTakenValidationError {
	readonly usernameAlreadyTaken: boolean;
	readonly message: string;
}

export const isUsernameAlreadyTakenValidationError = (error: unknown): error is UsernameAlreadyTakenValidationError => {
	return typeof error === 'object' && error !== null && 'usernameAlreadyTaken' in error && 'message' in error;
};
