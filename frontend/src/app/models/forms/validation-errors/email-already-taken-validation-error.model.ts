export const EMAIL_ALREADY_TAKEN_ERROR: string = 'emailAlreadyTaken';

export interface EmailAlreadyTakenValidationError {
	readonly emailAlreadyTaken: boolean;
	readonly message: string;
}

export const isEmailAlreadyTakenValidationError = (error: unknown): error is EmailAlreadyTakenValidationError => {
	if (typeof error !== 'object' || error === null) {
		return false;
	}

	return Object.keys(error).includes('emailAlreadyTaken') && Object.keys(error).includes('message');
};
