import type { ApplicationConfig } from '@angular/core';
import { provideZoneChangeDetection } from '@angular/core';
import { provideRouter } from '@angular/router';

import { provideHttpClient, withFetch } from '@angular/common/http';
import { provideClientHydration, withEventReplay } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import { environment } from '@environments/environment';
import { provideLumberjack } from '@ngworker/lumberjack';
import { provideLumberjackConsoleDriver } from '@ngworker/lumberjack/console-driver';
import Aura from '@primeng/themes/aura';
import { providePrimeNG, type ThemeType } from 'primeng/config';
import { routes } from './app.routes';

export const appConfig: ApplicationConfig = {
	providers: [
		provideZoneChangeDetection({ eventCoalescing: true }),
		provideRouter(routes),
		provideClientHydration(withEventReplay()),
		providePrimeNG({
			theme: {
				preset: Aura as unknown as ThemeType,
			},
		}),
		provideHttpClient(withFetch()),
		provideAnimations(),
		provideLumberjack({
			levels: environment.logLevels,
		}),
		provideLumberjackConsoleDriver({
			levels: environment.logLevels,
		}),
	],
};
