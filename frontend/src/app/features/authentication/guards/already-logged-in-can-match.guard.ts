// already-logged-in-can-match.guard.ts
import { inject } from '@angular/core';
import type { CanMatchFn, Route, UrlSegment } from '@angular/router';
import { Router } from '@angular/router';
import { LumberjackService } from '@ngworker/lumberjack';
import { AuthenticationService } from '../services/authentication.service';

export const alreadyLoggedInCanMatchGuard: CanMatchFn = (_: Route, segments: UrlSegment[]): boolean => {
	const authService: AuthenticationService = inject(AuthenticationService);
	const router: Router = inject(Router);
	const logger: LumberjackService = inject<LumberjackService>(LumberjackService);

	const isLoginRoute: boolean = segments.some((segment: UrlSegment) => segment.path === 'login');
	const isRegisterRoute: boolean = segments.some((segment: UrlSegment) => segment.path === 'register');

	if (authService.isAuthenticated() && (isLoginRoute || isRegisterRoute)) {
		logger.logDebug(
			'User is already logged in. Preventing lazy load of AuthModule...',
			undefined,
			alreadyLoggedInCanMatchGuard.name
		);
		void router.navigate(['/dashboard']);
		return false;
	}

	return true;
};
