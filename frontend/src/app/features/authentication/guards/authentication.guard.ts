import { inject } from '@angular/core';
import { Router, type CanActivateFn } from '@angular/router';
import { LumberjackService } from '@ngworker/lumberjack';
import { AuthenticationService } from '../services/authentication.service';

export const authenticationGuard: CanActivateFn = async (): Promise<boolean> => {
	const authService: AuthenticationService = inject(AuthenticationService);
	const router: Router = inject(Router);
	const logger: LumberjackService = inject<LumberjackService>(LumberjackService);

	try {
		if (authService.isAuthenticated()) {
			return true;
		}

		await router.navigate(['/auth/login']);
		return false;
	} catch (error: unknown) {
		const errorMessage: string = error instanceof Error ? error.message : 'Unknown error';
		logger.logCritical(`Authentication guard failed: ${errorMessage}`, undefined, 'AuthenticationGuard');
		return false;
	}
};
