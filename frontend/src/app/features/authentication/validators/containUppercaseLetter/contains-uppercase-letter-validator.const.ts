import type { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

const containsUpperCaseRegex: RegExp = /[A-Z]/;

export const CONTAINS_UPPERCASE_LETTER_ERROR: string = 'containsUppercaseLetter';

export const containsUppercaseLetter: ValidatorFn = (control: AbstractControl<string>): ValidationErrors | null => {
	const text: string = control.value;

	if (!containsUpperCaseRegex.test(text)) {
		return { [CONTAINS_UPPERCASE_LETTER_ERROR]: true };
	}

	return null;
};
