import type { ValidationErrors } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { describe, expect, it } from '@jest/globals';
import { CONTAINS_UPPERCASE_LETTER_ERROR, containsUppercaseLetter } from './contains-uppercase-letter-validator.const';

describe(containsUppercaseLetter.name, (): void => {
	const stringsWithoutUppercase: string[] = [
		'',
		'abc',
		'123',
		'hello',
		'test@123',
		'!@#$%^&*()',
		'lowercase_only',
		'123456789',
		'test test',
		'a b c',
	];

	stringsWithoutUppercase.forEach((stringWithoutUppercase: string): void => {
		it(`should return an error object if string does not contain uppercase letter: "${stringWithoutUppercase}"`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(stringWithoutUppercase);

			// Act
			const response: ValidationErrors | null = containsUppercaseLetter(control);

			// Assert
			expect(response).not.toBeNull();
			expect(response).toEqual({ [CONTAINS_UPPERCASE_LETTER_ERROR]: true });
		});
	});

	const stringsWithUppercase: string[] = [
		'A',
		'ABC',
		'Hello',
		'Test123',
		'passwordWithUPPERCASE',
		'MixedCase',
		'With Spaces And Uppercase',
		'lower and UPPER',
		'special!@#With$%^uppercase',
		'123ABC',
	];

	stringsWithUppercase.forEach((stringWithUppercase: string): void => {
		it(`should return null if string contains uppercase letter: "${stringWithUppercase}"`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(stringWithUppercase);

			// Act
			const response: ValidationErrors | null = containsUppercaseLetter(control);

			// Assert
			expect(response).toBeNull();
		});
	});
});
