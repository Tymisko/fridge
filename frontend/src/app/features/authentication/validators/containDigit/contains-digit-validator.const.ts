import type { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

const containsDigitRegex: RegExp = /[0-9]/;

export const CONTAINS_DIGIT_ERROR: string = 'containsDigit';

export const containsDigit: ValidatorFn = (control: AbstractControl<string>): ValidationErrors | null => {
	const text: string = control.value;

	if (!containsDigitRegex.test(text)) {
		return { [CONTAINS_DIGIT_ERROR]: true };
	}

	return null;
};
