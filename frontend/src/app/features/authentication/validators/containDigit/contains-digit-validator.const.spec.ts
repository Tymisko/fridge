import type { ValidationErrors } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { describe, expect, it } from '@jest/globals';
import { CONTAINS_DIGIT_ERROR, containsDigit } from './contains-digit-validator.const';

describe(containsDigit.name, (): void => {
	const stringsWithoutDigit: string[] = [
		'',
		'abc',
		'hello',
		'TEST',
		'noDigits',
		'UPPERCASE',
		'lowercase',
		'test test',
		'Special@#$',
		'no-digits-here',
	];

	stringsWithoutDigit.forEach((stringWithoutDigit: string): void => {
		it(`should return an error object if string does not contain digit: "${stringWithoutDigit}"`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(stringWithoutDigit);

			// Act
			const response: ValidationErrors | null = containsDigit(control);

			// Assert
			expect(response).not.toBeNull();
			expect(response).toEqual({ [CONTAINS_DIGIT_ERROR]: true });
		});
	});

	const stringsWithDigit: string[] = [
		'0',
		'1test',
		'test2',
		'test3test',
		'4TEST',
		'pass5word',
		'Test6Test',
		'with 7 space',
		'special8@#$',
		'mixed9CASE',
		'multiple123digits',
		'digit0in-middle',
	];

	stringsWithDigit.forEach((stringWithDigit: string): void => {
		it(`should return null if string contains digit: "${stringWithDigit}"`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(stringWithDigit);

			// Act
			const response: ValidationErrors | null = containsDigit(control);

			// Assert
			expect(response).toBeNull();
		});
	});
});
