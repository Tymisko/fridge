import type { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

const containsLowerCaseRegex: RegExp = /[a-z]/;

export const CONTAINS_LOWERCASE_LETTER_ERROR: string = 'containsLowercaseLetter';

export const containsLowercaseLetter: ValidatorFn = (control: AbstractControl<string>): ValidationErrors | null => {
	const text: string = control.value;

	if (!containsLowerCaseRegex.test(text)) {
		return { [CONTAINS_LOWERCASE_LETTER_ERROR]: true };
	}

	return null;
};
