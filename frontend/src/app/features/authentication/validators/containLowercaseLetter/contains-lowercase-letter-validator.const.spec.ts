import type { ValidationErrors } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { describe, expect, it } from '@jest/globals';
import { CONTAINS_LOWERCASE_LETTER_ERROR, containsLowercaseLetter } from './contains-lowercase-letter-validator.const';

describe(containsLowercaseLetter.name, (): void => {
	const stringsWithoutLowercase: string[] = [
		'',
		'ABC',
		'123',
		'HELLO',
		'TEST@123',
		'!@#$%^&*()',
		'UPPERCASE_ONLY',
		'123456789',
		'TEST TEST',
		'A B C',
	];

	stringsWithoutLowercase.forEach((stringWithoutLowercase: string): void => {
		it(`should return an error object if string does not contain lowercase letter: "${stringWithoutLowercase}"`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(stringWithoutLowercase);

			// Act
			const response: ValidationErrors | null = containsLowercaseLetter(control);

			// Assert
			expect(response).not.toBeNull();
			expect(response).toEqual({ [CONTAINS_LOWERCASE_LETTER_ERROR]: true });
		});
	});

	const stringsWithLowercase: string[] = [
		'a',
		'abc',
		'Hello',
		'Test123',
		'PASSWORDwithLowercase',
		'mixedCase',
		'with spaces and lowercase',
		'UPPER and lower',
		'special!@#with$%^lowercase',
		'123abc',
	];

	stringsWithLowercase.forEach((stringWithLowercase: string): void => {
		it(`should return null if string contains lowercase letter: "${stringWithLowercase}"`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(stringWithLowercase);

			// Act
			const response: ValidationErrors | null = containsLowercaseLetter(control);

			// Assert
			expect(response).toBeNull();
		});
	});
});
