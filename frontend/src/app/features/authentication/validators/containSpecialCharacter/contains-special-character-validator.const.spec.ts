import type { ValidationErrors } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { describe, expect, it } from '@jest/globals';
import {
	CONTAINS_SPECIAL_CHARACTER_ERROR,
	containsSpecialCharacter,
} from './contains-special-character-validator.const';

describe(containsSpecialCharacter.name, (): void => {
	const stringsWithoutSpecialChar: string[] = [
		'',
		'abc',
		'123',
		'hello',
		'TEST123',
		'noSpecialChars',
		'UPPERCASE',
		'123456789',
		'test test',
		'aBC123',
		'test\ttest',
	];

	stringsWithoutSpecialChar.forEach((stringWithoutSpecialChar: string): void => {
		it(`should return an error object if string does not contain special character: "${stringWithoutSpecialChar}"`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(stringWithoutSpecialChar);

			// Act
			const response: ValidationErrors | null = containsSpecialCharacter(control);

			// Assert
			expect(response).not.toBeNull();
			expect(response).toEqual({ [CONTAINS_SPECIAL_CHARACTER_ERROR]: true });
		});
	});

	const stringsWithSpecialChar: string[] = [
		'!',
		'@test',
		'test#123',
		'Hello!',
		'pass@word',
		'Test$123',
		'With&Space',
		'(parentheses)',
		'under_score',
		'hypen-test',
		'slash/test',
		'back\\slash',
		'question?mark',
		'dot.test',
		'comma,test',
		'[brackets]',
		'{braces}',
		'plus+sign',
		'equals=sign',
		'pipe|test',
	];

	stringsWithSpecialChar.forEach((stringWithSpecialChar: string): void => {
		it(`should return null if string contains special character: "${stringWithSpecialChar}"`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(stringWithSpecialChar);

			// Act
			const response: ValidationErrors | null = containsSpecialCharacter(control);

			// Assert
			expect(response).toBeNull();
		});
	});
});
