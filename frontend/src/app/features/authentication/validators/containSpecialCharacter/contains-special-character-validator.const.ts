import type { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

const containsSpecialCharacterRegex: RegExp = /[^a-zA-Z0-9\s]/;

export const CONTAINS_SPECIAL_CHARACTER_ERROR: string = 'containsSpecialCharacter';

export const containsSpecialCharacter: ValidatorFn = (control: AbstractControl<string>): ValidationErrors | null => {
	const text: string = control.value;

	if (!containsSpecialCharacterRegex.test(text)) {
		return { [CONTAINS_SPECIAL_CHARACTER_ERROR]: true };
	}

	return null;
};
