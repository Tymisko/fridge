import type { ValidationErrors } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { describe, expect, it } from '@jest/globals';
import { emailValidator } from './email-validator.const';

describe(emailValidator.name, (): void => {
	const invalidEmailAddresses: string[] = [
		'plainaddress',
		'@missingusername.com',
		'missingatsign.com',
		'missingdomain@.com',
		'missingdot@domain',
		'missingextension@domain.',
		'invalid@characters!.com',
		'invalid@space inaddress.com',
		'double@@domain.com',
		'missing@domain..com',
		'.leadingdot@domain.com',
		'trailingdot@domain.com.',
		'double..dots@domain.com',
		'no_tld@domain',
		'specialchar@domain..com',
		'@domain.com',
		'user@.domain.com',
		'user@domain,com',
	];

	invalidEmailAddresses.forEach((invalidEmail: string): void => {
		it(`should return an error object if email is ${invalidEmail}`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(invalidEmail);

			// Act
			const response: ValidationErrors | null = emailValidator(control);

			// Assert
			expect(response).not.toBeNull();
			expect(response).toEqual({ InvalidEmail: true });
		});
	});

	const validEmailAddresses: string[] = [
		'john.doe@example.com',
		'jane_doe123@example.co.uk',
		'"first.last"@example.org',
		'user+tag@example.io',
		'"special_chars!#$%&\'*+/=?^_`{|}~"@example.com',
		'name.surname@sub.example.org',
		'user.name+label@example-mail.com',
		'"quoted.name"@example.name',
		'contact@my-domain.info',
		'underscores_are_valid@example.museum',
	];

	validEmailAddresses.forEach((validEmail: string): void => {
		it(`should return null if email is ${validEmail}`, (): void => {
			// Arrange
			const control: FormControl<string | null> = new FormControl<string>(validEmail);

			// Act
			const response: ValidationErrors | null = emailValidator(control);

			// Assert
			expect(response).toBeNull();
		});
	});
});
