import type { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

const EmailRegex: RegExp =
	/^(?!\.)((?!\.\.)[0-9a-z.!#$%&'*+/=?^_`{|}~-]|"[^"\\]*(?:\\.[^"\\]*)*")+(?<!\.)@[0-9a-z](?:[0-9a-z-]{0,61}[0-9a-z])?(?:\.[0-9a-z](?:[0-9a-z-]{0,61}[0-9a-z])?)+$/i;

export const EMAIL_VALIDATION_ERROR: string = 'InvalidEmail';

export const emailValidator: ValidatorFn = (control: AbstractControl<string>): ValidationErrors | null => {
	const email: string = control.value;

	if (!EmailRegex.test(email)) {
		return { [EMAIL_VALIDATION_ERROR]: true };
	}

	return null;
};
