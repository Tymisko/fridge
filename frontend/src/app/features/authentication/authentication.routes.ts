import type { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { alreadyLoggedInCanMatchGuard } from './guards/already-logged-in-can-match.guard';

export const authRoutes: Routes = [
	{
		path: '',
		canMatch: [alreadyLoggedInCanMatchGuard],
		children: [
			{ path: 'register', component: RegisterComponent },
			{ path: 'login', component: LoginComponent },
		],
	},
];
