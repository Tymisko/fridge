export enum RegisterFormControls {
	Username = 'username',
	Email = 'email',
	Password = 'password',
	ConfirmPassword = 'passwordConfirmation',
}
