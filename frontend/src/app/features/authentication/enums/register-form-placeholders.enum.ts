export enum RegisterFormPlaceholders {
	Username = 'Username',
	Email = 'Email',
	Password = 'Password',
	ConfirmPassword = 'Confirm Password',
}
