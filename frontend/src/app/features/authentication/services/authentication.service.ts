import { isPlatformBrowser } from '@angular/common';
import { inject, Injectable, PLATFORM_ID } from '@angular/core';
import { jwtDecode, JwtPayload } from 'jwt-decode';

@Injectable({
	providedIn: 'root',
})
export class AuthenticationService {
	private readonly JWT_TOKEN_KEY: string = 'token';
	private readonly platformId: object = inject(PLATFORM_ID);

	public isAuthenticated(): boolean {
		if (!isPlatformBrowser(this.platformId)) {
			return false;
		}

		const token: string | null = localStorage.getItem(this.JWT_TOKEN_KEY);
		if (token === null) {
			return false;
		}

		if (this.isTokenExpired(token)) {
			this.removeToken();
			return false;
		}

		return true;
	}

	public setToken(token: string): void {
		if (isPlatformBrowser(this.platformId)) {
			localStorage.setItem(this.JWT_TOKEN_KEY, token);
		}
	}

	public getToken(): string | null {
		if (!isPlatformBrowser(this.platformId)) {
			return null;
		}
		return localStorage.getItem(this.JWT_TOKEN_KEY);
	}

	public removeToken(): void {
		if (isPlatformBrowser(this.platformId)) {
			localStorage.removeItem(this.JWT_TOKEN_KEY);
		}
	}

	private isTokenExpired(token: string): boolean {
		const decodedToken: JwtPayload = jwtDecode(token);
		const currentTime: number = Math.floor(Date.now() / 1000);
		if (decodedToken.exp === undefined) {
			return true;
		}

		const isExpired: boolean = decodedToken.exp < currentTime;
		if (isExpired) {
			return true;
		}

		return false;
	}
}
