import { GenerateJwtTokenDto } from '@/app/api/identity-service/dto/generate-jwt-token.dto';
import { IdentityErrorTypes } from '@/app/api/identity-service/enums/identity-errors.enum';
import { IdentityServiceService } from '@/app/api/identity-service/identity-service.service';
import {
	INVALID_PASSWORD_ERROR,
	InvalidPasswordValidationError,
} from '@/app/api/identity-service/models/invalid-password-validation-error.model';
import {
	USER_DOES_NOT_EXIST_ERROR,
	UserDoesNotExistValidationError,
} from '@/app/api/identity-service/models/user-does-not-exists-validation-error.model';
import { ErrorResult } from '@/app/models/results/error-result.model';
import { Result } from '@/app/models/results/result-t.model';
import { Component, DestroyRef, Inject, ViewEncapsulation } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, ValidationErrors, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { FormInputValidationErrorsComponent } from '@app/components/forms/form-input-validation-errors/form-input-validation-errors.component';
import { FormTextInputComponent } from '@app/components/forms/form-text-input/form-text-input.component';
import { AuthenticationValidationRules } from '@app/features/authentication/enums/authentication-validation-rules.enum';
import { emailValidator } from '@app/features/authentication/validators/email/email-validator.const';
import { LumberjackService } from '@ngworker/lumberjack';
import { Button } from 'primeng/button';
import { Card } from 'primeng/card';
import { IftaLabel } from 'primeng/iftalabel';
import { Password } from 'primeng/password';
import { finalize } from 'rxjs';
import { LoginFormControls } from '../../enums/login-form-controls.enum';
import { LoginFormPlaceholders } from '../../enums/login-form-placeholders.enum';
import type { LoginForm } from '../../models/login-form.model';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
	standalone: true,
	imports: [
		ReactiveFormsModule,
		Card,
		Button,
		FormTextInputComponent,
		IftaLabel,
		Password,
		FormInputValidationErrorsComponent,
		RouterLink,
	],
	templateUrl: './login.component.html',
	styleUrl: './login.component.scss',
	encapsulation: ViewEncapsulation.Emulated,
})
export class LoginComponent {
	protected readonly form: FormGroup<LoginForm>;
	protected readonly emailControl: FormControl<string>;
	protected readonly passwordControl: FormControl<string>;
	protected readonly LoginFormControls: typeof LoginFormControls = LoginFormControls;
	protected readonly LoginFormPlaceholders: typeof LoginFormPlaceholders = LoginFormPlaceholders;

	public constructor(
		private readonly formBuilder: FormBuilder,
		@Inject(DestroyRef) private readonly destroyRef: DestroyRef,
		private readonly logger: LumberjackService,
		private readonly identityService: IdentityServiceService,
		private readonly authenticationService: AuthenticationService,
		private readonly router: Router
	) {
		const required: typeof Validators.required = Validators.required.bind(Validators);

		this.emailControl = this.formBuilder.nonNullable.control<string>('', [
			required,
			emailValidator,
			Validators.maxLength(AuthenticationValidationRules.EMAIL_MAX_LENGTH),
		]);

		this.passwordControl = this.formBuilder.nonNullable.control<string>('', [
			required,
			Validators.maxLength(AuthenticationValidationRules.PASSWORD_MAX_LENGTH),
		]);

		this.form = this.formBuilder.nonNullable.group<LoginForm>({
			[LoginFormControls.Email]: this.emailControl,
			[LoginFormControls.Password]: this.passwordControl,
		});

		this.logger.logInfo(`${LoginComponent.name} initialized`);
	}

	protected onSubmit(): void {
		const generateJwtTokenDto: GenerateJwtTokenDto = {
			email: this.emailControl.value,
			password: this.passwordControl.value,
		};

		this.identityService
			.generateJwtToken(generateJwtTokenDto)
			.pipe(
				takeUntilDestroyed(this.destroyRef),
				finalize(() => {
					this.logger.logInfo(`Unsubscribing from generateJwtToken observable`);
				})
			)
			.subscribe({
				next: (result: Result<string>): void => {
					if (result.isSuccess) {
						this.logger.logInfo(`Setting token and navigating to dashboard`);
						this.authenticationService.setToken(result.value);
						void this.router.navigate(['/dashboard']);
						return;
					}

					const error: ErrorResult = result.error;
					this.logger.logDebug(
						`An error occured while generating JWT token: ${error.type}, ${error.message}`
					);
					this.setFormErrors(error);
				},
			});
	}

	private setFormErrors(errorResult: ErrorResult): void {
		if (errorResult.type === IdentityErrorTypes.UserDoesNotExist) {
			const error: UserDoesNotExistValidationError = {
				userDoesNotExist: true,
				message: errorResult.message,
			};
			const emailErrors: ValidationErrors = {
				...this.emailControl.errors,
				...{ [USER_DOES_NOT_EXIST_ERROR]: error },
			};
			this.emailControl.setErrors(emailErrors);
			return;
		}

		if (errorResult.type === IdentityErrorTypes.InvalidPassword) {
			const error: InvalidPasswordValidationError = {
				invalidPassword: true,
				message: errorResult.message,
			};
			const passwordErrors: ValidationErrors = {
				...this.passwordControl.errors,
				...{ [INVALID_PASSWORD_ERROR]: error },
			};
			this.passwordControl.setErrors(passwordErrors);
		}
	}
}
