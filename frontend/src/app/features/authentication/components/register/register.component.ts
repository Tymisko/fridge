import { Component, DestroyRef, Inject, ViewEncapsulation } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import {
	AbstractControl,
	FormBuilder,
	FormControl,
	FormGroup,
	ReactiveFormsModule,
	ValidationErrors,
	Validators,
} from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { CreateUserDto } from '@app/api/identity-service/dto/create-user.dto';
import { IdentityErrorTypes } from '@app/api/identity-service/enums/identity-errors.enum';
import { IdentityServiceService } from '@app/api/identity-service/identity-service.service';
import { FormInputValidationErrorsComponent } from '@app/components/forms/form-input-validation-errors/form-input-validation-errors.component';
import { FormTextInputComponent } from '@app/components/forms/form-text-input/form-text-input.component';
import { AuthenticationValidationRules } from '@app/features/authentication/enums/authentication-validation-rules.enum';
import { emailValidator } from '@app/features/authentication/validators/email/email-validator.const';
import {
	EMAIL_ALREADY_TAKEN_ERROR,
	EmailAlreadyTakenValidationError,
} from '@app/models/forms/validation-errors/email-already-taken-validation-error.model';
import {
	USERNAME_ALREADY_TAKEN_ERROR,
	UsernameAlreadyTakenValidationError,
} from '@app/models/forms/validation-errors/username-already-taken-validation-error.model';
import { ErrorResult } from '@app/models/results/error-result.model';
import { Result } from '@app/models/results/result-t.model';
import { LumberjackService } from '@ngworker/lumberjack';
import { UUID } from 'node:crypto';
import { Button } from 'primeng/button';
import { Card } from 'primeng/card';
import { IftaLabel } from 'primeng/iftalabel';
import { Password } from 'primeng/password';
import { finalize } from 'rxjs';
import { RegisterFormControls } from '../../enums/register-form-controls.enum';
import { RegisterFormPlaceholders } from '../../enums/register-form-placeholders.enum';
import type { RegisterForm } from '../../models/register-form.model';
import { containsDigit } from '../../validators/containDigit/contains-digit-validator.const';
import { containsLowercaseLetter } from '../../validators/containLowercaseLetter/contains-lowercase-letter-validator.const';
import { containsSpecialCharacter } from '../../validators/containSpecialCharacter/contains-special-character-validator.const';
import { containsUppercaseLetter } from '../../validators/containUppercaseLetter/contains-uppercase-letter-validator.const';

@Component({
	standalone: true,
	imports: [
		ReactiveFormsModule,
		Card,
		Button,
		FormTextInputComponent,
		IftaLabel,
		Password,
		FormInputValidationErrorsComponent,
		RouterLink,
	],
	templateUrl: './register.component.html',
	styleUrl: './register.component.scss',
	encapsulation: ViewEncapsulation.Emulated,
})
export class RegisterComponent {
	protected readonly form: FormGroup<RegisterForm>;
	protected readonly usernameControl: FormControl<string>;
	protected readonly emailControl: FormControl<string>;
	protected readonly passwordControl: FormControl<string>;
	protected readonly confirmPasswordControl: FormControl<string>;
	protected readonly RegisterFormControls: typeof RegisterFormControls = RegisterFormControls;
	protected readonly RegisterFormPlaceholders: typeof RegisterFormPlaceholders = RegisterFormPlaceholders;
	protected readonly formErrors: ErrorResult[] = [];

	public constructor(
		private readonly formBuilder: FormBuilder,
		@Inject(DestroyRef) private readonly destroyRef: DestroyRef,
		private readonly identityService: IdentityServiceService,
		private readonly logger: LumberjackService,
		private readonly router: Router
	) {
		const required: typeof Validators.required = Validators.required.bind(Validators);

		this.usernameControl = this.formBuilder.nonNullable.control<string>('', [
			required,
			Validators.minLength(AuthenticationValidationRules.USERNAME_MIN_LENGTH),
			Validators.maxLength(AuthenticationValidationRules.USERNAME_MAX_LENGTH),
		]);
		this.emailControl = this.formBuilder.nonNullable.control<string>('', [
			required,
			emailValidator,
			Validators.maxLength(AuthenticationValidationRules.EMAIL_MAX_LENGTH),
		]);
		this.passwordControl = this.formBuilder.nonNullable.control<string>('', [
			required,
			Validators.minLength(AuthenticationValidationRules.PASSWORD_MIN_LENGTH),
			Validators.maxLength(AuthenticationValidationRules.PASSWORD_MAX_LENGTH),
			containsLowercaseLetter,
			containsUppercaseLetter,
			containsDigit,
			containsSpecialCharacter,
		]);
		this.confirmPasswordControl = this.formBuilder.nonNullable.control<string>('', [
			this.passwordMatchValidator.bind(this),
		]);

		this.form = this.formBuilder.nonNullable.group<RegisterForm>({
			[RegisterFormControls.Username]: this.usernameControl,
			[RegisterFormControls.Email]: this.emailControl,
			[RegisterFormControls.Password]: this.passwordControl,
			[RegisterFormControls.ConfirmPassword]: this.confirmPasswordControl,
		});

		this.logger.logInfo(`${RegisterComponent.name} initialized`);
	}

	private passwordMatchValidator(control: AbstractControl<string>): ValidationErrors | null {
		const password: string = this.passwordControl.value;
		const confirmPassword: string = control.value;

		if (password !== confirmPassword) {
			return { passwordMatch: false };
		}

		return null;
	}

	protected onSubmit(): void {
		if (!this.form.valid) {
			return;
		}

		const createUserDto: CreateUserDto = {
			username: this.usernameControl.value,
			email: this.emailControl.value,
			password: this.passwordControl.value,
		};
		this.identityService
			.createUser(createUserDto)
			.pipe(
				takeUntilDestroyed(this.destroyRef),
				finalize((): void => {
					this.logger.logDebug('Unsubscribing create user request');
				})
			)
			.subscribe({
				next: (createUserResult: Result<UUID>): void => {
					if (createUserResult.isSuccess) {
						void this.router.navigate(['/auth/login']);
						return;
					}

					const error: ErrorResult = createUserResult.error;
					this.logger.logDebug(
						`Creating user failed with error of type: ${error.type}, message: ${error.message}`,
						undefined,
						RegisterComponent.name
					);
					this.setFormError(error);
				},
			});
	}

	private setFormError(errorResult: ErrorResult): void {
		switch (errorResult.type) {
			case IdentityErrorTypes.UserWithEmailAlreadyExists: {
				const error: EmailAlreadyTakenValidationError = {
					emailAlreadyTaken: true,
					message: errorResult.message,
				};
				const emailControlErrors: ValidationErrors = {
					...this.emailControl.errors,
					...{ [EMAIL_ALREADY_TAKEN_ERROR]: error },
				};
				this.emailControl.setErrors(emailControlErrors);
				break;
			}

			case IdentityErrorTypes.UserWithUsernameAlreadyExists: {
				const error: UsernameAlreadyTakenValidationError = {
					usernameAlreadyTaken: true,
					message: errorResult.message,
				};
				const usernameControlErrors: ValidationErrors = {
					...this.usernameControl.errors,
					...{ [USERNAME_ALREADY_TAKEN_ERROR]: error },
				};
				this.usernameControl.setErrors(usernameControlErrors);
				break;
			}

			default: {
				throw new Error(`Unknown error: ${errorResult.type}`);
			}
		}
		this.form.updateValueAndValidity();
	}
}
