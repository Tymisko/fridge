import { Component } from '@angular/core';

@Component({
	selector: 'fdg-dashboard',
	standalone: true,
	imports: [],
	template: `<p>{{ title }}</p>`,
	styles: ``,
})
export class DashboardComponent {
	protected readonly title: string = 'Dashboard';
}
