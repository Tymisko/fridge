import { readdirSync, readFileSync, statSync, writeFileSync } from 'fs';
import { dirname, join } from 'path';
import type { TsConfigJson } from 'type-fest';
import { fileURLToPath } from 'url';

const __filename: string = fileURLToPath(import.meta.url);
const __dirname: string = dirname(__filename);

const TSCONFIG_PATH: string = join(__dirname, '../../', 'tsconfig.json');
const SRC_DIR: string = join(__dirname, '../../', 'src');

function getDirectories(srcPath: string): string[] {
	return readdirSync(srcPath).filter((file: string) => statSync(join(srcPath, file)).isDirectory());
}

function updateTsConfig(): void {
	const tsconfig: TsConfigJson = JSON.parse(readFileSync(TSCONFIG_PATH, 'utf-8')) as TsConfigJson;
	const srcDirs: string[] = getDirectories(SRC_DIR);
	const paths: Record<string, string[]> = {};
	paths['@/*'] = ['src/*'];
	srcDirs.forEach((dir: string) => {
		paths[`@${dir}/*`] = [`src/${dir}/*`];
	});

	if (!tsconfig.compilerOptions) {
		console.error('tsconfig.json does not have compilerOptions');
		process.exit(1);
	}

	tsconfig.compilerOptions.paths = paths;
	tsconfig.compilerOptions.baseUrl = '.';

	writeFileSync(TSCONFIG_PATH, JSON.stringify(tsconfig, null, 2));

	console.log('Updated tsconfig.json paths:', paths);
}

updateTsConfig();
