import type { Config } from 'jest';
import presets from 'jest-preset-angular/presets';
import { type JestConfigWithTsJest, pathsToModuleNameMapper } from 'ts-jest';

import tsconfig from './tsconfig.json';

const esmPreset: Config = presets.createEsmPreset();

export default {
	...esmPreset,
	moduleNameMapper: {
		...esmPreset.moduleNameMapper,
		...pathsToModuleNameMapper(tsconfig.compilerOptions.paths, { prefix: '<rootDir>' }),
		rxjs: '<rootDir>/node_modules/rxjs/dist/bundles/rxjs.umd.js',
	},
	setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
	testEnvironment: 'jsdom',
	testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/dist/', '<rootDir>/src/test.ts'],
	transform: {
		'^.+\\.(ts|js|html|svg)$': [
			'jest-preset-angular',
			{
				tsconfig: '<rootDir>/tsconfig.spec.json',
				stringifyContentPathRegex: '\\.(html|svg)$',
				useESM: true,
			},
		],
	},
	coverageDirectory: '<rootDir>/coverage',
	coverageProvider: 'v8',
	collectCoverageFrom: [
		'<rootDir>/src/app/**/*.ts',
		'!<rootDir>/src/app/**/*.spec.ts',
		'!<rootDir>/src/app/**/*.model.ts',
		'!<rootDir>/src/app/**/*.enum.ts',
		'!<rootDir>/src/app/**/*.dto.ts',
		'!<rootDir>/src/app/**/*.config.ts',
		'!<rootDir>/src/app/**/*.routes.ts',
		'!<rootDir>/src/app/**/*.config.server.ts',
	],
	coverageReporters: ['text', 'html', 'cobertura'],
	reporters: [
		'default',
		[
			'jest-junit',
			{
				outputDirectory: '<rootDir>/reports',
				outputName: 'junit.xml',
			},
		],
		[
			'jest-html-reporters',
			{
				filename: 'jest-report.html',
				publicPath: 'reports',
			},
		],
	],
} satisfies JestConfigWithTsJest;
