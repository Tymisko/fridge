import { APP_BASE_HREF } from '@angular/common';
import { CommonEngine } from '@angular/ssr';
import type { Express, NextFunction, Request, Response } from 'express';
import express from 'express';
import type { IncomingHttpHeaders } from 'node:http';
import { dirname, join, resolve } from 'node:path';
import { fileURLToPath } from 'node:url';
import bootstrap from './src/main.server';

export function app(): Express {
	const server: Express = express();
	const serverDistFolder: string = dirname(fileURLToPath(import.meta.url));
	const browserDistFolder: string = resolve(serverDistFolder, '../browser');
	const indexHtml: string = join(serverDistFolder, 'index.server.html');

	const commonEngine: CommonEngine = new CommonEngine();

	server.set('view engine', 'html');
	server.set('views', browserDistFolder);

	// Add a debug middleware before static files
	server.use((req: Request, _res: Response, next: NextFunction): void => {
		console.log(`Incoming request: ${req.method} ${req.url}`);
		next();
	});

	// Serve static files from /browser
	server.get(
		'*.*',
		express.static(browserDistFolder, {
			maxAge: '1y',
		})
	);

	// All regular routes use the Angular engine
	server.get('**', (req: Request, res: Response, next: NextFunction): void => {
		console.log(`SSR handling route: ${req.originalUrl}`);
		const {
			protocol,
			originalUrl,
			baseUrl,
			headers,
		}: {
			protocol: string;
			originalUrl: string;
			baseUrl: string;
			headers: IncomingHttpHeaders;
		} = req;
		console.log(`Rendering route ${originalUrl}`);

		commonEngine
			.render({
				bootstrap,
				documentFilePath: indexHtml,
				url: `${protocol}://${headers.host ?? 'localhost'}${originalUrl}`,
				publicPath: browserDistFolder,
				providers: [{ provide: APP_BASE_HREF, useValue: baseUrl }],
			})
			.then((html: string) => {
				res.send(html);
			})
			.catch((err: unknown) => {
				next(err);
			});
	});

	return server;
}

function run(): void {
	const port: number = Number(process.env['PORT']) || 4000;

	// Start up the Node server
	const server: Express = app();
	server.listen(port, (): void => {
		console.log(`Node Express server listening on http://localhost:${port.toString()}`);
	});
}

run();
