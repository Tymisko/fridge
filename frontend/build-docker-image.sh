#!/bin/sh

print_usage() {
    echo "Usage: $0 [tag] [port] [node_version]"
    echo ""
    echo "Build a Docker image for the web application"
    echo ""
    echo "Arguments:"
    echo "  tag          Optional. Tag for the Docker image. If not provided, version from package.json will be used"
    echo "  port         Optional. Port to expose (default: 4000)"
    echo "  node_version Optional. Node.js version to use (default: from environment NODE_VERSION)"
    echo ""
    echo "Examples:"
    echo "  $0                    # Uses version from package.json"
    echo "  $0 dev               # Creates dev tag"
    echo "  $0 prod 4000 18.0.0  # Creates prod tag with specific port and Node version"
    echo ""
    echo "Environment variables:"
    echo "  CI_REGISTRY_IMAGE    Optional. Registry path for the image (default: fridge)"
    echo "  CI                   Optional. If set to 'true', saves image to tar file"
    echo "  NODE_VERSION         Optional. Node.js version to use if not specified as argument"
}

# Show usage if --help or -h is passed
if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    print_usage
    exit 0
fi

# Validate package.json exists
if [ ! -f "package.json" ]; then
    echo "❌ Error: package.json not found"
    echo "Please run this script from the frontend directory"
    echo ""
    print_usage
    exit 1
fi

# Validate Dockerfile exists
if [ ! -f "Dockerfile" ]; then
    echo "❌ Error: Dockerfile not found"
    echo "Please ensure Dockerfile exists in the frontend directory"
    echo ""
    print_usage
    exit 1
fi

# Get version from package.json
VERSION=$(grep '"version":' package.json | cut -d'"' -f4)
if [ -z "$VERSION" ]; then
    echo "❌ Error: Could not extract version from package.json"
    exit 1
fi

TIMESTAMP=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
TAG=${1:-$VERSION}      # Use first argument as tag, fallback to version
PORT=${2:-4000}         # Use second argument as port, fallback to 4000
NODE_VERSION=${3:-$NODE_VERSION}  # Use third argument as node version, fallback to env var
REGISTRY=${CI_REGISTRY_IMAGE:-fridge}

# Validate NODE_VERSION is set
if [ -z "$NODE_VERSION" ]; then
    echo "❌ Error: Node version not specified and NODE_VERSION environment variable is not set"
    echo ""
    print_usage
    exit 1
fi

echo "🔨 Building Docker image"
echo "Version: $VERSION"
echo "Tag: $TAG"
echo "Port: $PORT"
echo "Registry: $REGISTRY"
echo "Node Version: $NODE_VERSION"
echo ""

# Build the image
docker buildx build \
    -t $REGISTRY/webapp:$TAG \
    --build-arg PORT=$PORT \
    --build-arg NODE_VERSION=$NODE_VERSION \
    --no-cache \
    --pull \
    --platform=linux/amd64 \
    --label=org.opencontainers.image.created=$TIMESTAMP \
    --label=org.opencontainers.image.version=$VERSION \
    --label="org.opencontainers.image.authors=Jan Urbaś" \
    -f Dockerfile \
    .

BUILD_SUCCESS=$?

if [ $BUILD_SUCCESS -eq 0 ]; then
    echo "\n🎉 Docker image built successfully!"
    
    # Save image if in CI environment
    if [ "$CI" = "true" ]; then
        echo "💾 Saving image to tar file..."
        docker save $REGISTRY/webapp:$TAG > image.tar
    fi
else
    echo "\n❌ Error building Docker image"
    exit 1
fi 
