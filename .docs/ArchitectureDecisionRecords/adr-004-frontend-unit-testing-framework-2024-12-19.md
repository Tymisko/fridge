# Choosing a New Testing Framework for Angular 18

- **Status**: Accepted
- **Deciders**: Jan Urbaś
- **Date**: 2024-12-19

## Context
With the deprecation of Karma in Angular 16, we need to select a new testing framework for our Angular 18 project. We require a mature solution that is compatible with Angular 18 and will have continued support. Our focus is primarily on unit testing, implementing the AAA (Arrange-Act-Assert) pattern with mocked dependencies for all System Under Test (SUT) components.

## Decision Drivers 
- Maturity and stability of the testing framework
- Compatibility with Angular 18 and future versions
- Strong community and developer support
- Efficient unit testing capabilities
- Integration with existing development workflow
- Performance and execution speed

## Considered Options
- Jest
- Jasmine with Web Test Runner
- Vitest
- Cypress
- Angular Testing Library
- Spectator

## Decision Outcome
Chosen option: "Jest", because it offers a mature, well-supported solution that is officially integrated with Angular 18, provides excellent unit testing capabilities, and has a large, active community.

### Positive Consequences
- Improved testing performance and speed
- Better integration with Angular's build process
- Extensive community support and resources
- Simplified configuration and setup

### Negative Consequences
- Potential learning curve for developers familiar with Karma/Jasmine
- Some existing tests may need to be refactored

## Pros and Cons of the Options

### Jest
- Good, because it's officially supported by Angular as of version 16
- Good, because it has a large, active community and extensive documentation
- Good, because it provides a rich set of matchers and mocking capabilities
- Bad, because it may require some adjustments for developers used to Karma/Jasmine
- Bad, because it can be slower than other testing frameworks in large projects

### Jasmine with Web Test Runner
- Good, because it maintains similarity with the previous Karma/Jasmine setup
- Good, because it's a familiar option for many Angular developers
- Good, because it's still supported and modernized for Angular 18
- Bad, because it may not offer the same level of performance improvements as Jest
- Bad, because it might not have as extensive future support compared to Jest

### Vitest
- Good, because it's designed to work with Vite, which is now used in Angular 18
- Good, because it offers significantly faster test execution compared to Jest in some scenarios
- Good, because it's compatible with most of the Jest API, allowing easy migration
- Bad, because it may require more setup effort if not using Vite
- Bad, because it has less extensive documentation and community support compared to Jest

### Cypress
- Good, because it supports both E2E and component testing
- Good, because it offers a real-time browser rendering and intelligent feedback
- Good, because it has strong community support and extensive documentation
- Bad, because it's primarily designed for E2E testing, which is not our main focus
- Bad, because it doesn't support native mobile testing

### Angular Testing Library
- Good, because it focuses on testing components from a user's perspective
- Good, because it encourages more maintainable tests
- Good, because it simplifies test spec configuration
- Bad, because it has a steeper learning curve
- Bad, because official documentation might be considered relatively limited

### Spectator
- Good, because it reduces boilerplate in unit tests
- Good, because it can be used in combination with Jest for a powerful testing setup
- Good, because it provides a readable and concise API
- Bad, because it has a learning curve that might be considered steep
- Bad, because official documentation might be considered relatively limited
