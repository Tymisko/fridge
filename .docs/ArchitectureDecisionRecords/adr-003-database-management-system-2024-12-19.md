# Choosing Database Management System

- **Status**: Accepted
- **Deciders**: Jan Urbaś
- **Date**: 2024-12-19

## Context

We need to select a Database Management System (DBMS) for our application. The DBMS must integrate seamlessly with .NET 8, be suitable for a microservices architecture, and run in a containerized environment like Docker. The solution must also be free for commercial use, mature, well-maintained, and supported by a strong community. Advanced features and scalability are crucial to support complex microservice-based architectures.

## Decision Drivers

- Free for commercial use.
- Compatibility with .NET 8.
- Ability to run in Docker containers.
- Active maintenance and strong community support.
- Suitability for microservice architectures.
- Maturity and stability of the solution.

## Considered Options

- PostgreSQL
- MySQL
- MariaDB
- SQLite
- Microsoft SQL Server Express

## Decision Outcome

Chosen option: **PostgreSQL**, because it fulfills all the decision drivers while providing advanced features for microservice architecture, strong community support, and excellent compatibility with .NET 8 and Docker.

### Positive Consequences

- PostgreSQL is a highly mature, stable, and well-supported DBMS.
- It integrates seamlessly with .NET 8 and supports containerized environments like Docker.
- Offers advanced features such as JSONB, full-text search, and robust scalability, making it suitable for microservice-based systems.
- Free for commercial use under the PostgreSQL license.

### Negative Consequences

- Steeper learning curve for developers unfamiliar with PostgreSQL compared to simpler systems like MySQL.
- May require more configuration and optimization for specific use cases.

## Pros and Cons of the Options

### PostgreSQL

- **Good**: Mature, stable, and actively maintained with a large community.
- **Good**: Advanced features (e.g., JSONB, full-text search) support modern application needs.
- **Good**: Highly compatible with Docker and .NET 8.
- **Good**: Free for commercial use.
- **Bad**: Slightly steeper learning curve for developers unfamiliar with it.

### MySQL

- **Good**: Widely adopted and actively maintained.
- **Good**: Compatible with Docker and .NET 8.
- **Good**: Free for commercial use under the GPL.
- **Bad**: Limited advanced features compared to PostgreSQL (e.g., no native JSONB).
- **Bad**: Licensing restrictions (GPL) may require careful evaluation for certain use cases.

### MariaDB

- **Good**: A drop-in replacement for MySQL with additional features.
- **Good**: Fully open-source and free for commercial use.
- **Good**: Compatible with Docker and .NET 8.
- **Bad**: Smaller community compared to PostgreSQL and MySQL.
- **Bad**: May lack some advanced features present in PostgreSQL.

### SQLite

- **Good**: Lightweight and easy to set up.
- **Good**: Free for commercial use and public domain.
- **Good**: Can be easily embedded in .NET applications.
- **Bad**: Not suitable for high-concurrency scenarios in microservices.
- **Bad**: Lacks advanced features needed for complex microservice architectures.

### Microsoft SQL Server Express

- **Good**: Official support for .NET and compatibility with Docker.
- **Good**: Free for commercial use (Express edition).
- **Bad**: Limited scalability compared to PostgreSQL or MySQL.
- **Bad**: Smaller community and less active open-source development compared to PostgreSQL.
