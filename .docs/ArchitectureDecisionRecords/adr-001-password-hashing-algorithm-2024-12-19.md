# Choosing Secure Password Hashing Algorithm

- **Status**: Accepted
- **Deciders**: Jan Urbaś
- **Date**: 2024-12-19

## Context
The application requires a secure password hashing algorithm to safeguard user credentials against modern threats, including brute-force attacks and hardware-accelerated attacks using GPUs or ASICs. The chosen algorithm must align with industry best practices and provide configurability to balance performance and security.

## Decision Drivers
- High resistance to brute-force and hardware-accelerated attacks.
- Compliance with modern security standards.
- Configurability to suit varying application needs.
- Compatibility with .NET 8 and availability as a free solution.

## Considered Options
- Argon2
- PBKDF2
- BCrypt

## Decision Outcome
Chosen option: **"Argon2"**, because it offers the best resistance to modern attacks, supports configurability for performance and security, and is considered the most secure algorithm by industry standards.

### Positive Consequences
- Provides high security due to memory-hard design, making brute-force attacks expensive.
- Configurable parameters (memory, time cost, parallelism) enable adaptability to different use cases.
- Established as the leading password hashing algorithm by the Password Hashing Competition.

### Negative Consequences
- Requires integration of a third-party library since it is not natively available in .NET 8.

## Pros and Cons of the Options
### Option 1: Argon2
- **Good**: Industry leader in password hashing security.
- **Good**: Memory-hard design resists GPU/ASIC attacks.
- **Good**: Configurability allows fine-tuning for performance and security.
- **Bad**: Not included in .NET standard libraries; requires a third-party package.

### Option 2: PBKDF2
- **Good**: Built into .NET as `Rfc2898DeriveBytes`.
- **Good**: Simple to implement with native support.
- **Bad**: Less resistant to modern GPU/ASIC attacks.
- **Bad**: Computational cost increases rapidly for similar security levels compared to Argon2.

### Option 3: BCrypt
- **Good**: Well-established, secure, and widely used.
- **Good**: Adaptive to hardware improvements by increasing cost factor.
- **Bad**: Lacks memory-hard design, making it less resistant to hardware-accelerated attacks.
- **Bad**: Requires a third-party library for integration into .NET.

