# Handling Api Error Responses

- **Status**: Accepted
- **Deciders**: Jan Urbaś
- **Date**: 2024-12-21


## Context
The issue motivating this decision is the need to handle API error responses (e.g., user with the same email already exists) effectively in Angular applications without throwing exceptions. Throwing exceptions introduces unnecessary complexity in error handling and debugging. Additionally, the solution should minimize data mapping to reduce overhead while adhering to Domain-Driven Design (DDD) principles and maintaining compatibility with Angular 18 and later versions.

## Decision Drivers
- Avoid throwing exceptions for predictable errors.
- Minimize data mapping while still transforming responses into a usable frontend format.
- Ensure alignment with Domain-Driven Design principles.
- Guarantee compatibility and executability in Angular 18+.

## Considered Options
- **Option 1**: Implementing a Result Pattern on the frontend.
- **Option 2**: Using HTTP interceptors for global error handling and data mapping.
- **Option 3**: Returning strongly typed error objects from the service layer.
- **Option 4**: Employing RxJS operators to differentiate between success and error cases.

## Decision Outcome
Chosen option: **"Option 1: Implementing a Result Pattern on the frontend"**, because it provides a clean, maintainable approach that avoids exceptions, aligns well with DDD principles, and allows components to handle domain-specific logic with minimal data mapping.

### Positive Consequences
- Consistency between frontend and backend (e.g., shared Result Pattern approach).
- Clear separation of concerns: services handle API communication, while components handle view-specific logic.
- Improved maintainability and readability of error handling logic.
- Compatibility with Angular 18+.

### Negative Consequences
- Slight increase in boilerplate code for defining and handling Result objects.
- Requires additional effort to design and implement a shared Result Pattern if it is not already present.

## Pros and Cons of the Options
### Option 1: Implementing a Result Pattern on the frontend
Description: A Result Pattern is used where the service returns a `Result<T>` object containing either a success payload or an error message.

- **Good**, because it avoids throwing exceptions for predictable errors.
- **Good**, because it enables explicit handling of success and error cases in the consuming components.
- **Good**, because it aligns with DDD principles by encapsulating domain-specific error handling logic.
- **Bad**, because it introduces additional boilerplate code for defining and processing Result objects.

### Option 2: Using HTTP interceptors for global error handling and data mapping
Description: HTTP interceptors map API errors to a standardized format globally.

- **Good**, because it centralizes error handling and reduces the need for repetitive logic in services.
- **Good**, because it minimizes mapping logic in individual services.
- **Bad**, because it may not adhere strictly to DDD principles, as it separates error mapping from domain logic.
- **Bad**, because it could lead to over-generalized error handling, making it harder to implement domain-specific logic in components.

### Option 3: Returning strongly typed error objects from the service layer
Description: The service layer returns domain-specific error objects alongside success responses using a union type (`T | ErrorObject`).

- **Good**, because it allows strongly typed error handling at the component level.
- **Good**, because it reduces reliance on generic error handling mechanisms, making the application more predictable.
- **Good**, because it avoids exceptions and allows for explicit control over error processing.
- **Bad**, because it requires significant changes to service definitions and may increase type complexity.
- **Bad**, because it can lead to additional boilerplate in managing union types.

### Option 4: Employing RxJS operators to differentiate between success and error cases
Description: Use RxJS `catchError` and `of` operators to transform HTTP errors into consumable observable streams.

- **Good**, because it leverages Angular's native RxJS framework to handle errors at the stream level.
- **Good**, because it avoids exceptions and allows services to transform errors into domain-specific objects.
- **Bad**, because it may lead to scattered error-handling logic if not managed carefully.
- **Bad**, because it can make code less readable when streams become overly complex.

### Option 5: Directly mapping API error codes to component states
Description: Services return raw or minimally transformed API error responses, and components handle mapping error codes to messages.

- **Good**, because it minimizes data mapping in services, keeping the logic lightweight.
- **Good**, because it allows for flexible and localized error handling in components.
- **Bad**, because it couples components more tightly with API-specific error formats.
- **Bad**, because it violates the principle of separating domain logic from UI logic, reducing maintainability.
