# Short Title of Solved Problem

- **Status**: [proposed, accepted, rejected, deprecated, superseded]
- **Deciders**: [list everyone involved in the decision]
- **Date**: [YYYY-MM-DD when decision was last updated]


## Context
What is the issue that we're seeing that is motivating this decision or change?

## Decision Drivers 
- [driver 1, e.g., improve maintainability]
- [driver 2, e.g., a force, facing concern, …]

## Considered Options
- [option 1]
- [option 2]

## Decision Outcome
Chosen option: "[option 1]", because [justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force force | … | comes out best (see below)].

### Positive Consequences
[e.g., improvement of quality attribute satisfaction, follow-up decisions required, …]
…
### Negative Consequences
[e.g., compromising quality attribute, follow-up decisions required, …]
…

## Pros and Confs of the Options
### Option 1
[example | description | pointer to more information | …]

- Good, because [argument a]
- Good, because [argument b]
- Bad, because [argument c]
…
### Option 2
[example | description | pointer to more information | …]

- Good, because [argument a]
- Good, because [argument b]
- Bad, because [argument c]
