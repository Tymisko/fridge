#!/bin/bash

REQUIRED_VARS=(
    "POSTGRES_USER"
    "POSTGRES_PASSWORD"
    "POSTGRES_DB"
    "POSTGRES__PORT"
    "POSTGRES__VERSION"
    "RABBITMQ_DEFAULT_USER"
    "RABBITMQ_DEFAULT_PASS"
    "RABBITMQ_ERLANG_COOKIE"
    "RABBITMQ_NODENAME"
    "RABBITMQ__PORT"
    "RABBITMQ__VERSION"
    "SERVICE_NAME"
)

is_integer() {
    [[ "$1" =~ ^[0-9]+$ ]]
}

ERRORS=()

echo "Validating environment variables..."
for VAR in "${REQUIRED_VARS[@]}"; do
    if [ -z "${!VAR}" ]; then
        ERRORS+=("$VAR is not set")
    fi
done

if ! is_integer "${POSTGRES__PORT:-}" 2>/dev/null; then
    ERRORS+=("POSTGRES__PORT must be an integer")
fi

if ! is_integer "${RABBITMQ__PORT:-}" 2>/dev/null; then
    ERRORS+=("RABBITMQ__PORT must be an integer")
fi

if [ ${#ERRORS[@]} -gt 0 ]; then
    echo "Environment variable validation failed:"
    for ERROR in "${ERRORS[@]}"; do
        echo "- $ERROR"
    done
    exit 1
else
    echo "All required environment variables are set and valid."
fi
