#!/bin/bash

echo "Integration Tests Script is starting..."

set -e

if [ $# -ne 1 ]; then
  echo "Usage: $0 <service-name>"
  echo "Example: $0 identity"
  exit 1
fi

SERVICE_NAME="${1}"
SERVICE_NAME_FIRST_LETTER_CAPITALIZED=$(echo "$SERVICE_NAME" | awk "{print toupper(substr(\$0,1,1))tolower(substr(\$0,2))}");
echo "SERVICE_NAME_FIRST_LETTER_CAPITALIZED: ${SERVICE_NAME_FIRST_LETTER_CAPITALIZED}"

TEST_PROJECT_PATH="backend/services/${SERVICE_NAME}/tests/${SERVICE_NAME}-integration-tests/${SERVICE_NAME_FIRST_LETTER_CAPITALIZED}.IntegrationTests.csproj"
echo "TEST_PROJECT_PATH: ${TEST_PROJECT_PATH}"

SERVICE_NAME_UPPERCASE=$(echo "$SERVICE_NAME" | awk '{print toupper($0)}')
DATABASE_VAR_NAME="POSTGRES__${SERVICE_NAME_UPPERCASE}__DATABASENAME"
RESOLVED_DATABASE_NAME=$(eval echo "\${${DATABASE_VAR_NAME}}")

if [ -z "${RESOLVED_DATABASE_NAME}" ]; then
    echo "ERROR: The resolved database name for service '${SERVICE_NAME}' is empty. Please set ${DATABASE_VAR_NAME}."
    exit 1
fi

echo "Resolved database name: ${RESOLVED_DATABASE_NAME}"

echo "Running integration tests for project ${TEST_PROJECT_PATH}"
echo "Running integration tests in a Docker container..."
docker run --rm \
    --name integration-tests-runner \
    --network integration_tests \
    -v "${CI_PROJECT_DIR}":/app \
    -v "${CI_PROJECT_DIR}/TestResults":/app/TestResults \
    -w /app \
    -e RABBITMQ__HOST="rabbitmq" \
    -e RABBITMQ__USERNAME="${RABBITMQ_DEFAULT_USER}" \
    -e RABBITMQ__PASSWORD="${RABBITMQ_DEFAULT_PASS}" \
    -e RABBITMQ__PORT="${RABBITMQ__PORT}" \
    -e RABBITMQ__VIRTUALHOST="/" \
    -e POSTGRES__USERID="${POSTGRES_USER}" \
    -e POSTGRES__PASSWORD="${POSTGRES_PASSWORD}" \
    -e POSTGRES__HOST="postgres" \
    -e POSTGRES__PORT="${POSTGRES__PORT}" \
    -e POSTGRES__${SERVICE_NAME^^}__DATABASENAME="${RESOLVED_DATABASE_NAME}" \
    -e AUTHENTICATION__ISSUER="${AUTHENTICATION__ISSUER}" \
    -e AUTHENTICATION__AUDIENCE="${AUTHENTICATION__AUDIENCE}" \
    -e AUTHENTICATION__SECRETKEY="${AUTHENTICATION__SECRETKEY}" \
    -e API__VERSION="${API__VERSION}" \
    -e APIGATEWAY__URL="${APIGATEWAY__URL}" \
    -e ASPNETCORE__ENVIRONMENT="${ASPNETCORE__ENVIRONMENT}" \
    mcr.microsoft.com/dotnet/sdk:8.0 \
    dotnet test "${TEST_PROJECT_PATH}" \
    --configuration Release \
    --verbosity minimal \
    --logger:"junit;LogFilePath=/app/TestResults/${SERVICE_NAME}_test_results.junit.xml" \
    --results-directory ./TestResults

echo "Integration tests completed successfully."