# Fridge

# Requirements
- .NET 8 SDK
- Node 22.11.0
- Docker
- Powershell >= 7.4.6

## Commands

### Start docker compose
```
docker-compose up --build  --remove-orphans --force-recreate -d
```

### Migrate project database
```
scripts/migrate-project-database.ps1 -projectName <project-name>
```


### Scan for outdated packages

#### Backend
```
dotnet outdated --version-lock Major --include-auto-references
```
to perform update just add `-u` flag

